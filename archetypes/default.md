---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
lastmod:
authors: [guillaume]
categories: []
tags: []
slug: {{ .File.TranslationBaseName }}
bibliography: ../../static/bibliography/references.bib
csl: ../../static/bibliography/the-embo-journal.csl
link-citations: true
draft: true # Remove this once this post is ready for publication.
---


First paragraph will be the post excerpt displayed in post listings.

<!--more-->

Rest of the post will display after a click on "read more".

## Replicate this post

You can replicate this post by running [the Rmd source][rmd-source] with
[RStudio][rstudio] and [R][r]. The easiest way to replicate this post is to
clone the entire [git repository][repo].

```{r Session info}
sessioninfo::session_info()
```

## References


[rmd-source]: `r paste0(getOption("blogdown.source.repo"), getOption("blogdown.source.raw"), "content/blog/{{ .Name }}/index.en.Rmd")`

[rstudio]: https://www.rstudio.com/

[r]: https://www.r-project.org/

[repo]: `r getOption("blogdown.source.repo")`
