---
title: Accueil
---


Bienvenue !

Sur ce site, j'expose quelques informations de contact et professionnelles, et
j'essaye aussi de tenir un blog. Le blog français contient principalement des
photos, tandis que le blog anglais contient principalement des choses en lien
avec mon travail.
