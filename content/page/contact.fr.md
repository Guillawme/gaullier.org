---
title: Me contacter
linkTitle: Contact
description: Je tâche de répondre à tous les emails, soyez patient
menu: main
weight: 3
slug: contact
---


## Par email

Vous pouvez me joindre par email à l'adresse <contact@gaullier.org>. Un lien
vers cette adresse se trouve dans la colonne de gauche et au pied de toutes les
pages de ce site.

Si vous souhaitez me contacter pour une raison professionnelle, utilisez plutôt
l'adresse listée sur ma page [ORCiD][orcid].


[orcid]: https://orcid.org/0000-0003-3405-6021
