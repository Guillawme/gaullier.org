---
title: À propos
linkTitle: À propos
description: Qui suis-je et pourquoi ce site existe-t-il
menu: main
weight: 2
slug: a-propos
---


## À propos de moi

Je suis chercheur en biologie structurale, et je travaille actuellement dans [le
groupe de Cecilia Blikstad][lab] au Département de Chimie, laboratoire Ångström,
Université d'Uppsala. J'ai obtenu un doctorat en "Biochimie et Biologie
Structurale" de l'Université Paris-Sud en septembre 2015, après avoir soutenu ma
[thèse sur l'étude structurale d'un complexe de protéines télomériques
humaines][phd].

Une version à jour de mon CV se trouve sur [mon profil public
LinkedIn][linkedin]. Un lien vers ce profil se trouve dans la colonne de gauche
sur toutes les pages de ce site.

En dehors du labo, j'aime écouter de la musique traditionnelle de différents
pays (Bretagne, Irlande, Écosse, Suède, Québec, États-Unis, etc.), et j'aime les
danses associés à certaines de ces musiques. J'aime aussi jouer de certaines de
ces musiques au *tin whistle* et à la flûte traversière en bois. Enfin,
j'apprécie aussi des activités en extérieur comme le kitesurf, la voile, le ski
et la randonnée (en raquettes l'hiver).

## À propos de ce site

Ce petit site internet est principalement destiné à établir une présence en
ligne plus personnelle (et moins invasive) que ce que proposent les réseaux
sociaux à la mode.

J'écris de temps en temps des billets de blog, principalement en français pour
raconter ma vie, et parfois en anglais pour parler de science ou d'autres sujets
pouvant intéresser un lectorat plus large. Toutes les idées exprimées ici sont
les miennes, sauf *explicitement* indiqué, et ne doivent être associées à aucun
de mes employeurs (passés ou présent). Les billets de blog anciens ne
représentent pas nécessairement mes opinions actuelles.

Tous les billets de blog que j'écris sont publiés sous une [licence Creative
Commons Attribution 4.0 (CC BY 4.0)][cc-by], ce qui signifie que vous pouvez les
republier librement à condition de citer et d'inclure un lien vers l'original.

Ce site est produit avec les générateurs [Blogdown][blogdown] et [Hugo][hugo],
et utilise le thème [Minimo][minimo] avec quelques personnalisations.

## Ce site ne vous piste pas

Le site n'a pas de système de commentaires, c'est intentionnel. Cependant,
n'hésitez pas à commenter [par email][email-me] ou publiquement en écrivant sur
votre propre blog (ou ailleurs sur internet selon vos préférences) en incluant
un lien vers l'article que vous commentez.

Le site n'utilise pas non plus de système d'analyse de traffic ni de cookies.
Toutes les pages, images et fichiers appartenant directement au site sont servis
par [GitLab Pages][gitlab]. Les cartes interactives dans certains billets de
blog sont servies par [Framacarte][framacarte] (qui utilise les fonds de carte
d'[OpenStreetMap][osm]). Certain articles du blog incorporent des vidéos servies
par YouTube.


[lab]: https://www.kemi.uu.se/angstrom/research/molecular-biomimetics/microbial-chemistry/blikstad-group

[phd]: {{< relref "phd.fr.md" >}}

[linkedin]: https://www.linkedin.com/in/guillaumegaullier

[cc-by]: https://creativecommons.org/licenses/by/4.0/deed.fr

[blogdown]: https://bookdown.org/yihui/blogdown

[hugo]: https://gohugo.io/

[minimo]: https://themes.gohugo.io/minimo/

[email-me]: {{< relref "contact.fr.md" >}}

[gitlab]: https://gitlab.com

[framacarte]: https://framacarte.org

[osm]: https://www.openstreetmap.org
