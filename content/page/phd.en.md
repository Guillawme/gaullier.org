---
title: PhD
linkTitle: PhD
description: Abstract of my PhD thesis
menu: main
weight: 4
slug: phd
---


## Structural study of the assembly of human TRF2/RAP1 telomeric complex

- Prepared under the supervision of Dr Marie-Hélène Le Du in the team
  ["Nuclear envelope, telomeres and DNA repair"][lbsr] of the Department of
  Biochemistry, Biophysics and Structural Biology (B3S) of the Institute of
  Integrative Biology of the Cell (I2BC), UMR 9198 CNRS, CEA, Université
  Paris-Sud.
- Publicly defended on September 22<sup>nd</sup> 2015 at the Institut National
  des Sciences et Techniques du Nucléaire (INSTN), Saclay, France.
- [Thesis prize][afc-prize] awarded by the
  [French Crystallographic Association][afc] in 2016.

## Abstract

Telomeres are the ends of eukaryotic linear chromosomes. They are made of tandem
repeats of a short guanine-rich motif and bound by specific proteins.
In vertebrates, these proteins form a complex called shelterin, the integrity of
which is critical to ensure proper replication of chromosome ends and to protect
them against illicit targeting by DNA double-strand break repair pathways.
Telomere dysfunctions lead to genome instability, which can ultimately cause
senescence or cancer. Telomeres are a subnuclear region in which shelterin
proteins are highly enriched, enhancing low affinity interactions of important
biological function. Among shelterin proteins, telomeric repeat-binding protein
TRF2 and its constitutive partner RAP1 are the main factors responsible for end
protection. We studied in details the assembly of TRF2/RAP1 complex by means of
integrated structural, biophysical and biochemical approaches. We showed that
this assembly displays important conformational adjustments of both proteins,
and involves a low affinity interaction engaging large regions in both proteins
which affects their interaction properties.

Keywords: telomeres, TRF2, RAP1, SAXS, ITC, crystallography,
protein footprinting.

## Full text

Because the thesis was defended in a French university and I am a native French
speaker, university rules requested it be written in French.

[The final version of the manuscript][these-tel] is published in the [*Thèses en
ligne*][tel] public archive. All source files and full modification history are
available as [a public git repository][these-repo].


[lbsr]: http://www.i2bc.paris-saclay.fr/spip.php?article168&lang=en

[afc-prize]: http://www.afc.asso.fr/prix-de-these/les-laureats-2016#prix_biologie

[afc]: http://www.afc.asso.fr

[these-tel]: https://tel.archives-ouvertes.fr/tel-01281310

[tel]: https://tel.archives-ouvertes.fr

[these-repo]: https://github.com/Guillawme/these
