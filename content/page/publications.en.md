---
title: Publications
linkTitle: Publications
description: List of my research articles
menu: main
weight: 5
slug: publications
---


Here is a list of publications I contributed to. I try to publish in open access
venues [as much as possible][oa-score], but if you don't have access to any of
my articles you should not hesitate to [email me][email] to ask: I will always
send a PDF if I have it. These publications are also all listed on my [ORCiD
record][orcid] and [Google Scholar page][gscholar]. You can download [a BibTeX
file containing all these references][my-pubs-bib].

## Peer-reviewed articles

- Sanchez-Garcia R, **Gaullier G**, Cuadra-Troncoso JM & Vargas J (2024) Cryo-EM
  Map Anisotropy Can Be Attenuated by Map Post-Processing and a New Method for
  Its Estimation. *International Journal of Molecular Sciences* **25**: 3959
  + {{< doi "10.3390/ijms25073959" >}}
- Bacic L, **Gaullier G**, Mohapatra J, Mao G, Brackmann K, Panfilov M, Liszczak
  G, Sabantsev A & Deindl S (2024) Asymmetric nucleosome PARylation at DNA
  breaks mediates directional nucleosome sliding by ALC1. *Nat Commun* **15**:
  1000
  + {{< doi "10.1038/s41467-024-45237-8" >}}
  + {{< pdb 8B0A >}}
  + {{<emdb 15777 >}}
  + {{< empiar 11211 >}}
- Bacic L, **Gaullier G**, Sabantsev A, Lehmann LC, Brackmann K, Dimakou D,
  Halic M, Hewitt G, Boulton SJ & Deindl S (2021) Structure and dynamics of the
  chromatin remodeler ALC1 bound to a PARylated nucleosome. *eLife* **10**:
  e71420
  + {{< doi "10.7554/eLife.71420" >}}
  + {{< pdb 7OTQ >}}
  + {{< emdb 13065 >}}
  + {{< emdb 13070 >}}
  + {{< emdb 17944 >}}
  + {{< empiar 10739 >}}
  + {{< empiar 11618 >}}
- Lehmann LC, Bacic L, Hewitt G, Brackmann K, Sabantsev A, **Gaullier G**,
  Pytharopoulou S, Degliesposti G, Okkenhaug H, Tan S, Costa A, Skehel JM,
  Boulton SJ & Deindl S (2020) Mechanistic Insights into Regulation of the ALC1
  Remodeler by the Nucleosome Acidic Patch. *Cell Reports* **33**
  + {{< doi "10.1016/j.celrep.2020.108529" >}}
  + {{< pdb 6ZHX >}}
  + {{< pdb 6ZHY >}}
  + {{< emdb 11220 >}}
  + {{< emdb 11221 >}}
  + {{< empiar 10465 >}}
- **Gaullier G**, Roberts G, Muthurajan UM, Bowerman S, Rudolph J, Mahadevan J,
  Jha A, Rae PS & Luger K (2020) Bridging of nucleosome-proximal DNA
  double-strand breaks by PARP2 enhances its interaction with HPF1. *PLOS ONE*
  **15**: e0240932
  + {{< doi "10.1371/journal.pone.0240932" >}}
  + data sets: {{< doi "10.5281/zenodo.3519436" >}}
  + {{< pdb 6USJ >}}
  + {{< emdb 20864 >}}
  + {{< empiar 10336 >}}
- Makowski MM, **Gaullier G** & Luger K (2020) Picking a nucleosome lock:
  Sequence- and structure-specific recognition of the nucleosome. *J Biosci*
  **45**: 13
  + {{< doi "10.1007/s12038-019-9970-7" >}}
- Brown P, RELISH Consortium & Zhou Y (2019) Large expert-curated database for
  benchmarking document similarity detection in biomedical literature search.
  *Database* **2019**: baz085
  + {{< doi "10.1093/database/baz085" >}}
- Zhou K, **Gaullier G** & Luger K (2019) Nucleosome structure and dynamics are
  coming of age. *Nature Structural & Molecular Biology* **26**: 3-13
  + {{< doi "10.1038/s41594-018-0166-x" >}}
- **Gaullier G**, Miron S, Pisano S, Buisson R, Le Bihan Y-V, Tellier-Lebègue C,
  Messaoud W, Roblin P, Guimarães BG, Thai R, Giraud-Panis M-J, Gilson E & Le Du
  M-H (2016) A higher-order entity formed by the flexible assembly of RAP1 with
  TRF2. *Nucleic Acids Research* **44**: 1962–1976
  + {{< doi "10.1093/nar/gkv1531" >}}
  + {{< pdb 4RQI >}}

## Preprints

- Sanchez-Garcia R, **Gaullier G**, Cuadra-Troncoso JM & Vargas J (2023) Cryo-EM
  map anisotropy can be attenuated by map post-processing and a new method for
  its estimation. *bioRxiv*: **2022.12.08.517920**
  + {{< doi "10.1101/2022.12.08.517920" >}}
- Bacic L, **Gaullier G**, Sabantsev A, Lehmann L, Brackmann K, Dimakou D,
  Halic M, Hewitt G, Boulton SJ & Deindl S (2021) Structure and dynamics of the
  chromatin remodeler ALC1 bound to a PARylated nucleosome. *bioRxiv*:
  **2021.06.18.448936**
  + {{< doi "10.1101/2021.06.18.448936" >}}
  + {{< pdb 7OTQ >}}
  + {{< emdb 13065 >}}
  + {{< emdb 13070 >}}
  + {{< empiar 10739 >}}
- **Gaullier G**, Roberts G, Muthurajan UM, Bowerman S, Rudolph J, Mahadevan J,
  Jha A, Rae PS & Luger K (2019) Bridging of nucleosome-proximal DNA double-strand
  breaks by PARP2 enhances its interaction with HPF1. *bioRxiv*: **846618**
  + {{< doi "10.1101/846618" >}}
  + data sets: {{< doi "10.5281/zenodo.3519436" >}}
  + {{< pdb 6USJ >}}
  + {{< emdb 20864 >}}
  + {{< empiar 10336 >}}

## Other publications

- **Gaullier G** (2019) French cuts to research jobs could fuel brain drain.
  *Nature* **569**: 336
  + {{< doi "10.1038/d41586-019-01529-4" >}}
- **Gaullier G** & Luger K (2017) PARP1 and Sox2: An Unlikely Team of Pioneers
  to Conquer the Nucleosome. *Molecular Cell* **65**: 581–582
  + {{< doi "10.1016/j.molcel.2017.02.001" >}}

## Software

- **Gaullier G** (2021) classconvergence: Plot the class distribution as a
  function of iteration from a Class2D or Class3D job from RELION. Zenodo
  + {{< doi "10.5281/zenodo.4732050" >}}
- **Gaullier G** (2021) topaztrainmetrics: Plot metrics from a Topaz training
  run. Zenodo
  + {{< doi "10.5281/zenodo.4451826" >}}
- **Gaullier G** (2020) countparticles: Report the number of particles in each
  class from RELION. Zenodo
  + {{< doi "10.5281/zenodo.4139795" >}}
- **Gaullier G** (2020) angdist: Plot the 2D histogram of Euler angles covered
  by a set of cryo-EM particles. Zenodo
  + {{< doi "10.5281/zenodo.4104083" >}}
- **Gaullier G** (2020) localres: Plot the histogram of local resolution values
  of a cryo-EM reconstruction. Zenodo
  + {{< doi "10.5281/zenodo.4103384" >}}


[oa-score]: http://progs.coudert.name/OA_audit?orcid=0000-0003-3405-6021

[email]: {{< relref path="/page/contact" lang="en" >}}

[orcid]: https://orcid.org/0000-0003-3405-6021

[gscholar]: https://scholar.google.com/citations?user=KtRaJTsAAAAJ

[my-pubs-bib]: /bibliography/guillaume-gaullier-publications.bib
