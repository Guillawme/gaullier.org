---
title: Contact me
linkTitle: Contact
description: I will answer email when appropriate
menu: main
weight: 3
slug: contact
---


## By email

You can contact me by email using the address <contact@gaullier.org>, which is
also linked in the left sidebar and at the bottom of all pages of this website.

If you are contacting me for professional reasons, please prefer the address
listed on my [ORCiD][orcid] page.


[orcid]: https://orcid.org/0000-0003-3405-6021
