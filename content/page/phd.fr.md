---
title: Thèse
linkTitle: Thèse
description: Le résumé de ma thèse
menu: main
weight: 4
slug: these
---


## Étude structurale de l'assemblage du complexe télomérique humain TRF2/RAP1

- Préparée sous la direction du Dr Marie-Hélène Le Du dans l'équipe
  ["Enveloppe nucléaire, télomères et réparation de l'ADN"][lbsr] du Départment
  de Biochimie, Biophysique et Biologie Structurale (B3S) de l'Institut de
  Biologie Intégrative de la Cellule (I2BC), UMR 9198 CNRS, CEA, Université
  Paris-Sud.
- Soutenue publiquement le 22 septembre 2015 à l'Institut National des Sciences
  et Techniques du Nucléaire (INSTN), Saclay, France.
- Récompensée par le [prix de thèse][afc-prize] de l'[association française de
  cristallographie][afc] en 2016.

## Résumé

Les télomères sont les extrémités des chromosomes linéaires des eucaryotes. Ils
sont constitués de répétitions en tandem d'un motif court riche en guanine, et
liés par des protéines spécifiques. Chez les vertébrés ces protéines forment un
complexe appelé le *shelterin* et dont l'intégrité est critique pour assurer la
réplication correcte des extrémités des chromosomes, et pour les protéger contre
une prise en charge illicite par les voies de réparation des cassures
double-brin de l'ADN. Des dysfonctions des télomères engendrent une instabilité
du génome qui peut conduire à la sénescence ou au cancer. Les télomères
représentent une région subnucléaire où les protéines du *shelterin* sont
fortement enrichies, ce qui permet l'implication dans les fonctions biologiques
d'interactions de basse affinité. Parmi les protéines du shelterin, la protéine
de liaison aux répétitions télomériques TRF2 et son partenaire constitutif RAP1
sont les facteurs majeurs responsables de la protection des extrémités. Nous
avons étudié en détails l'assemblage du complexe TRF2/RAP1 par des approches
intégrées de biologie structurale, de biophysique et de biochimie. Nous avons
montré que cet assemblage s'accompagne d'importants ajustements de conformation
des deux protéines, et implique une interaction de basse affinité qui engage de
grandes régions des deux protéines et affecte leurs propriétés d'interactions.

Mots-clés : télomères, TRF2, RAP1, SAXS, ITC, crystallographie, empreinte
protéique.

## Texte intégral

[La version finale de ma thèse][these-tel] est publiée dans l'archive publique
[Thèses en ligne][tel]. Les fichiers sources et l'historique complet des
modifications sont disponibles dans [ce dépôt git public][these-repo].


[lbsr]: http://www.i2bc.paris-saclay.fr/spip.php?article168&lang=en

[afc-prize]: http://www.afc.asso.fr/prix-de-these/les-laureats-2016#prix_biologie

[afc]: http://www.afc.asso.fr

[these-tel]: https://tel.archives-ouvertes.fr/tel-01281310

[tel]: https://tel.archives-ouvertes.fr

[these-repo]: https://github.com/Guillawme/these
