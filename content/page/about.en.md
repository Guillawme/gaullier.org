---
title: About
linkTitle: About
description: Who I am and why this website exists
menu: main
weight: 2
slug: about
---


## About me

I am a researcher in structural biology, currently working in [the group of Dr
Cecilia Blikstad][lab] at the Department of Chemistry, Ångström Laboratory,
Uppsala University. I earned a PhD in "Biochemistry and Structural Biology" from
the Université Paris-Sud in September 2015, after defending my [thesis about the
structural study of a human telomere protein complex][phd].

The current version of my CV can be found on my [LinkedIn public
profile][linkedin], which is also linked at the bottom of every pages of this
website.

Outside the lab, I enjoy listening to traditionnal music from several countries
(Brittany, Ireland, Scotland, Sweden, Québec, American old time music...), and
dancing on some of them. I also try to play them on the wooden flute and tin
whistle. And I like outdoor activities like kitesurfing, sailing, skiing,
snowshoeing and hiking.

## About this website

This small website is meant to establish online presence in a more personal (and
less invasive) way than what trendy social media want to offer.

I also maintain a blog. It is mostly written in French, except for articles
whose topic could interest a broader readership. All ideas expressed here are my
own, except *explicitly* indicated otherwise, and should not be attributed to
any of my employers (past or present). Old blog posts may not reflect my current
opinions.

All blog posts I authored here are released under a [Creative Commons
Attribution 4.0 license (CC BY 4.0)][cc-by], which means you are free to repost
them anywhere you like, as long as you link back to the original. All code
presented in blog posts is released under the [MIT license][mit], which means
you are free to use it (at your own risk), modify it and redistribute it (even
with less permissions) as long as you give credit to the original.

This website is built with the [Blogdown][blogdown] and [Hugo][hugo] generators,
and uses the [Minimo][minimo] theme with some customizations.

## Privacy policy

The blog section does not let visitors write comments, this is on purpose. Feel
free to comment by [sending me an email][email-me], or by posting your comment
publicly on your own blog (or anywhere you feel is appropriate) and linking back
to the article you are commenting.

This website does not use any traffic analysis tool, nor does it use cookies.
All pages, images and files directly belonging to this site are served by
[GitLab Pages][gitlab]. Interactive maps in certain blog posts are served by
[Framacarte][framacarte] (using maps from [OpenStreetMap][osm]). Some blog posts
embed videos served by YouTube.


[lab]: https://www.kemi.uu.se/angstrom/research/molecular-biomimetics/microbial-chemistry/blikstad-group

[phd]: {{< relref "phd.en.md" >}}

[linkedin]: https://www.linkedin.com/in/guillaumegaullier

[cc-by]: https://creativecommons.org/licenses/by/4.0

[mit]: https://choosealicense.com/licenses/mit/

[blogdown]: https://bookdown.org/yihui/blogdown

[hugo]: https://gohugo.io/

[minimo]: https://themes.gohugo.io/minimo/

[email-me]: {{< relref "contact.en.md" >}}

[gitlab]: https://gitlab.com

[framacarte]: https://framacarte.org

[osm]: https://www.openstreetmap.org
