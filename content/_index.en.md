---
title: Home
---


Welcome!

I keep here some information about me, how to contact me, some work-related
information (my PhD thesis abstract and my publication list), and I also
maintain a blog. The French blog mostly contains pictures, while the English
blog mostly contains work- and science-related things.
