---
title: "Moxie Bread Company"
date: 2016-12-16
lastmod: 2023-02-05
authors: [guillaume]
categories: []
tags: [Colorado, food]
slug: moxie-bread-co
---


**Mise à jour du 5 février 2023 :** en novembre 2022, nous avons appris le
[décès d'Andy Clark][andy], le fondateur de Moxie. Je ne le connaissais pas très
bien, mais nous avons souvent joué de la musique avec lui au cours des soirées
pizza à la boulangerie, et cette nouvelle m'a rendu très triste et nostalgique
de ces bons moments. Andy était toujours rayonnant de bonne humeur et
d'enthousiasme, et je me souviendrai toujours de son grand sourire. Il y a
quelques photos de lui sur le [blog de Moxie][moxie-blog].

À Louisville, une des villes voisines de Boulder, il y a une boulangerie
appellée [*Moxie Bread Company*][moxie]. C'est pour le moment la meilleure
boulangerie que nous avons trouvée.

<!--more-->

![Moxie Bread Co](images/moxie-1.jpg)

![Moxie Bread Co](images/moxie-2.jpg)

Ils font même des kouign amann, une pâtisserie bretonne ! (le leur est très
allégé en beurre si on compare à l'original, mais c'est bon quand même).
Le "nature" est bon ; nous n'avons pas encore goûté les autres. Pour Halloween
ils en avaient fait un au potiron et chocolat... Étrange.

![Moxie Bread Co](images/moxie-3.jpg)


[moxie]: https://www.moxiebreadco.com

[andy]: https://boulderweekly.com/news/remembering-andy-clark-of-moxie-bread-co/

[moxie-blog]: https://www.moxiebreadco.com/blog
