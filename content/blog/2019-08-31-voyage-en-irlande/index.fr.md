---
title: Voyage en Irlande
author: Guillaume Gaullier
date: '2019-08-31'
slug: voyage-en-irlande
categories: []
tags:
  - travels
  - ireland
lastmod: ~
authors:
  - guillaume
---


Cet été, du 8 au 14 août, nous sommes allés en Irlande avec notre amie harpiste
Margot. Nous sommes arrivés à Cork, avons loué une voiture et avons roulé à
travers le Kerry et le Connemara avant de rentrer à Cork pour ensuite aller en
Bretagne.

<!--more-->

À Cork, nous avons visité le [*butter museum*][butter], qui expliquait comment
l'Irlande a industrialisé et normalisé sa production de beurre au début du 20e
siècle pour pouvoir l'exporter, alors qu'avant cette période chaque crèmerie
produisait un beurre différent.

![Butter Museum](images/butter-museum-1.jpg)

La partie que j'ai trouvée la plus intéressante dans ce musée parlait de la
préhistoire en Irlande et mentionnait le *bog butter*, du beurre qu'on retrouve
parfois dans des tourbières et qui est très ancien. On ne sait toujours pas
pourquoi les gens à cette époque enfouissaient du beurre dans des tourbières.
Certains pensent que c'était un rituel, d'autres pensent que c'était un moyen
de conservation pendant l'été quand la température ambiante était trop chaude
pour pouvoir conserver du beurre.

![Bog butter](images/butter-museum-2.jpg)

![Bog butter sign](images/butter-museum-3.jpg)

Après une nuit et une journée à Cork, nous sommes allés dans le Kerry. Nous
avons passé une nuit à Killarney et une nuit à Dingle. Près de Killarney, nous
avons vu le [*gap of Dunloe*][dunloe] :

![Gap of Dunloe](images/kerry-1.jpg)

À Ventry, juste à l'ouest de Dingle, nous avons visité le *Celtic Prehistoric
Museum* :

![Celtic Prehistoric Museum](images/celtic-prehistoric-museum-1.jpg)

![Celtic Prehistoric Museum](images/celtic-prehistoric-museum-2.jpg)

Après notre étape à Dingle, nous avons fait le tour de la péninsule de Dingle.

![Péninsule de Dingle](images/kerry-2.jpg)

L'île dans la photo suivante s'appelle [*Great Blasket Island*][blasket], et
nous avions prévu d'y passer la deuxième nuit du voyage après Cork, mais quand
nous sommes arrivés la météo était trop mauvaise et les ferrys annulés, donc
nous avons passé cette nuit à Killarney.

![Blasket Island](images/kerry-3.jpg)

Il y a [plusieurs sites archéologiques][sites] sur la péninsule de Dingle, et
nous n'avons pas pris le temps de nous arrêter à chacun d'eux. Nous avons visité
l'[oratoire de Gallarus][gallarus] :

![Oratoire de Gallarus](images/kerry-4.jpg)

Nous avons fait une étape à [Barna][barna], chez Úna, une amie (elle aussi
harpiste) de Margot. Nous sommes ensuites allés dans le [Connemara][connemara].

![Connemara](images/connemara-1.jpg)

Nous avons visité l'[abbaye de Kylemore][kylemore] et ses jardins :

![Abbaye de Kylemore](images/kylemore-1.jpg)

![Abbaye de Kylemore](images/kylemore-2.jpg)

![Jardins de l'abbaye de Kylemore](images/kylemore-3.jpg)

![Jardins de l'abbaye de Kylemore](images/kylemore-4.jpg)

Nous avons fait une rando dans le [*Connemara National Park*][park], près de
[Letterfrack][letterfrack] :

![Connemara National Park](images/connemara-2.jpg)

![Connemara National Park](images/connemara-3.jpg)

![Connemara National Park](images/connemara-4.jpg)

Le lendemain, nous avons fait une autre petite promenade dans les environs.

![Connemara National Park](images/connemara-5.jpg)

![Embouteillage dans le Connemara](images/connemara-6.jpg)

Après cet embouteillage, nous avons roulé jusqu'à [Ennis][ennis] où nous avons
passé un peu de temps avec Kate, une autre amie musicienne de Boulder qui était
en vacances dans la région au même moment. Puis nous avons continué jusqu'à
[Kinsale][kinsale], où nous avons passé la dernière nuit et le dernier jour
avant de reprendre l'avion à Cork. Nous avons suivi une visite guidée fascinante
sur l'histoire de la ville. J'ai bien aimé les maisons très colorées (encore
plus qu'à Cork et Dingle) :

![Kinsale](images/kinsale.jpg)


[butter]: http://thebuttermuseum.com/

[dunloe]: https://fr.wikipedia.org/wiki/Gap_of_Dunloe

[blasket]: https://fr.wikipedia.org/wiki/An_Blascaod_M%C3%B3r

[sites]: https://dingle-peninsula.ie/explore/history-and-archaeology/8-explore-the-dingle-peninsula/history-and-archaeology/206-monuments-and-archaeological-sites-on-the-dingle-peninsula-laithreacha-seandaliochta-i-gcorca-dhuibhne.html

[gallarus]: https://fr.wikipedia.org/wiki/Oratoire_de_Gallarus

[barna]: https://en.wikipedia.org/wiki/Barna

[connemara]: https://fr.wikipedia.org/wiki/Connemara

[kylemore]: https://fr.wikipedia.org/wiki/Abbaye_de_Kylemore

[park]: https://fr.wikipedia.org/wiki/Parc_national_du_Connemara

[letterfrack]: https://fr.wikipedia.org/wiki/Letterfrack

[ennis]: https://fr.wikipedia.org/wiki/Ennis

[kinsale]: https://fr.wikipedia.org/wiki/Kinsale
