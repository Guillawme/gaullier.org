---
title: "Première chute de neige"
date: 2016-11-19
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: premiere-chute-de-neige
---


Il y a trois jours c'était encore presque l'été : 25 °C et un grand soleil.
L'automne a littéralement duré une seule nuit, car le lendemain de ce jour-là il
a neigé !

<!--more-->

Pour la saison et l'altitude, cette première chute de neige est bien en retard
(même si c'était bien agréable de se sentir en été encore en novembre).
Voilà des photos.

Pendant la chute de neige :

![Neige](images/neige-1.jpg)

Et le lendemain matin, depuis chez nous :

![Neige](images/neige-2.jpg)

![Neige](images/neige-3.jpg)

Le lendemain matin, la vue depuis le labo :

![Neige](images/neige-4.jpg)
