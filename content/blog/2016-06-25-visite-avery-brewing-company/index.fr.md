---
title: "Visite d'Avery Brewing Company"
date: 2016-06-25
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, food]
slug: visite-avery-brewing-company
---


Occupé par un aller-retour à Paris début juin, puis par la préparation d'une
candidature pour une demande de financement depuis mon retour, cela fait très
longtemps que je n'ai pas écrit sur le blog... Voilà un bref récit, surtout en
photos, d'une visite de la brasserie [*Avery Brewing Company*][avery] située en
périphérie de Boulder.

<!--more-->

C'était le 15 juin, cette visite était organisée par le labo en tant que
*science tour* pour les étudiants en stage chez nous. C'était obligatoire pour
eux (je n'ai toujours pas bien compris pourquoi), et les autres membres du labo
pouvaient se joindre à la visite. Comme je suis encore le nouveau, j'accepte
toutes les opportunités de visites. :-)

Au Colorado il y a un réel engouement pour la nourriture locale en général, et
pour la brasserie en particulier. Il y a des dizaines de bières créées
localement, qui ne ressemblent à rien de ce qu'on peut trouver en Europe.
Lors de la visite d'Avery c'était absolument impossible de tout goûter bien
sûr... Voilà quelques photos de la brasserie.

![Avery Brewing Company](images/avery-1.jpg)

La visite se fait sur un balcon qui surplombe les installations, et on peut
tout à fait déguster en même temps qu'on visite, d'où l'avertissement suivant :

![Avery Brewing Company](images/avery-2.jpg)

![Avery Brewing Company](images/avery-3.jpg)

Ces immenses cuves contiennent le produit final prêt à être mis en bouteilles ou
canettes (on aperçoit une personne en bas, presque cachée par le bord du
balcon : cela donne une idée de la taille des cuves) :

![Avery Brewing Company](images/avery-4.jpg)

Le contenu d'une des cuves où la bière est brassée :

![Avery Brewing Company](images/avery-5.jpg)

Une des cuves de brassage, vide celle-ci :

![Avery Brewing Company](images/avery-6.jpg)

Leur bière au fruit de la passion a apparemment pas mal de succès :

![Avery Brewing Company](images/avery-7.jpg)

La chaîne de remplissage des canettes :

![Avery Brewing Company](images/avery-8.jpg)

Les produits finis, en bouteilles et à la pression :

![Avery Brewing Company](images/avery-9.jpg)

![Avery Brewing Company](images/avery-10.jpg)

Je suis trop occupé en ce moment pour écrire régulièrement sur le blog, mais
j'ai quelques autres choses à raconter. Ce sera pour courant juillet, après un
deuxième aller-retour en France du 30 juin au 8 juillet pour un congrès... Je
vais passer tout l'été à me remettre du décalage horaire. :-/


[avery]: https://www.averybrewing.com
