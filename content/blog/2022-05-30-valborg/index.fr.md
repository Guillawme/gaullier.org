---
title: Valborg
author: Guillaume Gaullier
date: '2022-05-30'
slug: valborg
categories: []
tags:
  - sweden
lastmod: ~
authors:
  - guillaume
---


Ce billet arrive avec un mois de retard, car je n'ai pas eu beaucoup le temps
d'écrire sur ce blog dernièrement. *Valborg* est une fête importante à Uppsala,
qui a lieu le dernier jour d'avril. Nous n'avions jusqu'à cette année pas eu
l'occasion de voir cette fête ni d'y participer, car elle était bien sûr annulée
à cause de la pandémie. Mais cette année, elle a eu lieu.

<!--more-->

Il s'agit d'une [fête du retour du printemps][walpurgis], apparemment fêtée dans
pas mal d'endroits en Europe (mais pas dans les régions où j'ai grandi, où c'est
plutôt [la fin de l'automne et de la saison des récoltes][samain] que l'on fête).

À Uppsala, la fête commence avec une descente de la rivière [Fyrisån][fyrisan]
par les étudiants de l'université, dans des radeaux qu'ils ont construits et
décorés pour l'occasion. Il y a plein de spectateurs à l'arrivée car c'est là
que tous les radeaux doivent franchir un petit barrage, et beaucoup des étudiants
finissent à l'eau. Voilà quelques photos prises à ce moment de la journée :

![Valborg](images/valborg-01.jpeg)

![Valborg](images/valborg-02.jpeg)

![Valborg](images/valborg-03.jpeg)

![Valborg](images/valborg-04.jpeg)

![Valborg](images/valborg-05.jpeg)

![Valborg](images/valborg-06.jpeg)

![Valborg](images/valborg-07.jpeg)

![Valborg](images/valborg-08.jpeg)

Après ça, nous avons déjeuné et chanté avec nos amis, puis nous sommes allés
écouter la chorale devant la [bibliothèque *Carolina Rediviva*][bibliotheque].
Ensuite nous avons joué une marche en ville avec le [*V-Dala
Spelmanslag*][v-dala], puis nous avons joué quelques morceaux de plus à
l'arrivée. Le soir, nous sommes allés à un bal au [château][chateau]. Je n'aime
pas devoir porter un costume, mais j'ai quand même passé un bon moment car il y
avait un groupe de jazz très bon.


[walpurgis]: https://fr.wikipedia.org/wiki/Nuit_de_Walpurgis

[samain]: https://fr.wikipedia.org/wiki/Samain_(mythologie)

[fyrisan]: https://fr.wikipedia.org/wiki/Fyris%C3%A5n

[bibliotheque]: https://fr.wikipedia.org/wiki/Biblioth%C3%A8que_de_l%27universit%C3%A9_d%27Uppsala

[v-dala]: https://www.v-dalaspelmanslag.se

[chateau]: https://fr.wikipedia.org/wiki/Ch%C3%A2teau_d%27Uppsala
