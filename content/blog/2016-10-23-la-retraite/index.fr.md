---
title: "La retraite"
date: 2016-10-23
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, work]
slug: la-retraite
---


Le week-end dernier (15 et 16 octobre) a eu lieu la retraite du département.
En français nous n'avons que ce mot, "retraite", alors qu'en anglais ce type
d'événement s'appelle *a retreat* tandis que prendre sa retraite au sens
"arrêter de travailler" se dit *to retire* (le nom qui désigne le même concept
est *retirement*). Il est donc impossible de jouer avec le sens du mot en
anglais... La retraite se tenait dans un centre de vacances perdu au milieu des
Rocheuses, un endroit magnifique, mais malheureusement nous n'avons pas eu de
temps libre pour en profiter. Voilà quelques photos (encore des photos de
montagnes... ce blog devient banal, mais promis la prochaine fois ce seront
d'autres photos).

<!--more-->

![Au milieu des Rocheuses](images/retraite-1.jpg)

![Au milieu des Rocheuses](images/retraite-2.jpg)

![Au milieu des Rocheuses](images/retraite-3.jpg)
