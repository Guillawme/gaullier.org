---
title: Blog posts
linkTitle: Blog
description: I write about work and science in English. The French blog mostly contains pictures.
menu: main
weight: 1
slug: blog
---

![RSS icon](/images/rss.svg) Feeds: [RSS]({{< relref path="_index.en.md" outputFormat="rss" >}}), [JSON]({{< relref path="_index.en.md" outputFormat="json" >}}).
([What is a feed?](https://aboutfeeds.com/))
