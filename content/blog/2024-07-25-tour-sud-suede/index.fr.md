---
title: Tour du sud de la Suède
author: Guillaume Gaullier
date: '2024-07-25'
slug: tour-sud-suede
categories: []
tags:
  - travels
  - sweden
lastmod: ~
authors:
  - guillaume
---


En juin, les parents de Rebecca nous ont rendu visite, et nous avons fait un
tour du sud de la Suède en train ensemble.

<!--more-->

## Kalmar et Öland

Peu après leur arrivée, je suis parti pour [Landskrona][landskrona] pour un
stage de musique de 4 jours. Je les ai ensuite retrouvés à leur première étape à
[Kalmar][kalmar]. Nous y étions pendant [Midsommar][midsommar], et la ville
était donc déserte, c'était bien reposant. Voilà quelques photos :

![Kalmar](./images/kalmar-01.jpeg)

La cathédrale :

![La cathédrale de Kalmar](images/kalmar-02.jpeg)

Le château, qui était le siège de l'[Union de Kalmar][kalmar-union] :

![Le château de Kalmar](images/kalmar-03.jpeg)

Ce détail sur une plaque au château de Kalmar m'a interpellé, puisque je venais
de participer à un stage de flûte :

![Un ancien dessin au château de Kalmar, représentant des musiciens dont un flûtiste](images/kalmar-04.jpeg)

L'hôtel où nous logions était près d'un grand étang peuplé de nombreux hérons :

![Un héron à Kalmar](images/kalmar-05.jpeg)

Nous avons aussi pris le ferry jusqu'à l'île d'[Öland][oeland]. Il a plu presque
toute la journée... mais nous avons quand même fait une petite promenade. Voilà
la vue de Kalmar depuis l'île, on aperçoit au fond à droite le pont qui la relie
au continent :

![Kalmar vue depuis Öland](images/oeland-01.jpeg)


## Karlskrona et l'archipel de Blekinge

Nous avons ensuite pris le train jusqu'à [Karlskrona][karlskrona], un peu plus
au sud, où nous sommes restés trois jours. Voilà quelques photos de la ville :

![Karlskrona](images/karlskrona-01.jpeg)

![Karlskrona](images/karlskrona-02.jpeg)

![Karlskrona](images/karlskrona-03.jpeg)

![Karlskrona](images/karlskrona-04.jpeg)

![Karlskrona](images/karlskrona-05.jpeg)

Nous avons visité le musée naval. La ville est historiquement une base navale,
et l'est encore aujourd'hui.

![Musée naval de Karlskrona](images/karlskrona-06.jpeg)

Nous avons visité la forteresse Drottningskär sur l'île d'[Aspö][aspoe] :

![Aspö](images/aspoe-01.jpeg)

![Forteresse Drottningskär](images/aspoe-02.jpeg)

![Forteresse Drottningskär](images/aspoe-03.jpeg)

![Forteresse Drottningskär](images/aspoe-04.jpeg)

Un autre jour, nous avons pris un bâteau jusqu'à l'île de [Stenshamn][stenshamn].
Voilà une vue d'une petite partie de l'[archipel de Blekinge][archipel] :

![Archipel de Blekinge](images/archipel-01.jpeg)

Stenshamn n'a pas de voitures, c'était très calme. La vue donnait envie de se
baigner :

![Un beau plan d'eau à Stenshamn](images/stenshamn-01.jpeg)

Mais regarder de plus près m'a vite rappelé pourquoi je n'aime pas me baigner
dans la mer Baltique...

![Mais pas si attirant en regardant de plus près...](images/stenshamn-02.jpeg)

Nous avons randonné à travers l'île :

![Stenshamn](images/stenshamn-03.jpeg)

![Stenshamn](images/stenshamn-04.jpeg)

![Stenshamn](images/stenshamn-05.jpeg)

![Stenshamn](images/stenshamn-06.jpeg)


## Lund

En route pour notre prochaine étape, nous nous sommes arrêtés à [Lund][lund]
pour seulement une journée. Voilà quelques photos de la
[cathédrale][lund-cathedrale] :

![La cathédrale de Lund](images/lund-01.jpeg)

![La cathédrale de Lund](images/lund-02.jpeg)

![La cathédrale de Lund](images/lund-03.jpeg)

![Un vitrail de la cathédrale de Lund, représentant un violoniste](images/lund-04.jpeg)

![Le choeur de la cathédrale de Lund](images/lund-05.jpeg)

L'horloge astronomique fonctionne encore, et nous avons pu en voir une
démonstration :

![L'horloge astronomique de la cathédrale de Lund](images/lund-06.jpeg)

La vieille ville est uniquement piétonne et très agréable.

![Lund](images/lund-07.jpeg)

Le jardin botanique dans la vieille ville contribue bien à cette ambiance calme :

![Le jardin botanique de Lund](images/lund-08.jpeg)

![Le jardin botanique de Lund](images/lund-09.jpeg)

![Le jardin botanique de Lund](images/lund-10.jpeg)


## Helsingborg et Helsingør

Après cette escale d'une journée à Lund, nous sommes allés à
[Helsingborg][helsingborg]. Voilà quelques vues de la ville :

![Helsingborg](images/helsingborg-01.jpeg)

![Helsingborg vue des hauteurs, avec l'Öresund en arrière plan](images/helsingborg-02.jpeg)

![Helsingborg vue des hauteurs, avec l'Öresund en arrière plan](images/helsingborg-03.jpeg)

La [tour Kärnan][kaernan] :

![Helsingborg](images/helsingborg-04.jpeg)

![Le port d'Helsingborg](images/helsingborg-05.jpeg)

Pendant une promenade en soirée, nous avons eu ce magnifique coucher de soleil
sur l'[Öresund][oeresund] :

![Coucher de soleil sur l'Öresund](images/helsingborg-06.jpeg)

La mairie d'Helsingborie est très imposante, et vaut le détour :

![La mairie d'Helsingborg](images/helsingborg-11.jpeg)

![La mairie d'Helsingborg](images/helsingborg-09.jpeg)

![La mairie d'Helsingborg](images/helsingborg-10.jpeg)

![La mairie d'Helsingborg](images/helsingborg-07.jpeg)

![La mairie d'Helsingborg](images/helsingborg-08.jpeg)

Nous avons passé une journée à [Helsingør][helsingoer] au Danemark, accessible
avec des ferries qui traversent l'Öresund régulièrement :

![Le ferry traversant l'Öresund](images/helsingborg-12.jpeg)

![Le ferry traversant l'Öresund](images/helsingborg-13.jpeg)

En approche du Danemark :

![Helsingør vue depuis l'Öresund](images/helsingoer-01.jpeg)

Nous commençons à apercevoir le [château de Kronborg][kronborg] :

![Le château de Kronborg vu depuis l'Öresund](images/helsingoer-02.jpeg)

![Le château de Kronborg](images/helsingoer-03.jpeg)

![Le château de Kronborg](images/helsingoer-04.jpeg)

![Le château de Kronborg](images/helsingoer-06.jpeg)

![La Suède de l'autre côté de l'Öresund, vue depuis le Danemark](images/helsingoer-05.jpeg)

Après avoir fait le tour du château, nous avons marché un peu dans la ville.
Voilà encore quelques photos :

![Helsingør](images/helsingoer-07.jpeg)

![Helsingør](images/helsingoer-08.jpeg)

![Helsingør](images/helsingoer-09.jpeg)

![Helsingør](images/helsingoer-10.jpeg)

![Helsingør](images/helsingoer-11.jpeg)


## Göteborg

Nous sommes ensuite allés à [Göteborg][goeteborg], qui était la dernière étape
pour moi avant de rentrer à Uppsala car mes deux semaines de vacances se
terminaient. Mais Rebecca et ses parents ont continué deux jours de plus jusqu'à
[Strömstad][stroemstad], un peu plus au nord.

Nous avons commencé par faire un tour en bateau sur le fleuve
[Göta älv][goeta-aelv] qui traverse la ville :

![Göteborg vue depuis le fleuve Göta älv](images/goeteborg-01.jpeg)

![Göteborg vue depuis le fleuve Göta älv](images/goeteborg-02.jpeg)

![Göteborg vue depuis le fleuve Göta älv](images/goeteborg-03.jpeg)

Cette ancienne grue, maintenant seulement décorative, était impressionante (les
immeubles à côté sont plus petits !) :

![Göteborg vue depuis le fleuve Göta älv](images/goeteborg-04.jpeg)

Et elle donne une bonne échelle pour se faire une idée de la taille des ferries
qui desservent la ville :

![Göteborg vue depuis le fleuve Göta älv](images/goeteborg-05.jpeg)

Et ces immeubles, cette grue et les ferries sont bien petits comparés à ce pont
qui enjambe le fleuve :

![Göteborg vue depuis le fleuve Göta älv](images/goeteborg-06.jpeg)

Passer à côté de ces choses toutes plus immenses les unes
que les autres en seulement quelques minutes donnait un peu le vertige.

Nous avons aussi visité le musée de la ville, qui présente toute son histoire
depuis la préhistoire jusqu'à nos jours. Toute l'expo était intéressante, mais
j'ai préféré la partie sur l'époque Viking :

![Le musée de Göteborg](images/goeteborg-07.jpeg)

![Le musée de Göteborg](images/goeteborg-08.jpeg)

![Le musée de Göteborg](images/goeteborg-09.jpeg)

![Le musée de Göteborg](images/goeteborg-10.jpeg)

Nous avons aussi écouté un concert d'une chorale britannique donné dans la
cathédrale, dont voilà une photo :

![La cathédrale de Göteborg](images/goeteborg-11.jpeg)

Ce n'est qu'un petit échantillon de tout ce que nous avons vu, mais donne un bon
aperçu. C'était un voyage bien agréable, j'ai aimé nous déplacer uniquement en
train, bus et bateau. Nous n'avons pas pu visiter certains endroits accessibles
seulement en voiture (comme par exemple la côte du sud de la Scanie, apparemment
très jolie), mais ça sera pour un prochain voyage.


[landskrona]: https://fr.wikipedia.org/wiki/Landskrona

[kalmar]: https://fr.wikipedia.org/wiki/Kalmar

[midsommar]: https://fr.wikipedia.org/wiki/Midsummer

[kalmar-union]: https://fr.wikipedia.org/wiki/Union_de_Kalmar

[oeland]: https://fr.wikipedia.org/wiki/%C3%96land

[karlskrona]: https://fr.wikipedia.org/wiki/Karlskrona

[aspoe]: https://fr.wikipedia.org/wiki/Asp%C3%B6_(archipel_de_Blekinge)

[stenshamn]: https://maps.app.goo.gl/y7akPseTMAYWgwxV7

[archipel]: https://fr.wikipedia.org/wiki/Archipel_de_Blekinge

[lund]: https://fr.wikipedia.org/wiki/Lund_(Su%C3%A8de)

[lund-cathedrale]: https://fr.wikipedia.org/wiki/Cath%C3%A9drale_de_Lund

[helsingborg]: https://fr.wikipedia.org/wiki/Helsingborg

[kaernan]: https://fr.wikipedia.org/wiki/K%C3%A4rnan

[oeresund]: https://fr.wikipedia.org/wiki/%C3%98resund

[helsingoer]: https://fr.wikipedia.org/wiki/Elseneur

[kronborg]: https://fr.wikipedia.org/wiki/Ch%C3%A2teau_de_Kronborg

[goeteborg]: https://fr.wikipedia.org/wiki/G%C3%B6teborg

[stroemstad]: https://fr.wikipedia.org/wiki/Str%C3%B6mstad

[goeta-aelv]: https://fr.wikipedia.org/wiki/G%C3%B6ta_%C3%A4lv
