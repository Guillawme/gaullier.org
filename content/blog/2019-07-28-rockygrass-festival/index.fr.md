---
title: RockyGrass Festival
author: Guillaume Gaullier
date: '2019-07-28'
slug: rockygrass-festival
categories: []
tags:
  - music
  - Colorado
lastmod: ~
authors:
  - guillaume
---


Hier nous sommes allés à [*RockyGrass*][rockygrass], un festival de musique qui
a lieu chaque été à Lyons.

<!--more-->

Voilà une photo pour donner une idée de l'endroit (il y avait beaucoup plus de
monde derrière nous, et une autre scène plus petite ailleurs) :

![RockyGrass](images/rockygrass.jpg)

Tous les groupes étaient bons, mais j'ai particulièrement apprécié
[*Che Apalache*][cheapalache], un groupe de bluegrass avec des influences
d'Amérique Latine.

{{< youtube id="zNStoTSXhsw" class="embedded-content" >}}


[rockygrass]: https://bluegrass.com/rockygrass/

[cheapalache]: https://www.cheapalache.com/
