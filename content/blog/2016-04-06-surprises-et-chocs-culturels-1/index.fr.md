---
title: "Surprises et chocs culturels - 1"
date: 2016-04-06
lastmod:
authors: [guillaume]
categories: []
tags: [relocation, food, Colorado]
slug: surprises-et-chocs-culturels-1
---


Lorsque l'on déménage sur un autre continent, on est nécessairement amené
à observer des différences avec son pays d'origine, notamment des différences
culturelles (et c'est ce qui rend l'expérience intéressante !). En seulement
cinq jours j'ai déjà pu en faire l'expérience un bon nombre de fois.
J'écrirai ces expériences dans des articles séparés, car elles n'ont pas
beaucoup de rapport avec ce que je peux raconter dans des articles de type
"journal de bord". Le format sera un peu désordonné car j'écrirai simplement ce
dont je peux me souvenir. Je soupçonne qu'il y aura plusieurs articles sur les
différences culturelles, alors je vais d'ores et déjà commencer à numéroter
ces articles.

<!--more-->

La première surprise, même si je m'y attendais, c'est la faune sauvage.
Les États-Unis possèdent de vastes régions peu urbanisées, et pour cette raison
on rencontre fréquemment des animaux sauvages. Venant de la France où il reste
assez peu (voire pas du tout ?) de zones complètement sauvages, c'est assez
surprenant. Par exemple, peu après mon arrivée Marta m'a expliqué que les bacs
poubelles utilisés ici sont *bear proof*, ce qui signifie "résistant aux
ours"... Le couvercle est verrouillé et ne peut s'ouvrir qu'en appuyant
simultanément sur deux leviers (ce que les ours ne peuvent apparemment pas
faire). Quelqu'un m'a aussi raconté qu'il est déjà arrivé qu'un élan se balade
dans la rue piétonne *Pearl Street*. Même s'il est rare de voir des ours et des
élans (je n'en ai pas encore vus), tout ça parait complètement délirant quand on
vient d'un pays où il n'y a que très peu d'animaux sauvages (ou seulement des
petits). Il est par contre fréquent de croiser des petits animaux :
ratons-laveurs, renards, et surtout beaucoup d'écureuils, y compris en pleine
ville. Voilà une photo de l'un d'eux :

![Écureuil](images/ecureuil.jpg)

Le premier choc culturel, ce sont les repas. Je ne critique pas la cuisine
américaine : les clichés que l'on s'en fait existent ici comme en France (nous
avons importé beaucoup de leurs enseignes de *fast food*), mais d'après ce que
j'ai pu voir les gens achètent des aliments sains et cuisinent des plats
normaux. Le choc culturel vient plutôt de l'aspect social des repas. En France
cet aspect est très ancré : à moins de vivre seul, chaque repas est prétexte
à passer du temps avec d'autres gens. Ici, les gens ont plus tendance à manger
dans leur coin. Je me suis surpris à attendre les gens pour manger, avant de
m'apercevoir qu'ils ne se soucient pas du tout de commencer en même temps que
moi. C'est une situation assez étrange et difficile à décrire, mais je m'y
adapterai certainement.

Une différence notable, en tout cas dans mon quotidien, concerne les bus. Bon,
déjà en France la première fois que j'ai pris le bus je ne savais pas qu'il
fallait appuyer sur un bouton pour demander un arrêt... Je connaissais le métro
qui s'arrête à chaque station, alors pour moi il était naturel que le bus fasse
pareil. En fait non. Ici c'est pareil, il faut demander l'arrêt. Sauf qu'il n'y
a pas de bouton ! Il faut en fait tirer sur un câble qui est tendu
horizontalement le long des fenêtres du bus. Ce n'est pas évident à deviner, et
c'est seulement indiqué par de minuscules étiquettes collées près du câble.
Avant de voir quelqu'un s'en servir, j'étais convaincu que ce câble servait
juste à se tenir... Les bus ne rendent pas la monnaie : le chauffeur n'a pas de
caisse, et on met les sous dans une machine qui ne rend pas la monnaie, donc si
on n'a pas l'appoint pour le ticket il faut considérer l'excédent comme un don
à la compagnie de transports (évidemment on ne peut pas mettre moins que le prix
du ticket). En parlant de ticket, il n'y en a pas (du moins quand on paye le
trajet dans le bus, je crois qu'il est possible d'acheter des carnets de tickets
quelque part), il faut demander un reçu si on tient absolument à avoir un bout
de papier avec soi pendant le trajet. Il faut aussi demander un ticket de
transfert si on souhaite emprunter une autre ligne de bus. Enfin, à Boulder les
lignes de bus qui parcourent la ville ne sont pas désignées par des numéros mais
par des noms (celles qui desservent les villes voisines sont désignées par des
numéros). C'est assez pratique une fois qu'on a compris la logique.

Nouveau choc culturel : les gens en général ont l'air super heureux au travail.
Vraiment tout le monde : les chauffeurs de bus, les personnels des caisses des
magasins, les personnels de l'université qui me donnent toujours plus de
paperasses à remplir, les collègues du labo, les bibliothécaires, etc. Ils sont
surtout heureux de pouvoir rendre service (par exemple, le chauffeur du bus me
conduisant de l'aéroport de Denver jusqu'à Boulder était super heureux de porter
mes deux valises de 23 kg) ou de pouvoir échanger quelques mots sympas (comme
une caissière à *Goodwill* qui s'intéressait à mon arrivée récente ici).
C'est assez agréable de croiser toute la journée des gens souriants, alors qu'à
Paris tout le monde semble mettre un point d'honneur à détester son boulot (ou
le boulot en général).
