---
title: "Rando dans un décor de western"
date: 2017-07-17
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, hiking]
slug: rando-decor-western
---


Je n'ai pas beaucoup le temps d'écrire sur le blog en ce moment, alors que j'ai
beaucoup de belles photos qui s'accumulent... Ce sera donc une note très courte.

<!--more-->

C'était fin juin, nous avons fait une rando très sympa près de [Lyons][lyons],
et le paysage ressemblait beaucoup à un décor de western :

![Décor de western](images/western-1.jpg)

![Décor de western](images/western-2.jpg)

![Décor de western](images/western-3.jpg)


[lyons]: https://www.google.com/maps/place/Lyons,+CO/@40.2233476,-105.2859206,14z/data=!3m1!4b1!4m5!3m4!1s0x876bdff42b432cab:0x8faa337863246c0a!8m2!3d40.2247075!4d-105.271378?hl=en
