---
title: New preprint
author: Guillaume Gaullier
date: '2019-11-19'
slug: new-preprint
categories:
  - Preprints
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
---


The results of the main project I have been working on for the past three years
went online yesterday in a preprint at [bioRxiv][biorxiv]. The full reference is:

**Gaullier G**, Roberts G, Muthurajan UM, Bowerman S, Rudolph J, Mahadevan J,
Jha A, Rae PS & Luger K (2019) Bridging of nucleosome-proximal DNA double-strand
breaks by PARP2 enhances its interaction with HPF1. *bioRxiv*: **846618**.
{{< doi "10.1101/846618" >}}

I also added it to the [publications page][publications].

[biorxiv]: https://www.biorxiv.org

[publications]: {{< relref path="/page/publications" lang="en" >}}
