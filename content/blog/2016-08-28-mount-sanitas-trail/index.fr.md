---
title: "Mount Sanitas Trail"
date: 2016-08-28
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, hiking]
slug: mount-sanitas-trail
---


Voilà quelques photos d'une randonnées que nous avons faite il y a une semaine :
le [*Mount Sanitas Trail*][mount-sanitas-trail].

<!--more-->

![Mount Sanitas Trail](images/hike-1.jpg)

![Mount Sanitas Trail](images/hike-2.jpg)

![Mount Sanitas Trail](images/hike-3.jpg)

![Mount Sanitas Trail](images/hike-4.jpg)

![Mount Sanitas Trail](images/hike-5.jpg)

![Mount Sanitas Trail](images/hike-6.jpg)

![Mount Sanitas Trail](images/hike-7.jpg)

![Mount Sanitas Trail](images/hike-8.jpg)

![Mount Sanitas Trail](images/hike-9.jpg)

![Mount Sanitas Trail](images/hike-10.jpg)


[mount-sanitas-trail]: https://bouldercolorado.gov/osmp/mount-sanitas-trailhead
