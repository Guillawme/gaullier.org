---
title: "Boulder Farmers Market"
date: 2016-05-22
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, food]
slug: boulder-farmers-market
---


Ces dernières semaines la météo était assez frustrante : il a fait beau en
semaine, et gris avec de la pluie du vendredi soir au lundi matin... Ce week-end
est le premier à être ensoleillé depuis un mois ! J'ai profité de ça pour
finalement aller au *farmers market* qui a lieu le samedi matin.

<!--more-->

![Boulder Farmers Market](images/farmers-market.jpg)

J'adore la mention "*established 1987*" qui rappelle que les États-Unis sont un
pays très jeune, et particulièrement l'état du Colorado qui a intégré l'Union en
1876, soit cent ans après la déclaration d'indépendance des États-Unis.
En Europe un panneau comme celui-là indiquerait 1887, ou même plus ancien.

Cela faisait un moment que je voulais parcourir ce marché. La première fois que
je suis passé à côté c'était il y a un mois juste après avoir acheté le vélo,
mais c'était en début d'après-midi et tous les exposants étaient en train de
partir. J'y suis retourné par un samedi pluvieux pour déjeûner avec Marta.
Et finalement hier par un temps splendide (toutes les photos n'ont pas été
prises ce week-end, comme on peut le deviner avec la météo).

Je ne ferai pas toutes mes courses ici car tout est assez cher... mais rien que
pour le pain ça vaut la peine car on peut trouver du *vrai* pain ici.

![Du vrai pain](images/vrai-pain.jpg)

Voilà quelques photos prises dans le marché :

![Des tomates](images/tomates.jpg)

![Des champignons](images/champignons.jpg)

![Des fleurs](images/fleurs.jpg)

![Des desserts](images/desserts.jpg)

Il y a aussi dans ce marché plein de commerces pour manger sur place (ce que
j'ai fait avec Marta la semaine dernière), et de la musique live. Je ne sais pas
si c'est une scène ouverte ou un groupe engagé pour animer le marché.
