---
title: New publication
author: Guillaume Gaullier
date: '2020-12-22'
slug: new-publication
categories:
  - Publications
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
---


An article I contributed to was published just today. My contribution was to
help collect the cryoEM data, process it, analyze the resulting map, build the
atomic model and deposit the map and models into the EMDB and PDB. Here is the
full reference:

Lehmann LC, Bacic L, Hewitt G, Brackmann K, Sabantsev A, **Gaullier G**,
Pytharopoulou S, Degliesposti G, Okkenhaug H, Tan S, Costa A, Skehel JM,
Boulton SJ & Deindl S (2020) Mechanistic Insights into Regulation of the ALC1
Remodeler by the Nucleosome Acidic Patch. *Cell Reports* **33**
{{< doi "10.1016/j.celrep.2020.108529" >}}

I added it to the [publications page][publications].


[publications]: {{< relref path="/page/publications" lang="en" >}}
