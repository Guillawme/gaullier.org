---
title: Spanish Peaks Festival
author: Guillaume Gaullier
date: '2018-09-25'
slug: spanish-peaks-festival
categories: []
tags:
  - hiking
  - Colorado
lastmod: ~
authors:
  - guillaume
---


Le week-end dernier nous sommes allés dans le sud du Colorado, dans un petit
village appelé [*La Veta*][laveta] où avait lieu le [*Spanish Peaks
International Celtic Music Festival*][festival]. Le festival tient son nom de
[deux montagnes d'origine volcanique][peaks] situées tout près. Voilà quelques
photos prises dans les environs, entre les concerts et ateliers de musique.

<!--more-->

Voilà une vue depuis l'entrée du village :

![La Veta](images/la-veta-1.jpg)

C'est un endroit très isolé. Les routes secondaires sont en terre et les
animaux se promènent partout :

![La Veta](images/la-veta-2.jpg)

Voilà une vue des montagnes en soirée, alors que nous partions pour la ville
voisine de Walsenburg pour aller écouter un des concerts :

![Spanish Peaks](images/peaks.jpg)

Et voilà quelques photos prises pendant une petite promenade au pied des
montagnes. Je ne me lasse pas de photographier les peupliers au début de
l'automne, ces couleurs sont vraiment magnifiques.

![Randonnée](images/rando-1.jpg)

![Randonnée](images/rando-2.jpg)

![Randonnée](images/rando-3.jpg)

![Randonnée](images/rando-4.jpg)

![Randonnée](images/rando-5.jpg)

![Randonnée](images/rando-6.jpg)

![Randonnée](images/rando-7.jpg)


[laveta]: https://goo.gl/maps/6EisiLc1iWK2

[festival]: http://celticmusicfest.com/

[peaks]: https://en.wikipedia.org/wiki/Spanish_Peaks
