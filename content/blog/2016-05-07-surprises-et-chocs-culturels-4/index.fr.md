---
title: "Surprises et chocs culturels - 4"
date: 2016-05-07
lastmod:
authors: [guillaume]
categories: []
tags: [relocation, food]
slug: surprises-et-chocs-culturels-4
---


Nouveaux chocs culturels.

<!--more-->

Aux États-Unis il n'y a pas de jours fériés en mai... C'est un peu déroutant
quand on s'attend à avoir un mois parsemé de longs week-ends. Mais on s'y fait.

Je dois avoir un peu le mal du pays sans me l'avouer, car récemment j'ai craqué
en faisant des courses : j'ai acheté 4 pains différents...

![Pains](images/pains.jpg)

Les deux au centre ne méritent même pas d'être appelés du pain. Les deux autres
sont très moyens. Là où je fais les courses ils vendent aussi des *French
breads* et *French baguettes*, mais ça ressemble à ce qu'on peut trouver en
France dans la grande distribution et ça n'atteint même pas la qualité du pain
le plus simple qu'on trouve en boulangerie. Heureusement il y a le *Dave's
killer bread* qui sauve un peu la situation : c'est un pain de mie bio et
complet avec des céréales entières, une fois grillé c'est excellent. Il doit
aussi y avoir du pain correct au *farmers market* de Boulder, il faudra que
j'aille l'explorer un de ces jours.
