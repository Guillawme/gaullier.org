---
title: "Surprises et chocs culturels - 3"
date: 2016-04-26
lastmod:
authors: [guillaume]
categories: []
tags: [relocation]
slug: surprises-et-chocs-culturels-3
---


Nouvelles surprises.

<!--more-->

Le Colorado est le premier des 50 états qui a légalisé la consommation du
cannabis. Ainsi, il flotte de temps en temps une odeur d'*herbe* en pleine rue
(même pas dans des endroits isolés). Depuis presque un mois que je suis ici,
j'ai senti ça quatre ou cinq fois environ, c'est donc assez courant.

Je ne suis pas encore complètement habitué à l'altitude : c'est surprenant de
s'essoufler en vélo sur du plat, sans forcer...

Ici le niveau "rez-de-chaussée" est numéroté 1, et il m'arrive encore de me
tromper quand on m'indique un étage : je vais un étage trop haut. Encore une de
ces habitudes qui changera difficilement (c'est tellement logique pour moi de
devoir monter quand on m'indique le premier étage).
