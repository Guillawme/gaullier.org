---
title: Färnebofjärden National Park
author: Guillaume Gaullier
date: '2021-01-31'
slug: farnebofjarden-national-park
categories: []
tags:
  - hiking
  - sweden
lastmod: ~
authors:
  - guillaume
---


Nous n'avons quasiment rien fait depuis [notre visite d'Öregrund][oregrund] il y
a presque un an, à part des promenades aux alentours immédiats d'Uppsala (à
cause de la pandémie). Hier, un collègue à moi nous a emmenés faire une rando au
[parc national de Färnebofjärden][farnebofjarden].

Il faisait très beau, mais aussi pas plus chaud qu'environ -10ºC... Voilà
quelques photos.

![Färnebofjärden](images/farnebofjarden-01.jpeg)

![Färnebofjärden](images/farnebofjarden-02.jpeg)

![Färnebofjärden](images/farnebofjarden-03.jpeg)

Les parcs en Suède ont souvent des aires de picnic équipées pour griller :

![Färnebofjärden](images/farnebofjarden-04.jpeg)

![Färnebofjärden](images/farnebofjarden-05.jpeg)

![Färnebofjärden](images/farnebofjarden-06.jpeg)

Des traces d'oiseaux étonnantes sur la poudreuse qui recouvrait le lac gelé :

![Färnebofjärden](images/farnebofjarden-07.jpeg)

![Färnebofjärden](images/farnebofjarden-08.jpeg)

![Färnebofjärden](images/farnebofjarden-09.jpeg)

![Färnebofjärden](images/farnebofjarden-10.jpeg)


[oregrund]: {{< relref path="2020-04-26-oregrund" >}}

[farnebofjarden]: https://fr.wikipedia.org/wiki/Parc_national_de_F%C3%A4rnebofj%C3%A4rden
