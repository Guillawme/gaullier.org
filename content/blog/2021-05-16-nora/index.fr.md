---
title: Nora
author: Guillaume Gaullier
date: '2021-05-16'
slug: nora
categories: []
tags:
  - sweden
  - hiking
lastmod: ~
authors:
  - guillaume
---


Pour le pont de l'Ascension, notre ami Staffan nous a emmené dans sa
*sommarstuga* (maison d'été) près de [Nora][nora], à environ 2 heures à l'ouest
d'Uppsala.

<!--more-->

Sa maison est sur une colline juste à côté d'un lac, voilà la vue depuis son
jardin :

![Vue depuis le jardin de Staffan](images/staffan-stuga-1.jpeg)

![Vue depuis le jardin de Staffan](images/staffan-stuga-2.jpeg)

Le premier jour, nous avons visité le centre de Nora :

![Église de Nora](images/nora-1.jpeg)

![Gare de Nora](images/nora-2.jpeg)

![Gare de Nora](images/nora-3.jpeg)

![Gare de Nora](images/nora-4.jpeg)

![Gare de Nora](images/nora-5.jpeg)

Les deuxième et troisième jours nous avons fait des randos dans la forêt autour
de chez lui, en commençant près de cette ancienne usine (je n'ai pas bien
compris ce qu'elle fabriquait, mais apparemment beaucoup de gens travaillaient
là quand l'usine était encore en fonctionnement) :

![Randonnée en forêt](images/rando-1.jpeg)

![Randonnée en forêt](images/rando-2.jpeg)

![Randonnée en forêt](images/rando-3.jpeg)

Nous n'avons pas vu les castors qui habitent dans cette rivière, mais la forêt
étaient pleine de traces de leur activité, comme sur cet arbre :

![Randonnée en forêt](images/rando-4.jpeg)

![Randonnée en forêt](images/rando-5.jpeg)

![Randonnée en forêt](images/rando-6.jpeg)

![Randonnée en forêt](images/rando-7.jpeg)

À un moment la randonnée nous a fait passer à côté d'une grande maison ancienne,
et la photo suivante montre seulement la grange de cette propriété :

![Une grange immense](images/rando-8.jpeg)


[nora]: https://goo.gl/maps/G7dxNXnZrTVu4iVp8
