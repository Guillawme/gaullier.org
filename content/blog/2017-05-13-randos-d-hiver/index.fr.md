---
title: "Randos d'hiver"
date: 2017-05-13
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, hiking]
slug: randos-d-hiver
---


Nous sommes déjà mi-mai. Au Colorado, c'est à peu près à partir de cette date
que l'on est quasiment sûr de ne plus avoir de chutes de neige ni de gel. Il est
donc grand temps que je mette en ligne des photos des quelques randonnées
hivernales que nous avons faites, puisque je ne l'ai pas encore fait.

<!--more-->

Les photos qui suivent ont été prises pendant des randonnées en raquettes au
[*Rocky Mountain National Park*][rocky-mountain], dans la station de ski
d'[Eldora][eldora] (tout près de chez nous), et à [*Caribou Town*][caribou] (un
village fantôme, aussi tout près de chez nous). Les trois premières randonnées,
à Eldora puis au parc national, étaient mi-janvier et fin janvier. La randonnée
autour de *Caribou Town* était mi-février. La dernière randonnée, au parc
national, était tout récemment : le 30 avril (juste après la dernière chute de
neige).

## Eldora

![Eldora](images/eldora-1.jpg)

![Eldora](images/eldora-2.jpg)

![Eldora](images/eldora-3.jpg)

![Eldora](images/eldora-4.jpg)

## Première rando au parc national

![Rocky Mountain National Park](images/rocky-1-1.jpg)

![Rocky Mountain National Park](images/rocky-1-2.jpg)

![Rocky Mountain National Park](images/rocky-1-3.jpg)

## Deuxième rando au parc national

![Rocky Mountain National Park](images/rocky-2-1.jpg)

![Rocky Mountain National Park](images/rocky-2-2.jpg)

![Rocky Mountain National Park](images/rocky-2-3.jpg)

![Rocky Mountain National Park](images/rocky-2-4.jpg)

## *Caribou Town*

![Caribou Town](images/caribou-1.jpg)

Le village abandonné est en pleine campagne, seule une route en terre permet d'y
arriver :

![Caribou Town](images/caribou-2.jpg)

Il ne reste plus grand chose du village :

![Caribou Town](images/caribou-3.jpg)

![Caribou Town](images/caribou-4.jpg)

![Caribou Town](images/caribou-5.jpg)

Depuis les collines aux alentours de *Caribou Town*, on peut voir la station de
ski d'Eldora :

![Caribou Town](images/caribou-6.jpg)

## Dernière rando au parc national

[*Bierstadt Lake*][bierstadt] :

![Rocky Mountain National Park](images/rocky-3-1.jpg)

![Rocky Mountain National Park](images/rocky-3-2.jpg)

![Rocky Mountain National Park](images/rocky-3-3.jpg)

*Flattop Mountain* :

![Rocky Mountain National Park](images/rocky-3-4.jpg)

Un chipmunk :

![Rocky Mountain National Park](images/rocky-3-5.jpg)


[rocky-mountain]: https://fr.wikipedia.org/wiki/Parc_national_des_Montagnes_Rocheuses

[eldora]: https://en.wikipedia.org/wiki/Eldora,_Colorado

[caribou]: https://en.wikipedia.org/wiki/Caribou,_Colorado

[bierstadt]: https://en.wikipedia.org/wiki/Bierstadt_Lake
