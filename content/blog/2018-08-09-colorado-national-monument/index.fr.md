---
title: Colorado National Monument
author: Guillaume Gaullier
date: '2018-08-09'
slug: colorado-national-monument
categories: []
tags:
  - Colorado
  - hiking
  - travels
lastmod: ~
authors:
  - guillaume
---


Le dernier week-end de juillet, nous avons pris quatre jours pour faire un petit
voyage en camping sur le versant ouest des Rocheuses. Nous avons pu aller
jusqu'au [*Colorado National Monument*][monument], près de la frontière avec
l'Utah.

<!--more-->

Pour commencer, voilà une vue d'ensemble du parcours :

<div class="embedded-content">
<iframe width="100%" height="300px" frameBorder="0" src="https://framacarte.org/en/map/western-slope-2018_29118?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>
</div>

Nous sommes partis de Boulder en suivant [I-70][i70] vers l'ouest. C'est une
grande autoroute, mais à travers les Rocheuses elle passe au milieu de paysages
magnifiques, ce qui rend la circulation plus supportable. À Glenwood Springs,
nous avons quitté cette autoroute pour atteindre le minuscule village de
Redstone, notre première étape, et planter notre tente dans ce décor :

![Redstone Campground](images/redstone-1.jpg)

Redstone est minuscule, il n'y a qu'une seule rue le long d'une rivière et
seulement une centaine d'habitants :

![Redstone](images/redstone-2.jpg)

Juste à la sortie du village se trouvent d'anciens fours à charbon, utilisés
pour produire [un dérivé du charbon appelé coke][coke] :

![Redstone](images/redstone-3.jpg)

![Redstone](images/redstone-4.jpg)

Le lendemain matin, nous nous sommes levés tôt pour aller prendre un bain dans
une source chaude appelée *Penny Hot Springs*, toute proche du camping en
remontant la rivière. Un petit ruisseau sort des rochers à un endroit de la
route, et son eau est littéralement brûlante :

![Penny Hot Springs](images/redstone-5.jpg)

Ce ruisseau coule vers la rivière, et plusieurs piscines ont été aménagées avec
des rochers pour donner des mélanges plus ou moins chauds entre l'eau chaude de
la source et l'eau froide de la rivière :

![Penny Hot Springs](images/redstone-6.jpg)

![Penny Hot Springs](images/redstone-7.jpg)

![Penny Hot Springs](images/redstone-8.jpg)

Nous avons repris la route tôt le matin pour Paonia. Les paysages entre Redstone
et Paonia sont splendides :

![Sur la route](images/road-1.jpg)

Enfin, pas partout, car il y a aussi quelques mines de charbon dans cette
région... (oui ce tas noir énorme, c'est du charbon ; et je n'ai pas de photo,
mais il y avait aussi les installations de la mine pas loin) :

![Sur la route](images/road-2.jpg)

Paonia est plus grande que Redstone, mais tout de même aussi une petite ville.
Nous nous y sommes arrêtés quelques heures pour prendre un petit déjeûner à
[*The Living Farm Cafe*][livingfarm], et en visitant un peu la ville nous avons
trouvé le studio de [ce photographe][photographe] qui a entre autres des photos
impressionnates des paysages locaux.

C'est à partir d'ici et plus à l'ouest qu'il y a des vergers réputés pour leurs
pêches délicieuses, comme un peu plus loin sur la route près de Hotchkiss :

![Verger à Hotchkiss](images/orchards-1.jpg)

À partir des environs de Paonia et Hotchkiss, les paysages changent beaucoup à
mesure que l'on s'éloigne des montagnes. Voilà quelques photos prises sur la
route :

![Sur la route](images/road-3.jpg)

![Sur la route](images/road-4.jpg)

![Sur la route](images/road-5.jpg)

![Sur la route](images/road-6.jpg)

À Hotchkiss, plutôt que de continuer vers l'ouest vers Delta et Grand Junction,
nous avons décidé de faire un détour par le [*Black Canyon of the Gunisson
National Park*][black-canyon]. Eh bien ce détour valait la peine, car cet
endroit est très impressionnant ; à vrai dire, encore plus impressionnant que le
[*Glenwood Canyon*][glenwood-canyon] que nous avons traversé sur l'autoroute
juste avant d'arriver à Glenwood Springs. Ces photos ne donnent qu'une vague
idée de l'immensité de ce canyon (et ce n'est même pas le célèbre *Grand
Canyon*...) :

![Black Canyon](images/black-canyon-1.jpg)

![Black Canyon](images/black-canyon-2.jpg)

![Black Canyon](images/black-canyon-3.jpg)

![Black Canyon](images/black-canyon-4.jpg)

Après cet endroit, toute la région au sud de la rivière ressemble à ces collines
arides jusqu'à Montrose :

![Sur la route](images/road-7.jpg)

![Sur la route](images/road-8.jpg)

De Montrose à Grand Junction, c'est un plateau lui aussi très sec, avec des deux
côtés une vue sur des montagnes : les Rocheuses à l'est, et à l'ouest de grands
plateaux parmi lesquels finalement le *Colorado National Monument*. Et c'est là
que nous avons vus les paysages les plus grandioses de ce court voyage. Nous
sommes entrés dans le monument par le sud-est, et à peine arrivés en haut du
plateau nous nous sommes arrêtés fréquemment le long de la route.

![Colorado National Monument](images/colorado-monument-1.jpg)

![Colorado National Monument](images/colorado-monument-2.jpg)

![Colorado National Monument](images/colorado-monument-3.jpg)

![Colorado National Monument](images/colorado-monument-4.jpg)

![Colorado National Monument](images/colorado-monument-5.jpg)

La route croise à plusieurs endroits des chemins de randonnée. Nous avons fait
une petite promenade jusqu'en bas d'un des canyons, en début de soirée avec une
lumière incroyable.

![Colorado National Monument](images/colorado-monument-6.jpg)

![Colorado National Monument](images/colorado-monument-7.jpg)

![Colorado National Monument](images/colorado-monument-8.jpg)

Le contraste entre le plateau aride et la forêt qui remplit le fond des canyons
est surprenant :

![Colorado National Monument](images/colorado-monument-9.jpg)

![Colorado National Monument](images/colorado-monument-10.jpg)

La vue depuis certains endroits sur le plateau est grandiose, avec la vallé puis
les Rocheuses en arrière plan :

![Colorado National Monument](images/colorado-monument-11.jpg)

Et les points de vue changent après chaque canyon que nous passons :

![Colorado National Monument](images/colorado-monument-12.jpg)

Nous avons passé la deuxième nuit en camping dans le monument, au *Saddlehorn
Campground*. Le lendemain matin, nous avons fait une nouvelle promenade sur le
plateau avant qu'il ne fasse trop chaud. Différents points de vue, différente
lumière, toujours aussi grandiose :

![Colorado National Monument](images/colorado-monument-13.jpg)

![Colorado National Monument](images/colorado-monument-14.jpg)

![Colorado National Monument](images/colorado-monument-15.jpg)

![Colorado National Monument](images/colorado-monument-16.jpg)

![Colorado National Monument](images/colorado-monument-17.jpg)

![Colorado National Monument](images/colorado-monument-18.jpg)

![Colorado National Monument](images/colorado-monument-19.jpg)

![Colorado National Monument](images/colorado-monument-20.jpg)

![Colorado National Monument](images/colorado-monument-21.jpg)

Nous avons ensuite roulé jusqu'à Palisade, où nous avons passé le reste de la
journée. Cette ville est traversée par le [fleuve Colorado][colorado-river],
dont l'eau est plutôt basse à cet endroit et à cette saison :

![Fleuve Colorado](images/palisade-1.jpg)

Palisade est une petite ville bien calme, surtout un dimanche après-midi. Voilà
une vue de *Main Street* (le centre-ville est à 5 blocs dans cette direction) :

![Palisade](images/palisade-2.jpg)

Nous avons goûté des cidres au [*13 Brix Cider Bistro*][13brix] puis dîné au
[*Palisade Café 11.0*][palisade-cafe]. Nous avons passé la troisième et dernière
nuit du voyage au *Dreamcatcher B&B* :

![Dreamcatcher B&B](images/dreamcatcher-bnb.jpg)

Cette chambre d'hôtes était idéalement située sur la route qui traverse la
colline où se trouvent la plupart des vergers de Palisade. Le matin du dernier
jour, avant de prendre la route pour rentrer à Boulder, nous avons donc fait le
plein de pêches ! Certains vergers permettent de cueillir soi-même, ce que nous
avont fait. Nous avons récolté un peu plus de 12 kg au total... plus deux
caisses achetées dans un autre verger... oui nous adorons les pêches ! Celles de
cette région sont délicieuses, et coûtent le double au marché chez nous, donc
nous nous sommes fait plaisir. Voilà une partie de notre récolte :

![Pêches](images/peaches-1.jpg)

Le coffre de la voiture plein de pêches :

![Pêches](images/peaches-2.jpg)

Et voilà des photos prises dans le verger où nous les avons cueillies :

![Verger à Palisade](images/orchards-2.jpg)

Et finalement, quelques photos prises sur l'autoroute pendant le trajet du
retour à Boulder :

![Sur la route](images/road-9.jpg)

![Sur la route](images/road-10.jpg)

![Sur la route](images/road-11.jpg)


[monument]: https://en.wikipedia.org/wiki/Colorado_National_Monument

[i70]: https://en.wikipedia.org/wiki/Interstate_70

[coke]: https://fr.wikipedia.org/wiki/Coke_(charbon)

[livingfarm]: http://thelivingfarmcafe.com/about/

[photographe]: https://www.lehmanimages.com/

[black-canyon]: https://en.wikipedia.org/wiki/Black_Canyon_of_the_Gunnison_National_Park

[glenwood-canyon]: https://en.wikipedia.org/wiki/Glenwood_Canyon

[colorado-river]: https://fr.wikipedia.org/wiki/Colorado_(fleuve)

[13brix]: https://www.13brixciderbistro.com/

[palisade-cafe]: https://www.palisadecafe11.com/
