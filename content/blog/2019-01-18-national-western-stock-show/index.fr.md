---
title: National Western Stock Show
author: Guillaume Gaullier
date: '2019-01-18'
slug: national-western-stock-show
categories: []
tags:
  - Colorado
  - food
lastmod: ~
authors:
  - guillaume
---


Dimanche dernier, des amis nous ont emmenés à Denver pour une nouvelle
expérience culturelle : nous sommes allés au [*National Western Stock
Show*][nwss]. C'est comparable au salon de l'agriculture, mais restreint aux
vaches et chevaux. Nous y sommes allés principalement pour le rodéo qui a lieu
pendant ce salon. Je ne suis pas certain d'avoir apprécié l'événement dans son
ensemble, parce que le bien-être animal n'y est pas vraiment une préoccupation
centrale... Mais certaines parties du spectacle étaient vraiment bien.

<!--more-->

Au [*Denver Coliseum*][coliseum] où avait lieu l'événement, l'ambiance rappelle
une fête foraine : il y a une foule dense partout, beaucoup de bruit, des
sponsors qui exposent leurs produits (principalement des tracteurs et pickups),
des exposants qui vendent des souvenirs, des sucreries et de la nourriture frite.
D'ailleurs, ça sent fort la friture partout. À mesure que nous avançons vers les
exposants de bétail, l'odeur de friture s'atténue et se transforme en odeur
d'étable... Et nous voilà littéralement dans les étables :

![Vaches](images/vaches-1.jpg)

![Vaches](images/vaches-2.jpg)

Les propriétaires de ces vaches sont très occuppés à les faire les plus belles
possibles :

![Vaches](images/vaches-3.jpg)

Car elles seront toutes évaluées par un jury qui attribuera des prix aux
meilleurs éleveurs (je ne sais pas sur quels critères) :

![Jury](images/jury.jpg)

Le spectacle principal que nous étions venus voir, c'était le rodéo. En fait
c'était surtout un spectacle équestre, avec seulement un peu de rodéo. Un groupe
[mariachi][mariachi] a animé tout le spectacle (je n'ai pas de bonne photo des
musiciens), et des danseurs et danseuses animaient les pauses entre les parties
du spectacle équestre :

![Danseuses](images/danseurs-1.jpg)

Au début, des cavaliers sont entrés et ont fait un tour de l'arène en portant
chacun un drapeau d'un état des États-Unis ou du Mexique (le Mexique est aussi
une fédération d'états) :

![Drapeaux](images/drapeaux-1.jpg)

![Drapeaux](images/drapeaux-2.jpg)

![Drapeaux](images/drapeaux-3.jpg)

Les deux derniers cavaliers portaient les drapeux des deux pays, côte à côte,
d'une façon assez touchante dans le [contexte actuel][shutdown] (le gouvernement
fédéral des États-Unis est partiellement bloqué car la chambre des représentants,
désormais à majorité démocrate, refuse de financer le projet du président de
construire un mur à la frontière avec le Mexique) :

![Drapeaux](images/drapeaux-4.jpg)

Ensuite, il y a eu plusieurs spectacles dont un cavalier et son cheval qui
dansaient (impressionant numéro de dressage), plusieurs acrobates à cheval, 
d'autres danseurs :

![Danseur à cheval](images/danseurs-2.jpg)

![Acrobates](images/acrobates-1.jpg)

![Acrobates](images/acrobates-2.jpg)

![Danseurs](images/danseurs-3.jpg)

Et bien sûr tout cela entrecoupé de phases de rodéo. Le commentateur est bien
protégé, installé dans une sorte de tonneau :

![Rodéo](images/rodeo-1.jpg)

![Rodéo](images/rodeo-2.jpg)

![Rodéo](images/rodeo-3.jpg)

![Rodéo](images/rodeo-4.jpg)

Il y a aussi eu un rodéo miniature avec des enfants de 4 à 7 ans qui devaient
tenir sur un mouton le plus longtemps possible (pauvres gamins...). La plupart
des gens dans le public venaient principalement pour le rodéo, et soutenaient un
participant en particulier : on les entendait acclamer quand les commentateurs
présentaient les participants et disaient d'où ils venaient (Colorado, Chihuahua
et d'autres états du Mexique et des États-Unis).

![Mouton](images/mouton.jpg)

Tous les enfants ont reçu un prix, peu importe combien de temps ils sont restés
accrochés à leur mouton :

![Gamins](images/gamins.jpg)

Il y a aussi eu une corrida (seulement pour le spectacle, sans mise à mort des
animaux) :

![Corrida](images/corrida-1.jpg)

![Corrida](images/corrida-2.jpg)

À la fin, tous les artistes se sont réunis dans l'arène :

![Final](images/final.jpg)

Après tout ça, comme je l'ai déjà écrit au début, je suis reparti mitigé entre
l'émerveillement procuré par tous les artistes (musiciens, danseurs, acrobates)
et le malaise provoqué par le traitement qui est fait aux animaux, en particulier
les chevaux et taureaux malmenés pour le rodéo, et aussi les vaches du concours
d'éleveurs (qui seront toutes abattues tôt ou tard...). Pour oublier ce malaise,
un des amis qui nous a emmenés au rodéo (qui est végétalien) a proposé d'aller
dîner dans un restaurant végétalien appelé *Watercourse*. Voilà la version
végétalienne du [sandwich cubain][sandwich-cubain] :

![Dîner](images/diner.jpg)

Eh bien je l'ai trouvé meilleur que l'original !


[nwss]: https://en.wikipedia.org/wiki/National_Western_Stock_Show

[coliseum]: https://en.wikipedia.org/wiki/Denver_Coliseum

[shutdown]: https://en.wikipedia.org/wiki/United_States_federal_government_shutdown_of_2018%E2%80%932019

[sandwich-cubain]: https://en.wikipedia.org/wiki/Cuban_sandwich
