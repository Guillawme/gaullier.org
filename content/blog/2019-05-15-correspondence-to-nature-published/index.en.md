---
title: Correspondence to Nature published
author: Guillaume Gaullier
date: '2019-05-15'
slug: correspondence-to-nature-published
categories:
  - Publications
tags:
  - work
lastmod: ~
---


I posted [earlier][post] that a correspondence I sent after reading a news
article in Nature did not get published, but I was wrong. The process actually
took longer than I thought it would (I sent this letter on February 26th, and
was notified of its acceptance on April 30th). This correspondence is now
[published][correspondence], heavily edited and much more concise. This was the
first time I sent a correspondence to a journal, and I discovered that the
editor I was in contact with did an excellent job to check the facts and
figures mentioned in the letter (after all, this is a scientific journal).
I will leave [my initial version][post] online, as a preprint.


[post]: {{< relref "2019-04-13-nature-on-research-funding-in-france" >}}

[correspondence]: https://doi.org/10.1038/d41586-019-01529-4
