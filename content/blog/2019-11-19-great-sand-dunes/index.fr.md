---
title: Great Sand Dunes
author: Guillaume Gaullier
date: '2019-11-19'
slug: great-sand-dunes
categories: []
tags:
  - Colorado
  - hiking
lastmod: ~
authors:
  - guillaume
---


Le week-end dernier, nous sommes allés aux [*Great Sand Dunes*][dunes]. J'ai
trouvé cet endroit encore plus impressionant que le [*Colorado National
Monument*][cnm], alors que la barre était déjà bien haut. Ces dunes sont situées
au nord-ouest des [*Spanish Peaks*][spanish-peaks] (où nous sommes allés [l'année
dernière][spanish-peaks-fest]) dans la [*San Luis Valley*][san-luis], un plateau
immense et très plat à environ 2300 m d'altitude et complètement entouré de
chaînes de montagnes. Voilà quelques photos.

<!--more-->

La vue vers l'ouest :

![Great Sand Dunes](images/great-sand-dunes-1.jpg)

La vue vers les dunes, au sud des dunes en regardant vers le nord :

![Great Sand Dunes](images/great-sand-dunes-2.jpg)

La vue vers l'est, sur la chaîne de montagnes du [*Sangre de Cristo*][sangre]:

![Great Sand Dunes](images/great-sand-dunes-3.jpg)

![Great Sand Dunes](images/great-sand-dunes-4.jpg)

![Great Sand Dunes](images/great-sand-dunes-5.jpg)

La vue vers le sud :

![Great Sand Dunes](images/great-sand-dunes-6.jpg)

![Great Sand Dunes](images/great-sand-dunes-7.jpg)

![Great Sand Dunes](images/great-sand-dunes-8.jpg)

![Great Sand Dunes](images/great-sand-dunes-9.jpg)

![Great Sand Dunes](images/great-sand-dunes-10.jpg)

Une fois en haut des premières dunes, la vue vers le nord avec encore des dunes
à perte de vue :

![Great Sand Dunes](images/great-sand-dunes-11.jpg)

![Great Sand Dunes](images/great-sand-dunes-12.jpg)

La plus haute dune :

![Great Sand Dunes](images/great-sand-dunes-13.jpg)

![Great Sand Dunes](images/great-sand-dunes-14.jpg)

La vue vers l'ouest depuis la plus haute dune :

![Great Sand Dunes](images/great-sand-dunes-15.jpg)

![Great Sand Dunes](images/great-sand-dunes-16.jpg)

![Great Sand Dunes](images/great-sand-dunes-17.jpg)

Sur le chemin du retour nous nous sommes arrêtés au [*Gators Reptile
Park*][gators], un refuge pour animaux spécialisé dans les reptiles (en
particulier alligators et crocodiles).

![Colorado Gators Reptile Park](images/gators-1.jpg)

![Colorado Gators Reptile Park](images/gators-2.jpg)


[dunes]: https://en.wikipedia.org/wiki/Great_Sand_Dunes_National_Park_and_Preserve

[cnm]: {{< relref "2018-08-09-colorado-national-monument" >}}

[spanish-peaks]: https://en.wikipedia.org/wiki/Spanish_Peaks

[spanish-peaks-fest]: {{< relref "2018-09-25-spanish-peaks-festival" >}}

[san-luis]: https://en.wikipedia.org/wiki/San_Luis_Valley

[sangre]: https://en.wikipedia.org/wiki/Sangre_de_Cristo_Mountains

[gators]: https://en.wikipedia.org/wiki/Colorado_Gators_Reptile_Park
