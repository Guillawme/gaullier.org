---
title: "Trail Ridge Road"
date: 2017-06-25
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, hiking]
slug: trail-ridge-road
---


Cela fait plus d'un mois que je n'ai rien écrit ici... alors que j'ai pourtant
plein de choses à raconter et à montrer. Nous sommes récemment retournés au
*Rocky Mountain National Park* car la route qui traverse les Rocheuses d'est en
ouest par ce parc, appelée [*Trail Ridge Road*][trail-ridge], est fermée l'hiver
et rouvre chaque année à la fin du printemps ou au début de l'été selon les
conditions d'enneigement. Cette année, c'était au début du mois de juin.
Une amie nous a conseillé d'aller voir cet endroit magnifique. Nous y sommes
allé le 4 juin, la route était complètement dégagée, mais les alentours étaient
encore sous une épaisse couche de neige, alors nous n'avons pu faire qu'une
petite promenade. Voilà quelques photos.

<!--more-->

Nous avons commencé par suivre la *Peak to Peak Highway* pour nous rendre au
parc. C'est une route magnifique aussi, le paysage en chemin ressemble à ça :

![Trail Ridge Road](images/trail-ridge-1.jpg)

![Trail Ridge Road](images/trail-ridge-2.jpg)

Une fois dans le parc, la route monte en altitude assez rapidement, et les
paysages changent beaucoup :

![Trail Ridge Road](images/trail-ridge-3.jpg)

![Trail Ridge Road](images/trail-ridge-4.jpg)

![Trail Ridge Road](images/trail-ridge-5.jpg)

![Trail Ridge Road](images/trail-ridge-6.jpg)

Il y avait encore trop de neige pour emprunter certains chemins piétons :

![Trail Ridge Road](images/trail-ridge-7.jpg)

À partir d'une certaine altitude, on peut apercevoir les sommets du
[*Continental Divide*][continental-divide] (la ligne de plus haute altitude qui
traverse le continent ; les rivières qui prennent leur source à l'est de cette
ligne se jettent toutes dans l'Atlantique, tandis que celles qui prennent leur
source à l'ouest de cette ligne se jettent dans le Pacifique) :

![Trail Ridge Road](images/trail-ridge-8.jpg)

En altitude, un arc-en-ciel peut être un cercle presque complètement fermé (?!
c'est la première fois que je vois ça) :

![Trail Ridge Road](images/trail-ridge-9.jpg)

![Trail Ridge Road](images/trail-ridge-10.jpg)

Après une courte promenade en altitude, et un certain temps dans la voiture,
nous avons finalement fait une vraie rando à une altitude plus basse, où il
faisait bien plus chaud :

![Trail Ridge Road](images/trail-ridge-11.jpg)


[trail-ridge]: https://en.wikipedia.org/wiki/Trail_Ridge_Road

[continental-divide]: https://fr.wikipedia.org/wiki/Ligne_continentale_de_partage_des_eaux_d%27Am%C3%A9rique_du_Nord
