---
title: New publication
author: Guillaume Gaullier
date: '2020-11-06'
slug: new-publication
categories:
  - Publications
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
---


My [preprint from almost exactly one year ago][preprint] is now peer-reviewed.
The article went online earlier this week. Here is the full reference:

**Gaullier G**, Roberts G, Muthurajan UM, Bowerman S, Rudolph J, Mahadevan J,
Jha A, Rae PS & Luger K (2020) Bridging of nucleosome-proximal DNA double-strand
breaks by PARP2 enhances its interaction with HPF1. *PLOS ONE* **15**: e0240932.
{{< doi "10.1371/journal.pone.0240932" >}}

I added it to the [publications page][publications].


[preprint]: {{< relref "2019-11-19-new-preprint" >}}

[publications]: {{< relref path="/page/publications" lang="en" >}}
