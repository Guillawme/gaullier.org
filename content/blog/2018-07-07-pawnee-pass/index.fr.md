---
title: "Pawnee Pass"
date: 2018-07-07T15:23:42-06:00
lastmod:
authors: [guillaume]
categories: []
tags: [hiking, Colorado]
slug: pawnee-pass
---


Mercredi dernier était un [jour férié][4th], donc une bonne occasion pour une
longue randonnée. Nous sommes allés à [*Brainard Lake*][brainard-lake], dans
l'[*Indian Peaks Wilderness area*][indian-peaks] et avons suivi un chemin
jusqu'au *Pawnee Pass*, sur le [*continental divide*][continental-divide]. C'est
un endroit splendide ! Voilà des photos.

<!--more-->

Voilà *Brainard Lake*, le lac à partir duquel partent la plupart des chemins de
randonnée dans cette zone :

![Brainard Lake](images/rando-1.jpg)

Les alentours sont magnifiques :

![Brainard Lake](images/rando-2.jpg)

![Brainard Lake](images/rando-3.jpg)

![Brainard Lake](images/rando-4.jpg)

Nous arrivons à un deuxième lac, *Lake Isabelle* :

![Brainard Lake](images/rando-5.jpg)

![Brainard Lake](images/rando-6.jpg)

![Brainard Lake](images/rando-7.jpg)

![Brainard Lake](images/rando-8.jpg)

À mesure que nous grimpons, il y a de plus en plus de rochers et de moins en
moins d'arbres :

![Brainard Lake](images/rando-9.jpg)

![Brainard Lake](images/rando-10.jpg)

![Brainard Lake](images/rando-11.jpg)

Maintenant le lac Isabelle semble tout petit :

![Brainard Lake](images/rando-12.jpg)

Nous sommes maintenant bien au dessus de l'altitude de la [limite des
arbres][tree-line] (*tree line*) :

![Brainard Lake](images/rando-13.jpg)

![Brainard Lake](images/rando-14.jpg)

![Brainard Lake](images/rando-15.jpg)

![Brainard Lake](images/rando-16.jpg)

Maintenant, on aperçoit le col *Pawnee Pass*, la fin de notre rando (ou plutôt
la moitié, car il faut encore tout redescendre) :

![Brainard Lake](images/rando-17.jpg)

Ce col est à environ 3 825 mètres d'altitude, et situé sur le *continental
divide* (les rivières qui prennent leur source à l'ouest du col coulent vers le
Pacifique) :

![Brainard Lake](images/rando-18.jpg)

À l'ouest du col, toujours des montagnes à perte de vue:

![Brainard Lake](images/rando-19.jpg)

À l'est du col, on peut apercevoir la plaine qui n'est pas si loin :

![Brainard Lake](images/rando-20.jpg)

Finalement, en voyant que la météo changeait, nous sommes redescendus
rapidement. Finalement ces nuages sont partis vers le sud-est et ne nous ont pas
posé de problèmes (la pluie n'est pas un problème, mais la foudre est beaucoup
plus dangereuse quand on se trouve plus haut que l'altitude à laquelle les
arbres arrêtent de pousser...).

![Brainard Lake](images/rando-21.jpg)

À cette époque de l'année, il y a plein de fleurs sauvages magnifiques partout.
Voilà toutes les photos de fleurs pour finir :

![Fleurs sauvages](images/fleurs-1.jpg)

![Fleurs sauvages](images/fleurs-2.jpg)

![Fleurs sauvages](images/fleurs-3.jpg)

![Fleurs sauvages](images/fleurs-4.jpg)

![Fleurs sauvages](images/fleurs-5.jpg)

![Fleurs sauvages](images/fleurs-6.jpg)

![Fleurs sauvages](images/fleurs-7.jpg)

![Fleurs sauvages](images/fleurs-8.jpg)

![Fleurs sauvages](images/fleurs-9.jpg)

![Fleurs sauvages](images/fleurs-10.jpg)

![Fleurs sauvages](images/fleurs-11.jpg)

![Fleurs sauvages](images/fleurs-12.jpg)

![Fleurs sauvages](images/fleurs-13.jpg)

![Fleurs sauvages](images/fleurs-14.jpg)

![Fleurs sauvages](images/fleurs-15.jpg)

![Fleurs sauvages](images/fleurs-16.jpg)

![Fleurs sauvages](images/fleurs-17.jpg)

![Fleurs sauvages](images/fleurs-18.jpg)

![Fleurs sauvages](images/fleurs-19.jpg)

![Fleurs sauvages](images/fleurs-20.jpg)

![Fleurs sauvages](images/fleurs-21.jpg)

![Fleurs sauvages](images/fleurs-22.jpg)


[4th]: https://fr.wikipedia.org/wiki/Jour_de_l%27Ind%C3%A9pendance_(%C3%89tats-Unis)

[brainard-lake]: https://goo.gl/maps/wHDEyAdrTiK2

[continental-divide]: https://fr.wikipedia.org/wiki/Ligne_continentale_de_partage_des_eaux_d%27Am%C3%A9rique_du_Nord


[indian-peaks]: https://en.wikipedia.org/wiki/Indian_Peaks_Wilderness

[tree-line]: https://fr.wikipedia.org/wiki/Limite_des_arbres#Types_de_limites
