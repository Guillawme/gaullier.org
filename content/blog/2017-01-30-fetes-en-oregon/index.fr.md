---
title: "Fêtes de fin d'année en Oregon"
date: 2017-01-30
lastmod:
authors: [guillaume]
categories: []
tags: [travels, Oregon]
slug: fetes-en-oregon
---


Cet article arrive bien en retard... Mais je voulais tout de même mettre ici
quelques photos prises pendant la semaine de Noël et du jour de l'an. Nous avons
passé les fêtes en Oregon, chez les parents de Rebecca.

<!--more-->

Nous avons visité un petit parc appelé [*McDowell Creek County Park*][mcdowell] :

![McDowell Creek County Park](images/mcdowell-1.jpg)

![McDowell Creek County Park](images/mcdowell-2.jpg)

![McDowell Creek County Park](images/mcdowell-3.jpg)

![McDowell Creek County Park](images/mcdowell-4.jpg)

![McDowell Creek County Park](images/mcdowell-5.jpg)

![McDowell Creek County Park](images/mcdowell-6.jpg)

![McDowell Creek County Park](images/mcdowell-7.jpg)

![McDowell Creek County Park](images/mcdowell-8.jpg)

Nous sommes aussi allés sur la côte, près de la ville de [*Newport*][newport] :

![Newport](images/newport-1.jpg)

![Newport](images/newport-2.jpg)

![Newport](images/newport-3.jpg)

![Newport](images/newport-4.jpg)

![Newport](images/newport-5.jpg)

![Newport](images/newport-6.jpg)

![Newport](images/newport-7.jpg)

![Newport](images/newport-8.jpg)

![Newport](images/newport-9.jpg)


[mcdowell]: https://goo.gl/maps/NtTsevmR1B22

[newport]: https://goo.gl/maps/g6SVtNB4yG12
