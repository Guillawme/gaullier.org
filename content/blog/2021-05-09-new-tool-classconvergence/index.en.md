---
title: 'New tool: classconvergence'
author: Guillaume Gaullier
date: '2021-05-09'
slug: new-tool-classconvergence
categories:
  - Software
tags:
  - work
  - Python
  - learning
  - cryo-em
  - visualization
lastmod: ~
authors:
  - guillaume
---


Last week, I finally took time to finish and upload another tool for helping
analysis of cryo-EM data. This new tool is called `classconvergence` and does
the same as [`countparticles`][countparticles], but for every iteration of a 2D
or 3D classification job from RELION instead of a single iteration. It then
plots the number of particles per class as a function of iteration number. This
is convenient to check whether a classification has converged (after which the
number of particles per class stops changing).

The tool can be installed [with pip][pypi-project], cited with
{{< doi "10.5281/zenodo.4732050" >}} and the code is available [here][repo].


[countparticles]: {{< relref "2020-11-06-new-tool-countparticles" >}}

[pypi-project]: https://pypi.org/project/classconvergence

[repo]: https://github.com/Guillawme/classconvergence
