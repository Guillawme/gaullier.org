---
title: Voyage au Bélize
author: Guillaume Gaullier
date: '2020-01-18'
slug: voyage-au-belize
categories: []
tags:
  - travels
  - hiking
  - belize
lastmod: ~
authors:
  - guillaume
---


Nous avons beaucoup voyagé ces dernières semaines. Pour Noël nous sommes allés
six jours en Oregon, où nous avons passé la plupart du temps chez le grand-père
de Rebecca. Ensuite nous sommes allés au [Bélize][belize] du 30 décembre au 12
janvier. Nous avons passé les quatre premiers jours avec la mère de Rebecca à
[San Ignacio][san-ignacio], puis nous sommes allés à [Hopkins][hopkins], puis
nous avons retrouvé le reste de sa famille à [Placencia][placencia].

<!--more-->

## San Ignacio
### 31 décembre

Le premier jour a été très difficile car il fallait s'adapter à un climat
tropical chaud et humide en venant du climat très froid (à cette saison) et sec
du Colorado. J'ai presque fait un malaise pendant la première visite, mais
heureusement j'ai pu m'assoir et rien n'est arrivé.

À San Ignacio, nous avons commencé par visiter le site [Maya][maya] de [*Cahal
Pech*][cahal-pech]. Le site est sur une colline en surplomb de la ville. Voilà la
vue de la ville depuis *Cahal Pech* :

![San Ignacio vue depuis Cahal Pech](images/cahal-pech-1.jpeg)

Nous avons suivi une visite guidée, notre guide connaissait très bien l'histoire
du site et aussi les plantes locales. Voilà quelques photos de *Cahal Pech* :

![Cahal Pech](images/cahal-pech-2.jpeg)

![Cahal Pech](images/cahal-pech-3.jpeg)

![Cahal Pech](images/cahal-pech-4.jpeg)

![Cahal Pech](images/cahal-pech-5.jpeg)

L'après-midi, nous sommes allés au [jardin botanique][jardin-botanique], situé
en dehors de la ville et difficilement accessible par une route en terre.

![Jardin botanique](images/jardin-1.jpeg)

![Jardin botanique](images/jardin-2.jpeg)

![Jardin botanique](images/jardin-3.jpeg)

L'arbre dans la photo suivante s'appelle [*Haematoxylum
campechianum*][bloodwood-tree]...

![Haematoxylum campechianum](images/jardin-4.jpeg)

... parce que sa sève est rouge comme du sang :

![Haematoxylum campechianum](images/jardin-5.jpeg)

### 1er janvier

Nous avons ensuite visité le site Maya de [*Xunantunich*][xunantunich]. Pour y
accéder, il faut traverser une rivière avec un bac à câble :

![Xunantunich](images/xunantunich-1.jpeg)

Autour de ce site, il y a encore de la végétation surprenante comme ce [figuier
étrangleur][strangler-fig] :

![Xunantunich](images/xunantunich-2.jpeg)

Voilà la structure principale (*el castillo*) :

![Xunantunich](images/xunantunich-3.jpeg)

![Xunantunich](images/xunantunich-4.jpeg)

La vue depuis *el castillo* :

![Xunantunich](images/xunantunich-5.jpeg)

Vers l'ouest on peut apercevoir la frontière avec le Guatemala dans la photo
suivante (la ligne qui coupe la forêt) :

![Xunantunich](images/xunantunich-6.jpeg)

![Xunantunich](images/xunantunich-7.jpeg)

![Xunantunich](images/xunantunich-8.jpeg)

Cette visite faisait partie d'une visite guidée en deux parties, avec la visite
en canoë de [*Barton Creek Cave*][barton-creek-cave] l'après-midi. Pour arriver
à cette grotte, il faut encore emprunter des routes en terre mais cette fois
encore plus étroites et plus pentues, et pour une distance plus longue que
celles qui conduisaient au jardin botanique :

![Barton Creek](images/barton-creek-1.jpeg)

Pour accéder à la grotte, il fallait finalement traverser la rivière à gué :

![Barton Creek](images/barton-creek-2.jpeg)

On peut apercevoir l'entrée de la grotte dans la photo suivante. Nous l'avons
visitée en canoë mais je n'ai pas pris de photos à l'intérieur (il fallait
ramer). La grotte est très haute pendant environ 2 km, puis continue avec un
plafond trop bas pour passer en canoë, donc la visite s'arrêtait là.

![Barton Creek](images/barton-creek-3.jpeg)

Il y a une population [Mennonite][mennonite] dans les environs de *Barton Creek*, 
et nous en avons croisés sur la route :

![Barton Creek](images/barton-creek-4.jpeg)

### 2 janvier

Nous nous sommes promenés autour de [*The Lodge at Chaa Creek*][chaa-creek].
Nous avons parcouru un sentier bordé de plantes médicinales à travers la jungle :

![Chaa Creek](images/chaa-creek-1.jpeg)

Je n'ai pas de photos de toutes les plantes car il y en avait beaucoup. Cet
arbre à l'écorce rouge, appelé [gumbolimbo][tourist-tree], était noté pour sa
sève et son écorce qui peuvent calmer les irritations causées par le [*black
poisonwood tree*][poisonwood]. Les premiers colons espagnols en Amérique
centrale appelaient cet arbre "indio desnudo" pour sa peau rouge, mais
aujourd'hui les béliziens lui donnent un nom beaucoup plus drôle : ils
l'appellent "arbre touriste" car il a la peau rouge et il pèle.

![Chaa Creek](images/chaa-creek-2.jpeg)

Nous avons aussi visité l'élevage de papillons de *The Lodge at Chaa Creek*.
Quand nous l'avons visité, deux espèces de papillons étaient visibles (ils ont
aussi des papillons bleus mais qui n'avaient pas encore éclos) :

![Chaa Creek](images/chaa-creek-3.jpeg)

![Chaa Creek](images/chaa-creek-4.jpeg)

Ensuite nous avons loué un canoë pour rentrer à San Ignacio par la rivière
[Macal][macal] :

![Macal River](images/macal-river-1.jpeg)

![Macal River](images/macal-river-2.jpeg)


### 3 janvier

Nous avons pris le bus de San Ignacio jusqu'à [Belmopan][belmopan], puis de
Belmopan à [Dangriga][dangriga], et enfin de Dangriga à [Hopkins][hopkins]. Les
bus au Bélize sont des anciens bus scolaires venant des États-Unis :

![Hopkins](images/hopkins-1.jpeg)

En Europe on voit souvent des statues de chefs militaires sur des chevaux et
dans des postures martiales. À Hopkins, on est accueilli par les bustes de deux
femmes importantes dans l'histoire du village, par une plaque qui liste les
noms des premiers habitants et par une fresque colorée :

![Hopkins](images/hopkins-2.jpeg)

![Hopkins](images/hopkins-3.jpeg)

![Hopkins](images/hopkins-4.jpeg)

![Hopkins](images/hopkins-5.jpeg)

Hopkins est un tout petit village, large de deux rues mais long de presque 5 km
le long de la côte.

![Hopkins](images/hopkins-6.jpeg)

![Hopkins](images/hopkins-7.jpeg)

La plage est peuplée de pélicans. C'était impressionnant de les observer pêcher,
et aussi de les observer planer au ras de l'eau tôt le matin quand la mer est
calme.

![Hopkins](images/hopkins-8.jpeg)

![Hopkins](images/hopkins-9.jpeg)

![Hopkins](images/hopkins-10.jpeg)

### 4 janvier

Nous nous sommes baignés une dernière fois à Hopkins, au lever du soleil, puis
nous avons pris le bus jusqu'à [Placencia][placencia].

### 5 janvier

Nous avons exploré Placencia. Voilà deux photos intéressantes :

![Placencia](images/placencia-1.jpeg)

![Placencia](images/placencia-2.jpeg)

### 6 janvier

Nous avons loué des kayaks et nous sommes promenés dans les mangroves autour de
Placencia. Je n'ai pas de photos car nous n'avions pas de sacs étanches pour
emporter des téléphones ou appareils photos en kayak.

### 7 janvier

Nous sommes allés à [Silk Caye][silk-caye], une petite île au large de Placencia,
pour plonger (le père et le frère de Rebecca avec des bouteilles, nous juste
avec des tubas). La météo n'était pas très bonne, et une fois sur l'île minuscule
il y avait du vent, il pleuvait et il faisait un peu froid. Il faisait plus
chaud dans l'eau. Les vagues rendaient la plongée en tuba un peu difficile et
réduisaient la visibilité, mais c'était une bonne expérience malgré tout. Je
n'ai pas de photos de cette journée non plus.

### 8 janvier

Nous avons visité la ferme de cacao [Ixcacao][ixcacao], à San Felipe Village.

![Ixcacao](images/ixcacao-1.jpeg)

C'est en fait une parcelle de jungle à peine domestiquée, car le
[cacaoyer][cacaoyer] et d'autres plantes tropicales ont besoin de l'ombre
d'arbres plus grands.

![Ixcacao](images/ixcacao-2.jpeg)

![Ixcacao](images/ixcacao-3.jpeg)

La plante dans la photo suivante s'appelle [chaya][chaya]. Les feuilles sont
toxiques crues, mais comestibles une fois cuites. Nous en avons goûté dans un
restaurant à San Ignacio, cuites avec des oeufs brouillés et le tout servi comme
garniture dans un [*fry jack*][fry-jack].

![Ixcacao](images/ixcacao-4.jpeg)

![Ixcacao](images/ixcacao-5.jpeg)

![Ixcacao](images/ixcacao-6.jpeg)

La plante grimpante dans la photo suivante est un vanillier :

![Ixcacao](images/ixcacao-7.jpeg)

Et voilà plusieurs photos de cacaoyers :

![Ixcacao](images/ixcacao-8.jpeg)

![Ixcacao](images/ixcacao-9.jpeg)

Les graines sont fermentées et séchées dans une serre :

![Ixcacao](images/ixcacao-10.jpeg)

![Ixcacao](images/ixcacao-11.jpeg)

La visite se poursuivait avec une démonstration. Nous avons appris comment
séparer la peau des graines (après fermentation, séchage et torréfaction), puis
comment moudre les graines pour obtenir de la pâte de cacao (la photo suivante
montre la meule en pierre traditionnelle, mais pour leur production ils utilisent
des meules motorisées et qui peuvent moudre plus finement).

![Ixcacao](images/ixcacao-12.jpeg)

La visite incluait bien sûr aussi une dégustation (et aussi un repas
traditionnel maya complet qui était délicieux) :

![Ixcacao](images/ixcacao-13.jpeg)

Après cette visite, nous nous sommes arrêtés voir le site maya de [*Nim Li
Punit*][nim-li-punit] qui se trouve sur le chemin du retour vers Placencia.

![Nim Li Punit](images/nim-li-punit-1.jpeg)

![Nim Li Punit](images/nim-li-punit-2.jpeg)

![Nim Li Punit](images/nim-li-punit-3.jpeg)

### 10 janvier

Pour notre dernier jour au Bélize, nous avons fait une randonnée dans le parc
naturel de [*Cockscomb Basin*][cockscomb].

![Cockscomb Basin](images/cockscomb-basin-1.jpeg)

![Cockscomb Basin](images/cockscomb-basin-2.jpeg)

![Cockscomb Basin](images/cockscomb-basin-3.jpeg)

![Cockscomb Basin](images/cockscomb-basin-4.jpeg)

![Cockscomb Basin](images/cockscomb-basin-5.jpeg)

Dans la photo suivante, les noix dans l'arbre s'appellent des *cahune nuts* (je
n'ai pas trouvé de traduction en français). Elles ont presque le même goût que
la noix de coco, et sont utilisées principalement pour faire de l'huile (leur
pulpe est très fibreuse).

![Cockscomb Basin](images/cockscomb-basin-6.jpeg)

![Cockscomb Basin](images/cockscomb-basin-7.jpeg)

En repartant, nous avons eu la chance de voir plein d'[aras rouges][ara] dans un
arbre :

![Cockscomb Basin](images/cockscomb-basin-8.jpeg)


[belize]: https://fr.wikipedia.org/wiki/Belize

[san-ignacio]: https://fr.wikipedia.org/wiki/San_Ignacio_(Belize)

[hopkins]: https://en.wikipedia.org/wiki/Hopkins,_Belize

[placencia]: https://en.wikipedia.org/wiki/Placencia

[maya]: https://fr.wikipedia.org/wiki/Civilisation_maya

[cahal-pech]: https://fr.wikipedia.org/wiki/Cahal_Pech

[jardin-botanique]: https://belizebotanic.org

[bloodwood-tree]: https://fr.wikipedia.org/wiki/Camp%C3%AAche_(arbre)

[xunantunich]: https://fr.wikipedia.org/wiki/Xunantunich

[strangler-fig]: https://fr.wikipedia.org/wiki/Figuier_%C3%A9trangleur

[barton-creek-cave]: https://en.wikipedia.org/wiki/Barton_Creek_Cave

[mennonite]: https://en.wikipedia.org/wiki/Mennonites_in_Belize

[chaa-creek]: https://en.wikipedia.org/wiki/The_Lodge_at_Chaa_Creek

[tourist-tree]: https://en.wikipedia.org/wiki/Bursera_simaruba

[poisonwood]: https://en.wikipedia.org/wiki/Metopium_brownei

[macal]: https://en.wikipedia.org/wiki/Macal_River

[belmopan]: https://en.wikipedia.org/wiki/Belmopan

[dangriga]: https://en.wikipedia.org/wiki/Dangriga

[silk-caye]: https://fr.wikipedia.org/wiki/R%C3%A9serve_marine_de_Gladden_Spit_et_Silk_Cayes

[ixcacao]: https://www.ixcacaomayabelizeanchocolate.com

[cacaoyer]: https://fr.wikipedia.org/wiki/Cacaoyer

[chaya]: https://en.wikipedia.org/wiki/Cnidoscolus_aconitifolius

[fry-jack]: https://en.wikipedia.org/wiki/Fry_jack

[nim-li-punit]: https://en.wikipedia.org/wiki/Nim_Li_Punit

[cockscomb]: https://en.wikipedia.org/wiki/Cockscomb_Basin_Wildlife_Sanctuary

[ara]: https://fr.wikipedia.org/wiki/Ara_rouge
