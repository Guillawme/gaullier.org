---
title: 'New tool: topaztrainmetrics'
author: Guillaume Gaullier
date: '2021-01-20'
slug: new-tool-topaztrainmetrics
categories:
  - Software
tags:
  - work
  - Python
  - learning
  - cryo-em
  - visualization
lastmod: ~
authors:
  - guillaume
---


I made another command-line tool, this time to plot metrics from a training run
of the [Topaz][topaz] particle picking neural network.

The tool can be  [installed with `pip`][pypi]. The code is available
[here][repo]. I also made it citeable with {{< doi "10.5281/zenodo.4451826" >}}.


[topaz]: https://github.com/tbepler/topaz

[pypi]: https://pypi.org/project/topaztrainmetrics

[repo]: https://github.com/Guillawme/topaztrainmetrics
