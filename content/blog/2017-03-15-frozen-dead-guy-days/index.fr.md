---
title: "Frozen Dead Guy Days"
date: 2017-03-15
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: frozen-dead-guy-days
---


La semaine dernière avait lieu à Nederland un festival appelé
[*Frozen Dead Guy Days*][fdgd] (qu'on pourrait vaguement traduire par "festival
du macchabé congelé").

<!--more-->

Il y a plein d'animations et de concerts. Parmi ces animations, il y a une
course de cercueils (*coffin race*) : 6 personnes portent un (faux) cercueil,
une septième personne se place dedans, et les équipes s'affrontent deux par deux
dans une course sur un parcours plein de bosses et de boue. J'étais un équipier
de secours pour une équipe, et heureusement ils étaient au complet donc je n'ai
pas eu à participer (courir dans de la neige à moitié fondue par 5 °C, ce n'est
pas mon activité préférée pour le week-end). Je suis donc passé d'équipier
à photographe de l'équipe. Malheureusement, avec la foule qu'il y avait, je n'ai
pas de très bonnes photos à montrer... En voilà une sans la foule.

![Frozen Dead Guy Days](images/fdgd.jpg)

[Plus d'infos sur ce festival.][fdgd-wiki]


[fdgd]: http://frozendeadguydays.org

[fdgd-wiki]: https://en.wikipedia.org/wiki/Frozen_Dead_Guy_Days
