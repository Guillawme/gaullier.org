---
title: "Enfin autonome !"
date: 2016-05-01
lastmod:
authors: [guillaume]
categories: []
tags: [relocation]
slug: enfin-autonome
---


Beaucoup de choses se sont passées depuis le retour du congrès à Cold Spring
Harbor. Au cours de ces deux semaines j'ai pu régler pas mal de points qui, mis
bout à bout, me rendent beaucoup plus autonome.

<!--more-->

La première chose, c'est que j'ai enfin emménagé dans l'appartement à *Marine
Court* samedi 23 avril. L'appartement est au dernier étage d'un immeuble de
5 étages (donc l'étage est numéroté 6, si vous avez suivi ce que j'écrivais dans
l'article précédent ^^ ). L'immeuble n'est pas tout récent et un peu bruyant (on
n'entend pas les voisins, mais il y a un bourdonnement constant qui doit venir
du chauffage central), mais il est vraiment très bien situé : il est à environ
30 min à pieds du labo en suivant le *Boulder Creek Path* (un chemin piéton et
cyclable qui suit *Boulder Creek*, une des rivières qui traverse la ville), et
à environ 15 min à pieds du quartier animé du centre ville (*Pearl Street* et
ses environs). Il n'est pas très loin non plus de deux lignes de bus majeures et
d'un supermarché (les commerces de *Pearl Street* sont plutôt pour les loisirs,
on n'y trouve pas les produits de première nécessité). Les fenêtres sont
orientées vers l'Ouest, donc vers les montagnes. L'appartement est doté d'un
petit balcon où il doit être possible de caser trois ou quatre chaises (en se
serrant bien). Voilà la vue depuis le balcon vers le N-O :

![Vue depuis le balcon vers le N-O](images/appart-vue-no.jpg)

Et la vue vers le S-O ; les bâtiments en briques qu'on aperçoit font partie du
campus principal de l'université :

![Vue depuis le balcon vers le S-O](images/appart-vue-so.jpg)

L'université m'a enfin donné une carte de bus, cela a pris plus de temps que je
ne l'imaginais (pour d'obscures raisons administratives). C'est extrêmement
pratique de pouvoir se déplacer en bus sans payer $2.60 à chaque trajet (oui
c'est plus cher qu'à Paris...). La carte de bus me permet d'aller jusqu'à Denver
et l'aéroport, c'est donc un élément important pour être complètement autonome.
Je crois qu'il y a même une ligne de bus qui dessert la station de ski la plus
proche. :-) J'ai aussi reçu une sorte de carte d'identité liée à l'université,
que je dois avoir avec moi à tout moment au labo. Cette carte fait aussi
porte-monnaie (comme les cartes monéo). Et j'ai une carte d'accès et une clé qui
me permettent d'entrer dans le labo en dehors des heures d'ouverture du
bâtiment : je vais pouvoir travailler la nuit et le week-end... `*_*`

J'ai aussi reçu mon permis de conduire. Ou devrais-je l'appeler mon permis de
boire ? Comme j'en parlais dans un article précédent, ici les restaurateurs
contrôlent systématiquement que leurs clients ont plus de 21 ans pour accepter
de leur servir des boissons alcoolisées : ce permis de conduire américain les
rassurera sur mon âge bien plus facilement que ma carte d'identité française.
Je n'ai pas seulement besoin d'une pièce d'identité pour pouvoir boire un verre
tranquillement, mais aussi pour les vols intérieurs (je pourrais utiliser mon
passeport mais je serai plus tranquille de pouvoir le laisser en sécurité à la
maison plutôt que de l'emporter à chaque déplacement en avion et risquer de le
perdre ; c'est une pièce d'identité critique si je suis amené à devoir justifier
ma présence légale aux États-Unis). Je ne compte pas acheter de voiture car je
n'en aurai pas besoin (tous mes trajets quotidiens se font facilement en bus, en
vélo ou à pieds), alors pourquoi avoir obtenu un permis de conduire ? Ici c'est
la même administration qui délivre les simples pièces d'identité et les permis
de conduire, et le Colorado est un des quelques états qui a un accord avec la
France : moyennant une taxe de $25, il m'a suffit de montrer mon permis français
et de faire tester ma vue (en lisant une ligne de petits caractères et en
indiquant de quel côté s'allumait une lampe dans le champ de vision
périphérique) pour pouvoir obtenir un permis de conduire du Colorado sans devoir
passer d'examen de code de la route ni d'épreuve de conduite. Dans ces
conditions, autant choisir le permis de conduire plutôt que la simple pièce
d'identité, et donc me donner la possibilité de conduire occasionnellement (une
voiture de location pour partir en week-end par exemple).

Ce qui va suivre est un peu terre à terre, mais cela m'a occupé la tête pendant
plusieurs jours alors j'ai besoin de l'écrire. Après un mois à comparer les
opérateurs de téléphonie mobile, je me suis résigné à prendre un numéro
(d'ailleurs les personnes de mon entourage qui le veulent sont invitées à me le
demander par email). Je commence à en avoir besoin, dans plein de situations on
me demande un numéro et jusque-là je ne pouvais donner que mon numéro français
avec lequel les appels (émis et reçus) me coûtent cher ici. L'offre de
téléphonie mobile aux États-Unis est assez repoussante... Il faut dire qu'en
France, Free nous a habitués à un bon service pour quasiment rien. C'est en
partie pour ça que j'ai mis un mois à me décider : il fallait surmonter la
barrière mentale qui me criait que toutes les offres que je trouvais étaient des
arnaques énormes... Ici les offres les moins chères reviennent à environ $40 par
mois quel que soit l'opérateur, avec souvent des frais cachés un peu partout
(AT&T semble être assez spécialiste des frais cachés), ou des engagements de 12
ou 24 mois avec obligation de payer le solde restant si on décide de rompre
l'engagement. La situation est quasiment comme en France avant l'arrivée de Free
sur le marché, si j'ai bonne mémoire (c'est peut-être même pire car je me
souviens qu'avant les offres de Free on pouvait tout de même trouver des
forfaits décents pour une vingtaine d'euros par mois). Pour parfaire le tableau,
du point de vue technique les différents opérateurs utilisent des antennes
différentes et certains téléphones ne sont tout simplement pas compatibles avec
certaines antennes (donc avec certains opérateurs). En conséquence, à moins
d'accepter de se faire fourguer par l'opérateur un téléphone avec un engagement,
il faut bien se renseigner sur la compatibilité entre un téléphone que l'on
possède déjà et l'opérateur chez qui on envisage de s'abonner. La plupart des
personnes avec qui j'ai discuté de téléphone ici ne sont que moyennement
satisfaites de leur offre de téléphonie (au mieux), et ça se comprend. Pour le
moment j'ai pris chez un opérateur appelé Ting un abonnement du type *pay as you
go*, c'est-à-dire que je ne paye que ce que j'utilise effectivement. Si mon
utilisation courante me fait dépasser les $40 par mois j'envisagerai de changer
de formule et donc d'opérateur (Ting ne fait que du *pay as you go*). T-Mobile
a une offre intéressante pour ce prix (ou du moins, la moins pire de tout ce que
j'ai vu) : appels et sms illimités en Amérique du Nord (États-Unis, Mexique et
Canada) et 3 Go d'internet, sans frais cachés ni engagement (ils basent toute
leur pub sur ces deux points d'après ce que j'ai vu). Pour $65 par mois ils
proposent appels et sms illimités vers la France, avec 2 Go d'internet. Je crois
que je me contenterai de Skype et WhatsApp pour communiquer avec la France, vu
que je passe 80% de mon temps dans des endroits où il y a du wifi. Les 20%
restants du temps, je n'ai ni besoin ni envie d'avoir internet.

En parlant d'internet, je ne l'ai pas encore à l'appartement. En principe
l'université a un accord avec un fournisseur d'accès, et quand on habite dans
les logements de l'université on a seulement à monter le contrat de location
à la boutique locale de l'opérateur pour qu'ils fournissent un modem.
J'ai essayé de faire ça la semaine dernière juste après avoir emménagé, et cela
aurait dû fonctionner dès le premier jour du contrat de location. Mais ça n'a
pas marché (là encore pour d'obscures raisons administratives).
J'attends toujours que le *housing office* de l'université règle cette histoire
avec le fournisseur d'accès. J'espère que ça sera réglé dans la semaine
qui vient.

Pour terminer sur une note positive, hier j'ai acheté un vélo (d'occasion, par
le site d'annonces Craig's List). C'est l'étape finale vers mon autonomie
complète (les bus ne donnent qu'une autonomie partielle : ils ne vont pas
partout et sont moins fréquents le soir et le week-end). Voilà une photo reprise
directement de l'annonce :

![Vélo](images/velo.jpg)

Il est en excellent état, et du type idéal pour des trajets quotidiens en ville.
Ici il y a énormément de cyclistes, et la circulation sur les pistes cyclables
est assez dense aux heures où les gens vont travailler ou rentrent du travail :
j'ai donc aussi acheté un casque. À Paris j'utilisais énormément les vélibs sans
jamais porter de casque, alors que les pistes cyclables y sont beaucoup moins
bien tracées qu'ici et que la circulation des voitures y est beaucoup plus
dense. Je ne me sens pas en danger sur un vélo ici, mais autant profiter de ce
changement pour prendre une bonne habitude.
