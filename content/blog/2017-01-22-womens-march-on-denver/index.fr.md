---
title: "Women's March on Denver"
date: 2017-01-22
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: womens-march-on-denver
---


Hier, premier jour du mandat du nouveau président des États-Unis, ont eu lieu
des centaines de manifestations à travers le pays mais aussi dans le monde
entier. Il s'agissait entre autres de montrer au président que sa mysoginie
décomplexée n'est pas la bienvenue, et que son mandat sera scruté par
la population.

<!--more-->

Rebecca et moi sommes allés à l'événement local, la
[*Women's March on Denver*][womensmarchondenver]. Ce sera certainement étonnant
venant d'un français (nous avons à l'étranger une réputation de grévistes et
manifestants invétérés :D ), mais je dois confier que c'était ma première
participation à une manifestation. En 2015 et 2016, il y a eu de nombreuses
manifestations à Paris qui se sont plus ou moins mal terminées (violences, de
nombreuses personnes en garde à vue, etc.), et même si j'étais d'accord avec
certaines je n'y avais pas participé car je ne me sentais pas en sécurité.
J'étais donc un peu nerveux à propos de cette marche à Denver. Il se trouve que
j'ai eu tort : l'ambiance était très festive et amicale, et à ma connaissance
aucun débordement ne s'est produit. Plusieurs articles du *Denver Post* (un
journal local) permettent de se faire une bonne idée de l'événement :

- [Women's March on Denver photos][denverpostphotos]
- [Who marched?][denverpostwhomarched]
- [Timelapse of the Women's March on Denver][timelapse] : cette vidéo ne montre
  que l'endroit du rassemblement initial, avant que la marche ne commence, mais
  donne une bonne idée de l'ampleur de la manifestation.
- Le *Denver Post* rapporte que
  [la manifestation locale a réuni plus de 100 000 personnes !][100000]

Il y a aussi sur Twitter de nombreuses photos prises par des participants, en
recherchant les mots-clés [#WomensMarchonDenver][twitter-1] pour l'événement
local à Denver, et [#WomensMarch][twitter-2] pour l'événement global.

C'était une expérience enrichissante ! En particulier, c'était un bon moyen de
se remonter le moral avec une forte dose de bonne humeur, après toutes les
nouvelles déprimantes qui s'accumulaient depuis le 8 novembre (*Election Day*).
Je soupçonne que d'autres occasions de manifester se présenteront au cours des
quatre prochaines années...


[womensmarchondenver]: http://www.marchoncolorado.org

[denverpostphotos]: http://www.denverpost.com/2017/01/21/womens-march-on-denver-photos

[denverpostwhomarched]: http://www.denverpost.com/2017/01/21/snapshots-who-participated-womens-march-on-denver

[timelapse]: http://www.denverpost.com/2017/01/21/womens-march-on-denver-timelapse

[100000]: http://www.denverpost.com/2017/01/21/womens-march-denver

[twitter-1]: https://twitter.com/hashtag/WomensMarchonDenver

[twitter-2]: https://twitter.com/hashtag/WomensMarch
