---
title: "Premiers jours à Boulder"
date: 2016-04-03
lastmod:
authors: [guillaume]
categories: []
tags: [relocation, Colorado]
slug: premiers-jours-a-boulder
---


Après avoir trouvé un labo où faire mon post-doc, et après beaucoup de
paperasses (d'ailleurs ce n'est pas encore terminé...), me voilà finalement
arrivé à [Boulder][boulder], [Colorado][colorado], pour au moins un an.
Pour garder des souvenirs précis de cette expérience, je tâcherai d'écrire
régulièrement dans ce blog. Voilà donc mes premières impressions trois jours
après mon arrivée, enfin remis des 8h de décalage horaire.

<!--more-->

## Le départ de Paris

Le voyage a commencé avec une bonne dose de stress : je suis parti le 31 mars
2016, jour de grève nationale en France (pour protester contre un projet de loi
de réforme du code du travail). Quiconque a déjà utilisé les transports en
commun à Paris sait la galère que devient le moindre trajet dans ces
conditions... Heureusement la grève était annoncée à l'avance, et en partant
suffisamment tôt j'ai pu arriver à l'aéroport à l'heure prévue. 

## Le vol

Peu de choses à dire concernant le vol... C'est long, bruyant (le bourdonnement
des moteurs de l'avion est rapidement insupportable), on est mal nourri, et mal
assis. Bref, c'est l'avion. L'escale à Reykjavik était cependant intéressante
car je n'étais encore jamais allé en Islande. Je n'ai passé qu'une heure dans
l'aéroport, mais j'ai quand même pu voir un peu les environs depuis l'avion, et
ce pays a l'air vraiment beau. J'aimerais faire une escale plus longue la
prochaine fois (au moins deux jours).

## L'arrivée à Denver

Je craignais un peu l'arrivée car je n'ai pas beaucoup dormi dans l'avion et
j'étais assez fatigué, il fallait récupérer et transporter deux valises de 23 kg
(heureusement équipées de roulettes), montrer patte blanche à la douane et
trouver le bus pour aller à Boulder. Je m'attendais à ce que tout ça prenne
beaucoup de temps (surtout attendre la livraison des bagages et passer la
douane), mais finalement tout s'est fait en moins d'une heure ! Une file était
réservée pour les voyageurs munis d'un visa, et je devais être le seul dans
cette situation dans cet avion (tous les autres étaient soit des touristes soit
des citoyens américains, et passaient la douane dans des files distinctes) : je
n'ai donc eu aucun temps d'attente. Ensuite le douanier a seulement jeté un coup
d'oeil au visa, appliqué un tampon dans mon passeport, m'a expliqué que je
devais avoir sur moi le papier associé au visa pour toute sortie du territoire
(en fait il faut avoir le papier pour pouvoir ré-entrer après une sortie), et
c'était fini. Il n'a même pas lu le formulaire de déclaration des biens entrant
dans le territoire avec mes bagages.

Ensuite j'ai assez facilement trouvé le bus pour aller à Boulder. Le conducteur
était très professionnel, il m'a aidé à mettre mes valises super lourdes dans la
soute du bus. Après environ 45 minutes de trajet je retrouvais donc Marta, la
dame qui m'accueille gentiment chez elle et son mari Jeff pendant les deux
semaines qui restent avant que se libère l'appartement que l'université m'a
proposé. Jeudi soir il était environ 22h (heure des Rocheuses) quand je me suis
couché, mais pour mon corps il était l'heure de Paris soit 6h du matin... ce qui
fait une journée de 22h (je m'étais levé vers 8h heure de Paris pour prendre
l'avion). Même en étant assis la majorité du temps, c'est épuisant. Je ne
comprends toujours pas comment je me suis automatiquement réveillé après
seulement 6h de sommeil (enfin si, ça correspondait à midi en heure française,
soit une heure à laquelle je suis toujours éveillé).

## Premier week-end à Boulder

Vendredi Marta m'a montré la ville, en passant près du campus principal (mon
labo est dans l'autre campus, plus à l'Est) et près de la résidence *Marine
Court*, juste au Nord du campus principal, dans laquelle se trouve l'appartement
que j'aurai dans deux semaines. Elle m'a aussi emmené dans les montagnes, d'où
il y a une vue magnifique sur la ville et sur tout le plateau (par temps très
clair on peut même voir l'aéroport de Denver depuis cet endroit). Denver et
Boulder se trouvent sur un plateau qui est à environ 1600 m d'altitude
([Denver est appelée la *Mile-High City*][denver] pour cette raison).
Les montagnes dans lesquelles Marta m'a emmené s'appellent la
[*Front Range*][front-range] : il s'agit de la chaine orientale des
[Rocheuses][rocheuses]. En regardant vers l'Ouest depuis la *Front Range*, on
voit d'autres montagnes plus hautes. Le paysage est magnifique. La *Front Range*
est aussi bien utile pour s'orienter : la chaine s'étend quasiment dans l'axe
Nord-Sud, et se trouve à l'Ouest de Boulder. Donc, quand on marche avec les
montagnes à sa gauche, on sait que l'on va vers le Nord. En remarquant aussi le
plan de la ville proche d'une grille avec des rues orientées N-S et d'autres
E-O, il devient assez facile de s'orienter ici. Le soir nous avons dîné dans un
restaurant mexicain où Marta & Jeff sont apparemment des habitués. Ce restaurant
est situé assez près de l'appartement. :-) Nous avons aussi marché dans une rue
piétonne appelée *Pearl Street*, dans laquelle se trouvent plein de boutiques et
de restaurants.

Samedi après-midi, encore sous l'effet du décalage horaire, j'ai exploré un peu
plus la ville, cette fois seul et à pieds. J'ai marché dans le campus principal,
qui est vraiment magnifique. Voilà quelques photos prises dans le campus :

![Vue du campus](images/campus-1.jpg)

![Vue du campus](images/campus-2.jpg)

![Vue du campus](images/campus-3.jpg)

Il faisait tellement beau que j'ai pris des coups de soleil...

Beaucoup de gens se déplacent en vélo, et des bus parcourent les rues
principales et desservent les endroits très fréquentés comme le campus. Je me
suis aussi aperçu que les distances sont assez raisonnables pour pouvoir se
déplacer à pieds, du moins dans le centre-ville. Beaucoup de villes américaines
sont très étendues et rendent donc les gens très dépendants de leurs voitures,
je suis content que ce ne soit pas trop le cas pour Boulder. Je pense utiliser
le bus, et peut-être [l'équivalent local des vélibs][velibs] si je trouve des
stations bien placées pour les trajets quotidiens au labo. Ou bien j'achèterai
un vélo. Ce qui est sûr c'est que je pourrai me passer de voiture la plupart du
temps, et simplement en louer une ponctuellement selon les besoins (pour partir
en week-end par exemple).

Samedi soir Jeff est rentré d'un déplacement professionnel à Chicago.
Il neigeait là-bas au moment où il partait, alors qu'ici dans les montagnes on
pouvait sortir en t-shirt tellement il faisait beau. Ce pays est vraiment
déroutant. Aujourd'hui, dimanche, nous étions invités à une fête chez une amie
de Marta à l'occasion de la naissance imminente du premier enfant du fils de
cette amie. Ce type de fête est appelé *baby shower*, c'est l'occasion pour les
amis des futurs parents de leur offrir des cadeaux utiles pour le nouveau né.
Des enfants du voisinage vendaient de la limonade faite maison (sans lien avec
la fête).

Demain sera mon premier jour au labo, avec principalement de la paperasse
(encore...) : il s'agit des dernières formalités avec mon employeur
([HHMI][hhmi]). Mardi matin sera aussi dédié à de la paperasse, cette fois ce
sera l'*International Students and Scholars Service* de l'université qui
procédera à la validation finale de mon visa et m'indiquera comment maintenir
mon statut (c'est une histoire d'assurance : c'est à moi de veiller à ce que je
sois assuré à tout moment pendant la durée du visa, sans quoi il peut être
révoqué).

De lundi à vendredi a lieu la [*Conference on World Affairs*][cwa],
malheureusement tous les événements ont lieu en pleine journée et je ne serai
pas disponible pour y aller. Tout a l'air très intéressant.
J'essayerai d'assister à quelques événements planifiés en soirée.

Je ne rencontrerai pas ma chef avant la fin de la semaine car elle est
apparemment en déplacement professionnel.


[boulder]: https://fr.wikipedia.org/wiki/Boulder

[colorado]: https://fr.wikipedia.org/wiki/Colorado

[rocheuses]: https://fr.wikipedia.org/wiki/Montagnes_Rocheuses

[denver]: https://fr.wikipedia.org/wiki/Denver

[front-range]: https://fr.wikipedia.org/wiki/Front_Range

[velibs]: https://boulder.bcycle.com

[hhmi]: http://www.hhmi.org

[cwa]: http://www.colorado.edu/cwa
