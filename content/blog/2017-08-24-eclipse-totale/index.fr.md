---
title: "Éclipse totale"
date: 2017-08-24
lastmod:
authors: [guillaume]
categories: []
tags: [travels, Wyoming]
slug: eclipse-totale
---


Lundi dernier (21 août) a eu lieu
[une éclipse solaire totale qui a traversé tout le continent d'ouest en est.][eclipse-wiki]
Boulder n'était pas dans la zone d'ombre où l'éclipse était totale, mais il nous
a suffit de rouler environ 3h30 vers le nord pour nous retrouver dans des
conditions parfaites : en pleine campagne dans l'état du [Wyoming][wyoming],
près de la ville de [Fort Laramie][fort-laramie]. Nous sommes partis le samedi
soir, avons campé deux nuits, et sommes rentrés à Boulder le lundi soir (avec
beaucoup plus de circulation qu'à l'aller, évidemment...). Voilà quelques photos
prises pendant ce week-end de camping.

<!--more-->

Voilà quelques vues du camping (improvisé dans un ranch), au lever du soleil et
en plein jour :

![Camping](images/eclipse-1.jpg)

![Camping](images/eclipse-2.jpg)

![Camping](images/eclipse-3.jpg)

![Camping](images/eclipse-4.jpg)

Pendant tout le week-end, et avant, pendant et après l'éclipse, des chercheurs
de la NASA étaient occupés à lancer des ballons pour observer le soleil (pour
avoir un point d'observation bien plus haut, donc avec beaucoup moins
d'absorption par l'atmosphère) :

![Ballons](images/eclipse-5.jpg)

![Ballons](images/eclipse-6.jpg)

![Ballons](images/eclipse-7.jpg)

Les petits ballons vus sur les photos précédentes ne sont jamais récupérés :
leurs sondes envoient les donnés en quasi-temps réel à l'ordinateur de l'équipe,
et ne sont pas assez chères pour justifier le coût d'aller les récupérer.
Les plus grands ballons vus sur les photos suivantes, en revanche, ont tous dû
être récupérés :

![Ballons](images/eclipse-8.jpg)

![Ballons](images/eclipse-9.jpg)

![Ballons](images/eclipse-10.jpg)

[Ce site de la NASA][nasa-balloons] donne plus d'explications sur ces ballons.

Sans ballons et sans lunettes, on peut tout de même observer l'éclipse pendant
la phase partielle à l'aide d'un [sténopé][sténopé] :

![Éclipse](images/eclipse-11.jpg)

![Éclipse](images/eclipse-12.jpg)

Je n'ai pas de photo de la phase totale de l'éclipse, car j'étais bien trop
fasciné par le spectacle pour penser à prendre des photos (sans un très bon
appareil, ces photos n'auraient pas été formidables de toute façon). La phase
totale a duré environ 2 minutes là où nous nous trouvions, mais j'ai
l'impression qu'elle n'a duré que 10 secondes ! J'avais déjà vu une éclipse
solaire totale, [en 1999][eclipse-1999], mais comme je n'avais que 9 ans
à l'époque je ne me souvenais pas à quel point c'est un phénomène
impressionnant. Comme un coucher de soleil à 360º, avec une obscurité suffisante
pour voir les étoiles les plus brillantes, et surtout avec l'incroyable
spectacle du disque solaire complètement masqué par la lune.

Pour finir, voilà une dernière photo. Pourrez-vous deviner à quoi sert cet
engin ?

![Camping](images/eclipse-13.jpg)


[eclipse-wiki]: https://fr.wikipedia.org/wiki/%C3%89clipse_solaire_du_21_ao%C3%BBt_2017

[wyoming]: https://fr.wikipedia.org/wiki/Wyoming

[fort-laramie]: https://goo.gl/maps/T2bJ9L5aT7q

[nasa-balloons]: https://eclipse2017.nasa.gov/balloon-observations

[sténopé]: https://fr.wikipedia.org/wiki/St%C3%A9nop%C3%A9

[eclipse-1999]: https://fr.wikipedia.org/wiki/%C3%89clipse_solaire_du_11_ao%C3%BBt_1999
