---
title: Sälen et Åre
author: Guillaume Gaullier
date: '2022-05-07'
slug: salen-et-are
categories: []
tags:
  - sweden
  - travels
lastmod: ~
authors:
  - guillaume
---


Il est un peu tard pour poster ça, mais je voulais garder un souvenir de deux
stations de ski où je suis allé en mars dernier : [Sälen][salen] et [Åre][are].

<!--more-->

J'ai passé deux jours à Sälen pour une retraite avec le labo. Nous avions une
conférence le matin, mais l'après-midi libre pour skier. C'est une petite
station, mais bien suffisante pour seulement deux après-midis. Et il a fait un
temps magnifique :

![Sälen](images/salen.jpeg)

La semaine suivante, j'étais à Åre avec des collègues, cette fois pour des
vacances. Voilà la vue que nous avions de l'hôtel :

![Åre](images/are-01.jpeg)

![Åre](images/are-02.jpeg)

Et quelques photos prises sur les pistes :

![Åre](images/are-03.jpeg)

![Åre](images/are-04.jpeg)

![Åre](images/are-05.jpeg)

![Åre](images/are-06.jpeg)

![Åre](images/are-07.jpeg)

![Åre](images/are-08.jpeg)

![Åre](images/are-09.jpeg)

![Åre](images/are-10.jpeg)

![Åre](images/are-11.jpeg)

![Åre](images/are-12.jpeg)

![Åre](images/are-13.jpeg)

Puisque j'en suis encore à poster des trucs de l'hiver dernier, voilà deux
autres bons souvenirs. Nous avons fait l'expérience authentique du sauna et de
la baignade dans un trou découpé dans un lac gelé. C'est moins fou que ça n'en a
l'air : se baigner dans l'eau froide une fois qu'on a trop chaud permet de
retourner dans le sauna pour y rester plus longtemps ; on peut aussi simplement
rester à l'extérieur un moment (par beau temps et sans vent on peut rester
dehors longtemps avant d'avoir froid). Voilà une photo depuis l'intérieur du
sauna :

![Sauna](images/sauna.jpeg)

L'autre activité sympa de l'hiver dernier, c'était deux sorties de snowkiting
(je n'ai pas écrit là-dessus sur ce blog, mais j'ai appris le kitesurf l'été
dernier ; eh bien le snowkiting c'est pareil mais sur des skis ou patins à
glace, et c'est même plus facile). Dans la deuxième photo c'est Simon, un ami
français rencontré à Uppsala et qui fait aussi du kitesurf. Nous étions sur le
lac de Knivsta, pas très loin d'Uppsala.

![Snowkiting](images/snowkiting-1.jpeg)

![Snowkiting](images/snowkiting-2.jpeg)


[salen]: https://goo.gl/maps/WrzLX8q5QrnBDj797

[are]: https://goo.gl/maps/DqvcrFyPws1QNimP9
