---
title: "First article from my PhD work finally published"
date: 2016-01-11
lastmod:
authors: [guillaume]
categories: [Publications]
tags: [work]
slug: first-article-finally-published
---


After more than one year of preparation and revisions, the first article from my
PhD work finally got accepted a few days before Christmas and appeared online
a few days ago. :-)

<!--more-->

Here is the full reference:

**Gaullier G**, Miron S, Pisano S, Buisson R, Le Bihan Y-V, Tellier-Lebègue C,
Messaoud W, Roblin P, Guimarães BG, Thai R, Giraud-Panis M-J, Gilson E & Le Du
M-H (2016) A higher-order entity formed by the flexible assembly of RAP1 with
TRF2. *Nucleic Acids Research* Advance Access published January 8, 2016.
{{< doi "10.1093/nar/gkv1531" >}}

This is the opportunity to introduce a new page simply called
"[Publications][publications]" which will hold a list of my publications. I will
do my best to keep this list up-to-date.


[publications]: {{< relref path="/page/publications" lang="en" >}}
