---
title: "New publication"
date: 2017-02-16
lastmod:
authors: [guillaume]
categories: [Publications]
tags: [work]
slug: new-publication
---


An article I was invited to write was just published today in
[*Molecular Cell*][mol-cell]. This doesn't really count as much as a real
research article (this one doesn't present my work and was not peer-reviewed),
but this was an interesting writing exercise.

<!--more-->

Here is the full reference (also added to the
[publications page][publications]):

**Gaullier G** & Luger K (2017) PARP1 and Sox2: An Unlikely Team of Pioneers to
Conquer the Nucleosome. *Molecular Cell* **65**: 581–582.
{{< doi "10.1016/j.molcel.2017.02.001" >}}


[mol-cell]: http://www.cell.com/molecular-cell/home

[publications]: {{< relref path="/page/publications" lang="en" >}}
