---
title: New preprint
author: Guillaume Gaullier
date: '2021-06-20'
slug: new-preprint
categories:
  - Preprints
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
---


A new study I contributed to went online yesterday in a preprint at
[bioRxiv][biorxiv]. The full reference is:

Bacic L, **Gaullier G**, Sabantsev A, Lehmann L, Brackmann K, Dimakou D,
Halic M, Hewitt G, Boulton SJ & Deindl S (2021) Structure and dynamics of the
chromatin remodeler ALC1 bound to a PARylated nucleosome. *bioRxiv*:
**2021.06.18.448936**. {{< doi "10.1101/2021.06.18.448936" >}}

I also added it to the [publications page][publications].

[biorxiv]: https://www.biorxiv.org

[publications]: {{< relref path="/page/publications" lang="en" >}}
