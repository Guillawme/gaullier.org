---
title: Mitchell Lake et Blue Lake
author: Guillaume Gaullier
date: '2019-07-20'
slug: mitchell-lake-blue-lake
categories: []
tags:
  - Colorado
  - hiking
lastmod: ~
authors:
  - guillaume
---


Dimanche dernier, nous avons fait une longue randonnée dans les environs de
Brainard Lake. Nous y étions déjà allés [l'année dernière][brainard-2018] ;
cette fois nous avons pris un sentier différent appelé *Mitchell Lake Trail*.

<!--more-->

Ce sentier, comme son nom l'indique, conduit à *Mitchell Lake* :

![Mitchell Lake](images/mitchell-lake-1.jpg)

![Mitchell Lake](images/mitchell-lake-2.jpg)

Le sentier continue après ce premier lac, jusqu'à une altitude où il y a toujours
pas mal de neige alors que nous sommes déjà au milieu de l'été :

![Sentier vers Blue Lake](images/blue-lake-1.jpg)

Il mène finalement à *Blue Lake*, qui est encore partiellement gelé :

![Blue Lake](images/blue-lake-2.jpg)

![Blue Lake](images/blue-lake-3.jpg)

Je me demande si ce lac fond complètement à la fin de l'été, il faudrait revenir
en Septembre.


[brainard-2018]: {{< relref "2018-07-07-pawnee-pass" >}}
