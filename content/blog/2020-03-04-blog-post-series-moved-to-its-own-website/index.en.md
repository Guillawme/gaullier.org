---
title: Blog post series moved to its own website
author: Guillaume Gaullier
date: '2020-03-04'
slug: blog-post-series-moved-to-its-own-website
categories:
  - Insights from the PDB
tags:
  - work
  - R
lastmod: ~
authors:
  - guillaume
---


A while ago I started a [series of blog posts][insights-pdb] to answer questions
using metadata from the [Protein Data Bank][pdb]. Writing these posts was fun,
and I learned a lot while doing so. I would like to keep the plots updated (they
are most useful this way), while also keeping the blog posts the way they were
on their publication date (because showing data more recent than the blog post
they appear in would be confusing). For this reason, I set up [a dedicated
website][website] that presents all the results. I try to update it every month.
I will post on this blog when I make a new analysis that's very different from
the others and worth commenting on, but I won't post when I simply adapt the
same analysis to a different molecule (this will go directly to the website).


[insights-pdb]: /en/categories/insights-from-the-pdb/

[pdb]: https://en.wikipedia.org/wiki/Protein_Data_Bank

[website]: https://guillawme.github.io/insights-from-the-pdb/
