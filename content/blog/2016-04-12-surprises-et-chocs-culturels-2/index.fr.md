---
title: "Surprises et chocs culturels - 2"
date: 2016-04-12
lastmod:
authors: [guillaume]
categories: []
tags: [relocation]
slug: surprises-et-chocs-culturels-2
---


Deux nouveaux chocs culturels.

<!--more-->

Voilà un choc culturel qui est arrivée plus rapidement que je ne l'attendais :
dans un restaurant on m'a demandé ma pièce d'identité. Les restaurateurs veulent
s'assurer qu'ils ne servent pas d'alcool à des clients de moins de 21 ans, alors
à moins d'avoir les cheveux gris (ou plus de cheveux du tout), quasiment tout le
monde se fait contrôler. C'est marrant que les gens pensent que j'ai moins de 21
ans alors que j'en aurai bientôt 26. :D

Une autre différence culturellle à laquelle je ne m'habitue toujours pas, ce
sont les étiquettes de prix dans les magasins qui sont toujours hors taxes.
Systématiquement je bug au moment de payer car le prix qu'on me demande ne
correspond pas à ce qui était écrit (il faut en gros ajouter 10 % du prix
affiché). Je ne sais pas si je finirai par m'y habituer, c'est assez déroutant.
