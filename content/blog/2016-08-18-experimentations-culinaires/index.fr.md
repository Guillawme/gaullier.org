---
title: "Expérimentations culinaires"
date: 2016-08-18
lastmod:
authors: [guillaume]
categories: []
tags: [food]
slug: experimentations-culinaires
---


Le dernier article date déjà de plus d'un mois... L'été passe très vite, même
quand on n'a pas de vacances. Comme je n'ai pas beaucoup d'imagination pour
écrire un article, je vais simplement raconter quelques expérimentations
culinaires de ces dernières semaines.

<!--more-->

La première de ces expérimentations, une découverte pour moi, ce sont les fruits
de [*milkweed*][milkweed]. Rebecca en a trouvé dans le coin, et m'a appris que
ça se mange. La page Wikipedia citée indique que cette famille de plantes est
toxique (je le découvre à l'instant en écrivant cet article), mais ça ne doit
pas être le cas de toutes les espèces. Ou alors nous n'en avons pas mangé assez
pour nous en apercevoir ? Nous avons essayé deux préparations différentes :
sauté avec de l'ail et des épices, ou frit dans une pâte à beignet simplifiée.
Les deux donnent un résultat assez bon. Une fois cuit, l'intérieur des fruits
a une consistance de fromage fondu, c'est très surprenant ! Voilà des photos :

![Milkweeds](images/milkweeds-1.jpg)

![Milkweeds](images/milkweeds-2.jpg)

Au *farmers market*, il y a une ferme qui a une offre intéressante : pour $10 on
peut remplir un grand sac avec ce que l'on veut. J'y ai trouvé des concombres
parfaits pour en faire des pickles :

![Concombres](images/concombres.jpg)

La mère de Rebecca a écrit [un livre de recettes de pickles][joy-pickling] (elle
tient aussi [un blog culinaire][linda-blog] intéressant), et ayant déjà goûté
les résultats de certaines de ces recettes, je n'ai pas hésité un instant pour
acheter un grand sac de concombres ! Pour le moment j'ai essayé une recette de
*sweet pickles* (conservation par le vinaigre) avec de bons résultats :

![Pickles](images/pickles-1.jpg)

![Pickles](images/pickles-2.jpg)

Avec un autre lot de concombres, j'ai suivi une recette de *fermented pickles*.
Ceux-là ne sont pas encore prêts, donc nous ne les avons pas encore goûtés (mais
ça fermente à fond, vu les bulles qui s'échappent des concombres).

Finalement, cette semaine Rebecca est allée chercher des champignons avec ses
collègues, et a trouvé ces énormes cèpes (la fourchette donne une échelle) :

![Cèpes](images/champignons.jpg)

Nous avons fait sécher la plupart, car c'était impossible de tout manger en une
fois. Le climat est très sec ici, c'est donc facile de les conserver secs.

Finalement, nous testé plusieurs glaciers à *Pearl Street*, et aussi [cet endroit
récemment aménagé par la ville et dédié à l'accueil de *food trucks*][rayback].
Je n'ai pas de photos pour tout ça : vous devrez imaginer les glaces...


[milkweed]: https://en.wikipedia.org/wiki/Asclepias

[joy-pickling]: https://www.quartoknows.com/books/9781558328600/The-Joy-of-Pickling-3rd-Edition.html

[linda-blog]: https://agardenerstable.com

[rayback]: https://therayback.com
