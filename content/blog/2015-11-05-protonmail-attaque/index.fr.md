---
title: "ProtonMail attaqué"
date: 2015-11-05
lastmod:
authors: [guillaume]
categories: []
tags: [internet]
slug: protonmail-attaque
---


Voici un article très bref pour réagir
à [la récente attaque par déni de service subie par ProtonMail][protonmail-ddos]
(qui est apparemment toujours en cours à l'heure où j'écris).

<!--more-->

Pour le moment il n'y a que peu d'informations sur cette attaque. On sait
seulement que l'accès à ProtonMail est impossible et qu'ils ont des difficultés
à le rétablir. Leur infrastructure étant assez importante, on peut supposer que
l'attaque en question est de grande envergure, un peu comme [celle qui a frappé
GitHub plus tôt cette année][github-ddos] ? Difficile d'en dire plus.
ProtonMail rassure ses utilisateurs en disant que les données hébergées ne sont
pas menacées, en effet ce type d'attaque vise à empêcher les utilisateurs
d'accéder au service mais ne cherche pas à détruire ou voler des données.

Ce ne sont donc pas les données qui intéressent l'attaquant, mais le seul fait
que celles-ci soient protégées : on peut donc maintenant supposer que des
initiatives comme ProtonMail (qui ont le potentiel d'ouvrir les yeux à un grand
nombre d'internautes sur l'importance de la protection de leurs données)
dérangent des gens ou des organisations suffisamment hostiles à la protection
des données et suffisamment équipés pour mettre en marche ce type d'attaque.
À mes yeux, cette attaque montre donc bien que la protection des données (entre
autres par le chiffrement de bout en bout tel que proposé par ProtonMail) est
importante, puisque quelqu'un cherche à nous en priver d'une façon assez
violente. Certaines lois plus ou moins récentes dans certains pays cherchent
aussi à nous en priver, mais dans ce cas c'est principalement de notre faute
(collectivement en tant que citoyens) car on a les gouvernements que l'on mérite
si l'on ne se bouge pas collectivement pour changer les choses.
Espérons seulement que des attaques de ce type n'émanent pas de gouvernements,
ce qui me mettrait personnellement très mal à l'aise (si un gouvernement censé
nous représenter et travailler pour l'intérêt général s'avère responsable d'une
attaque violente de ce type justement à l'encontre de l'intérêt général,
jusqu'où est-il prêt à aller dans ce sens ?). J'étais pour ma part déjà
convaincu que la protection des données est importante, mais cette attaque aura
peut-être le mérite de faire réaliser cela à un public plus large.

D'autre part, même si l'initiative de ProtonMail est intéressante et si je suis
désolé d'apprendre qu'ils sont la cible d'une telle attaque, cette nouvelle
souligne bien aussi le danger des services centralisés. Dans un scénario idéal
où chacun utiliserait le chiffrement mais avec une installation locale d'un
logiciel de chiffrement et avec des fournisseurs de messagerie électronique
différents, une telle attaque serait beaucoup moins probable. En effet pour
gêner les utilisateurs, l'attaquant devrait bloquer chaque fournisseur
indépendamment, ce qui est nettement plus difficile que de concentrer une
attaque de grande envergure sur un seul fournisseur.

Espérons que ces événements fassent réagir les utilisateurs des emails.
Pour rappel, ma clé publique OpenPGP est disponible sur la
[page de contact][contact].


[protonmail-ddos]: https://protonmaildotcom.wordpress.com

[github-ddos]: https://github.com/blog/1981-large-scale-ddos-attack-on-github-com

[contact]: {{< relref path="/page/contact" lang="fr" >}}
