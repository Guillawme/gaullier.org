---
title: "Cold Spring Harbor et New York"
date: 2016-04-22
lastmod:
authors: [guillaume]
categories: []
tags: [travels, work, "New York"]
slug: cold-spring-harbor-et-new-york
---


Du 13 au 16 avril je suis allé au *Cold Spring Harbor Laboratory* pour assister
à un congrès. Tout ne s'est pas passé comme prévu, mais pour une bonne surprise
au final. Voilà le récit de ces quelques jours (que j'aurais souhaité mettre en
ligne plus tôt, mais la semaine est passée très vite après le retour du
congrès).

<!--more-->

Mercredi 13 dans l'après-midi j'ai donc pris l'avion depuis Denver avec quatre
collègues du labo et notre chef, pour rejoindre le paisible hameau de
[Cold Spring Harbor][csh], sur la côte Nord de [*Long Island*][li], où se trouve
[le laboratoire qui porte le même nom][cshl]. Après environ deux heures à rouler
au pas dans les bouchons autour de l'aéroport de *La Guardia*, nous arrivons
finalement à destination. On nous dirige directement vers le dîner, c'est un
accueil qui me plaît bien. Ensuite se tient la première session du congrès,
quatre présentations de 20 min suivies de questions s'enchaînent. Cela se finit
assez tard (vers 22h30), et je fais partie des gens logés en dehors du campus
donc je dois encore prendre une navette (offerte par le campus) pour rejoindre
ma chambre et enfin dormir. Les autres sessions du congrès se sont enchaînées
jusqu'à samedi midi, avec une session posters jeudi après-midi. À la fin de sa
présentation vendredi, ma chef a mis une diapo avec nos photos pour remercier
les personnes qui ont réalisé les travaux qu'elle a montrés, et pour présenter
les trois nouvelles personnes récemment arrivées au labo (dont moi). Je ne
m'attendais pas du tout à ce qu'elle annonce à tout l'auditorium que c'est mon
anniversaire, en faisant apparaître une couronne au dessus de ma photo...
Première surprise ! C'est aussi ce soir-là qu'a lieu le banquet organisé pour le
congrès (car c'est le dernier soir, le congrès se termine le lendemain midi), on
m'a donc servi du homard pour mon anniversaire. :D Je n'en avais jamais goûté,
mais en fait je préfère le crabe.

C'est là que l'imprévu arrive : samedi il commence à neiger dans la région de
Denver ! Voilà une photo prise par Jeff dimanche :

![Neige à Boulder](images/neige-boulder.jpg)

Notre vol de retour prévu samedi après-midi est annulé... Nous ne pourrons
repartir que lundi matin. Samedi après-midi a donc été l'occasion de découvrir
les alentours du campus, ce qui était difficile pendant le congrès avec les
conférences qui duraient jusqu'en fin de soirée. Voilà quelques photos prises
dans les environs.

La vue depuis ma chambre :

![La vue depuis ma chambre](images/cshl-1.jpg)

La maison où j'étais logé (qui appartient au laboratoire et fait partie d'une
annexe du campus) et le mini-bus qui nous conduisait au campus :

![La maison où j'étais logé](images/cshl-2.jpg)

Quelques photos prises dans le campus principal, situé juste au bord de la mer
(on peut apercevoir des maquettes représentant un [plasmide][plasmid], des
[ribosomes][ribosome] en action et une [protéine][protein]) :

![Campus du CSHL](images/cshl-3.jpg)

![Campus du CSHL](images/cshl-4.jpg)

![Maquette de ribosomes](images/cshl-5.jpg)

![Maquette de protéine](images/cshl-6.jpg)

Une maquette d'ADN dans le bâtiment de l'auditorium :

![Maquette d'ADN](images/cshl-7.jpg)

Le *Cold Spring Harbor Laboratory* est un des quelques endroits au monde où ont
eu lieu la majorité des recherches qui ont permis l'essor de la biologie
moléculaire dans les années 1960-1970. C'est pour cette raison qu'il y a toutes
ces jolies maquettes dans le campus. :-)

Notre vol de retour étant retardé, personne n'avait envie de rester dans le
campus désert tout le dimanche, et Cold Spring Harbor est situé à environ 1h de
New York en train. Nous avons donc passé le dimanche à faire du tourisme à New
York : encore une bonne surprise pour mon anniversaire ! Nous avons marché le
long de la [cinquième avenue][fifth]. Nous avons visité la
[Grande Gare Centrale][ggc], et comme le dit bien Marty : "elle est grande, et
elle est centrale" :

![La Grande Gare Centrale](images/ny-1.jpg)

![La Grande Gare Centrale](images/ny-2.jpg)

![La Grande Gare Centrale](images/ny-3.jpg)

Nous sommes passés au pied de l'[*Empire State Building*][esb] :

![L'Empire State Building](images/ny-10.jpg)

Nous avons marché un peu au hasard dans [*Central Park*][cp] :

![Central Park](images/ny-4.jpg)

![Central Park](images/ny-5.jpg)

Et nous avons visité le [*Museum of Modern Art*][moma] (MoMA) :

![Museum of Modern Art](images/ny-6.jpg)

![Museum of Modern Art](images/ny-7.jpg)

![Museum of Modern Art](images/ny-8.jpg)

![Museum of Modern Art](images/ny-9.jpg)

Je ne sais pas comment cette réplique d'entrée de métro parisien s'est retrouvée
dans ce musée...

Le soir nous avons dîné dans un restaurant indien avant de rentrer à Cold Spring
Harbor pour une courte nuit, puisque notre avion pour rentrer à Denver décollait
à 6h20 du matin. C'était déroutant de monter dans l'avion par un temps
quasi-estival et d'en sortir dans la neige quatre heure après !

En rentrant du congrès j'ai finalement enfin commencé à vraiment travailler au
labo (pas encore d'expériences, mais de la bibliographie : c'est toujours plus
intéressant que les paperasses administratives). Ce week-end je m'installe dans
l'appartement à *Marine Court*.


[csh]: https://en.wikipedia.org/wiki/Cold_Spring_Harbor,_New_York

[li]: https://fr.wikipedia.org/wiki/Long_Island

[cshl]: https://en.wikipedia.org/wiki/Cold_Spring_Harbor_Laboratory

[ggc]: https://fr.wikipedia.org/wiki/Grand_Central_Terminal

[esb]: https://fr.wikipedia.org/wiki/Empire_State_Building

[moma]: https://fr.wikipedia.org/wiki/Museum_of_Modern_Art

[fifth]: https://fr.wikipedia.org/wiki/Cinqui%C3%A8me_Avenue

[cp]: https://fr.wikipedia.org/wiki/Central_Park

[plasmid]: https://fr.wikipedia.org/wiki/Plasmide

[ribosome]: https://fr.wikipedia.org/wiki/Ribosome

[protein]: https://fr.wikipedia.org/wiki/Prot%C3%A9ine
