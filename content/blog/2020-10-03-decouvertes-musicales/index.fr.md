---
title: Découvertes musicales
author: Guillaume Gaullier
date: '2020-10-03'
slug: decouvertes-musicales
categories: []
tags:
  - music
  - sweden
lastmod: ~
authors:
  - guillaume
---


Depuis que nous sommes arrivés en Suède fin janvier, nous avons fait plusieurs
bonnes découvertes musicales (malgré les mois d'activité très réduite à cause
de la pandémie).

<!--more-->

## V-Dala Spelmanslag

La plus importante de ces découvertes, c'est le [V-Dala Spelmanslag][v-dala].
C'est un ensemble de musique traditionnelle suédoise qui se réunit chaque
semaine pour apprendre et répéter des morceaux ([des enregistrements de tous
les morceaux du répertoire sont disponibles sur le site][repertoire]), qu'ils
jouent ensuite à différentes occasions. Ce groupe nous a été recommandé par Lena
Jonsson (une violoniste suédoise [de grand talent][lena] que nous avions hébergé
une nuit lors d'un passage à Paris en 2014), quand nous lui avons écrit avant de
déménager pour lui demander si elle connaissait des musiciens avec qui jouer à
Uppsala. Nous participons à la répétition hebdomadaire, et aux concerts quand
nous pouvons. Je suis super content que la répétition soit à nouveau en personne
après des mois de répétition en vidéoconférence au printemps ; ça me manquait
plus que ce que j'imaginais. Chaque année peu après la rentrée, l'ensemble
participe à un concours de [spelmanslag][spelmanslag] étudiants pour lequel
chaque groupe doit jouer un morceau de leur choix et un morceau proposé par les
organisateurs du concours. Cette année les groupes ne se réuniront pas, à cause
de la pandémie, mais le concours a quand même lieu et nous avons enregistré
notre prestation. [La principale répétition a été enregistrée.][repetition] Le
second morceau de la suite est celui imposé à tous les groupes, j'aime beaucoup
la deuxième voix que nous avons travaillée dessus (la partie pour les vents
était beaucoup moins intéressante, mais beaucoup plus facile à jouer...).

## Festival à Håga

En mars, juste avant que tous les concerts commencent à être annulés à cause de
la pandémie, nous sommes allés à un petit festival de musique à Håga (juste à
côté d'Uppsala) où nous avons découvert quelques groupes très bons.

[Sutari][sutari] est un trio de musiciennes polonaises qui jouent et chantent
une musique très énergique :

{{< youtube id="_bNikokdFtk" class="embedded-content" >}}

[Våtmark][vatmark], le groupe que j'ai préféré dans ce festival, est un duo
composé d'une violoniste et d'une flûtiste, qui chantent aussi toutes les deux.
Nous n'avons pas compris les paroles de leurs chansons en suédois (maintenant
Rebecca les comprendrait peut-être), mais le public rigolait bien. J'ai appris
pourquoi plus tard en lisant ceci sur leur site :

> Warning! This is only for the tough ones.
Dirty and filthy songs are the core of Våtmark's repertoire.
Rediscover traditional Swedish folk music with beautiful melodies but rough lyrics.
What else to tackle the unspoken taboo of our society?
Våtmark (Wetland) offers a fun, exciting show with old and new material written by women.

{{< youtube id="yt-h8QtU9ss" class="embedded-content" >}}

[Frifot][frifot] est un groupe local d'Uppsala qui joue depuis une trentaine
d'années :

{{< youtube id="evFN77MhK08" class="embedded-content" >}}

## Bilda

En avril, nous avons pu assister à un concert à public limité d'un groupe appelé
Bilda, dans lequel joue Oskar (un des meneurs du V-Dala Spelmanslag). [Tout le
concert a été filmé.][bilda] J'aime particulièrement le morceau à 26:20 (il a la
même progression d'accords que [Boil Them Cabbage Down][cabbage] !).

## Väsen

[Väsen][vasen] est un autre groupe dont les musiciens sont originaires d'Uppsala
et Stockholm, et qui joue depuis une trentaine d'années. Nous connaissions ce
groupe avant d'emménager à Uppsala (il est mondialement connu), et étions très
impatients de les entendre en concert. En mai, ils ont donné un concert par
vidéoconférence pour un festival en Angleterre, à partir d'une salle de concert
à Uppsala. Ils étaient tristes de devoir jouer dans une salle vide devant des
caméras, donc on les a autorisé à inviter douze personnes. Roger (le guitariste)
avait enseigné un morceau lors d'une répétition du V-Dala Spelmanslag quelques
semaines auparavant, donc il a choisi d'inviter le groupe, et nous étions parmi
les douze à répondre en premier à son invitation. Voilà une photo prise pendant
ce concert (de gauche à droite: Mikael Marin, Olov Johansson, Roger Tallroth) :

![Väsen](images/vasen.jpeg)

Le morceau "Till Gösta" composé par Olov (en hommage à son père Gösta, décédé en
2018), et enregistré sur leur dernier album, m'a particulièrement touché :

{{< youtube id="jJigQ1Zpels" class="embedded-content" >}}

En septembre nous avons aussi entendu Mikael et Olov en duo pour un concert
semi-privé donné dans le jardin d'Olov, qui habite dans la campagne proche
d'Uppsala.

## Ralsgård - Tullberg quartet

Encore des musiciens que je connaissais déjà car on m'avait prêté leur premier
album quand nous habitions à Paris, [Andreas Ralsgård et Markus
Tullberg][ralsgard-tullberg] sont deux flûtistes qui jouent de la musique
traditionnelle suédoise en duo, puis plus récemment en quartet. J'ai beaucoup
écouté et apprécié leur premier album simplement en duo de flûtes, avec des
morceaux et des deuxièmes voix superbes, un peu comme ça :

{{< youtube id="jyIF8eWSJ8o" class="embedded-content" >}}

Ils devaient donner un concert avec leur quartet  à Stockholm fin avril, qui a
bien sûr été reporté à cause de la pandémie. Finalement ce concert a eu lieu fin
septembre. Voilà un des morceaux qu'ils ont joué :

{{< youtube id="NfGPXLKFEoM" class="embedded-content" >}}

Ils organisent aussi un stage de flûte chaque année en juin, auquel j'espérais
bien participer, mais qui n'a bien sûr pas eu lieu cette année... j'espère qu'il
aura lieu en 2021.


[v-dala]: https://www.v-dalaspelmanslag.se

[repertoire]: https://www.v-dalaspelmanslag.se/inspelningar-ny

[lena]: https://www.youtube.com/watch?v=INA1Knq5rv8

[spelmanslag]: https://en.wikipedia.org/wiki/Spelmanslag

[repetition]: https://www.v-dalaspelmanslag.se/vm-2020

[sutari]: https://www.sutari.pl

[vatmark]: http://www.vatmarkmusic.n.nu/lyssna

[frifot]: https://en.wikipedia.org/wiki/Frifot

[bilda]: https://www.facebook.com/BildaFolk/videos/2912973178782989

[cabbage]: https://www.youtube.com/watch?v=EJdTZna0Oe8

[vasen]: https://vasen.se

[ralsgard-tullberg]: https://www.ralsgardtullberg.com
