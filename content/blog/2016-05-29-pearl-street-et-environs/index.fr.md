---
title: "Pearl Street et les environs de Boulder"
date: 2016-05-29
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, music]
slug: pearl-street-et-environs
---


Ce week-end je me suis promené dans *Pearl Street*, et dans les environs de
Boulder (en vélo), voilà quelques photos.

<!--more-->

Le week-end dans *Pearl Street* il y a beaucoup d'artistes de rues, et ça vaut
le détour car c'est bouillonnant d'activité. Il y a notamment quelques musiciens
vraiment excellents. Voilà quelques photos prises samedi soir :

![Quatuor à cordes](images/quatuor-cordes.jpg)

![Brass band](images/brass-band.jpg)

Le groupe avec les enfants qui jouent de la trompette était incroyable !
C'est d'ailleurs [un groupe apparemment bien établi][pelican212].

Il y avait aussi de l'art éphémère dans la rue :

![Art végétal](images/art-vegetal.jpg)

Dimanche après-midi je suis parti faire un tour de vélo un peu au hasard, en
suivant plus loin la piste cyclable que je prends habituellement pour aller
au labo. Voilà ce qu'il y a après, dans la campagne :

![La campagne autour de Boulder](images/campagne-1.jpg)

![La campagne autour de Boulder](images/campagne-2.jpg)

![La campagne autour de Boulder](images/campagne-3.jpg)

Je me suis un peu perdu au retour, dans des quartiers résidentiels super chics
mais très éloignés du centre-ville... C'était une bonne façon de découvrir
les environs. :-)


[pelican212]: http://pelican212.com
