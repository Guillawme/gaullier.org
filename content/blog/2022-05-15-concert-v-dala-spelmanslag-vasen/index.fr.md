---
title: Concert de V-Dala Spelmanslag avec Väsen
author: Guillaume Gaullier
date: '2022-05-15'
slug: concert-v-dala-spelmanslag-vasen
categories: []
tags:
  - sweden
  - music
lastmod: ~
authors:
  - guillaume
---


Hier soir, le [V-Dala Spelmanslag][v-dala] (l'ensemble de musique traditionnelle
suédoise dans lequel nous jouons) était invité par le groupe [Väsen][vasen] pour
donner un concert avec eux au *Regina teatern* à Uppsala :

![Affiche du concert](images/vasen-v-dala-concert.jpg)

Nous avions répété pour ce concert pendant des semaines, et cela a valu la
peine car la soirée d'hier était inoubliable. :-)


[v-dala]: https://www.v-dalaspelmanslag.se

[vasen]: https://en.wikipedia.org/wiki/V%C3%A4sen
