---
title: New publication
author: Guillaume Gaullier
date: '2019-12-03'
slug: new-publication
categories:
  - Publications
tags:
  - work
lastmod: ~
authors:
  - guillaume
---


An article I contributed to just went online a few days ago. Here is the full
reference (also added to the [publications page][publications]):

Brown P, RELISH Consortium & Zhou Y (2019) Large expert-curated database for
benchmarking document similarity detection in biomedical literature search.
*Database* **2019**: baz085. {{< doi "10.1093/database/baz085" >}}

This article describes a search engine and database of research articles linked
by their similarity. My contribution was to use [the search engine][relish] and
manually annotate the results to record whether the articles returned were
relevant to the initial article used to start the search process. In addition of
earning me authorship on this article, using this search engine was also very
helpful while writing [a review][review].


[publications]: {{< relref path="/page/publications" lang="en" >}}

[relish]: https://relishdb.ict.griffith.edu.au/

[review]: {{< relref "2018-12-10-new-publication" >}}
