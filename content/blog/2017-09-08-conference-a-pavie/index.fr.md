---
title: "Conférence à Pavie"
date: 2017-09-08
lastmod:
authors: [guillaume]
categories: []
tags: [travels, work, italy]
slug: conference-a-pavie
---


Du 3 au 6 septembre, j'ai assisté à une conférence pour apprendre quelques bases
de microscopie électronique (j'en ai besoin pour mes recherches).
Cette conférence se tenait en Italie, dans la magnifique ville de
[Pavie][pavie]. Je n'ai pas eu beaucoup de temps pour visiter la ville, mais
voilà tout de même quelques photos.

<!--more-->

Les Alpes vues depuis l'avion, peu avant l'arrivée à Milan :

![Pavie](images/pavie-1.jpg)

Pavie :

![Pavie](images/pavie-2.jpg)

![Pavie](images/pavie-3.jpg)

L'université de Pavie est la plus ancienne de Lombardie (fondée en 1361).
L'architecture est bien jolie :

![Pavie](images/pavie-4.jpg)

![Pavie](images/pavie-5.jpg)

La place de la mairie :

![Pavie](images/pavie-6.jpg)

![Pavie](images/pavie-7.jpg)

La cathédrale. Dans la première photo, le bâtiment en briques à gauche au
premier plan est l'endroit où se tenait la plupart de la conférence.

![Pavie](images/pavie-8.jpg)

![Pavie](images/pavie-9.jpg)

![Pavie](images/pavie-10.jpg)

Le pont et les bords du [Tessin][tessin] :

![Pavie](images/pavie-11.jpg)

![Pavie](images/pavie-12.jpg)

![Pavie](images/pavie-13.jpg)

![Pavie](images/pavie-14.jpg)


[pavie]: https://fr.wikipedia.org/wiki/Pavie

[tessin]: https://fr.wikipedia.org/wiki/Tessin_(rivi%C3%A8re)
