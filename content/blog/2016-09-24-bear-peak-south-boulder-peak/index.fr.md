---
title: "Bear Peak & South Boulder Peak"
date: 2016-09-24
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, hiking]
slug: bear-peak-south-boulder-peak
---


Il y a une semaine, j'ai fait une nouvelle rando : *Bear Peak* et *South Boulder
Peak*. Voilà quelques photos.

<!--more-->

Rebecca travaillait ce samedi, c'était d'ailleurs son dernier jour, donc elle
n'a pas encore fait cette rando. J'étais invité par des voisins du *university
housing*. Je pensais que ça durerait au maximum une demi-journée, finalement
c'était toute la journée (environ 7h !). En plus ils marchaient aussi vite que
des parisiens pressés... Nous sommes partis du
[*National Center for Atmospheric Research*][ncar], avons grimpé *Bear Peak*
puis *South Boulder Peak*, et avons terminé par une longue boucle qui revenait
au centre de recherche.

Le NCAR :

![NCAR](images/ncar-1.jpg)

![NCAR](images/ncar-2.jpg)

Une vue de *Bear Peak* :

![Bear Peak](images/bear-peak-1.jpg)

D'autres belles vues pendant l'ascension de *Bear Peak* :

![Bear Peak](images/bear-peak-2.jpg)

![Bear Peak](images/bear-peak-3.jpg)

![Bear Peak](images/bear-peak-4.jpg)

*Bear Peak* (à gauche) vu depuis le sommet de *South Boulder Peak* (qui est un
peu plus élevé) :

![South Boulder Peak](images/south-boulder-peak-1.jpg)

La vue vers l'intérieur des Rocheuses depuis le sommet de *South Boulder Peak* :

![South Boulder Peak](images/south-boulder-peak-2.jpg)

Un [chipmunk][chipmunk] au sommet de *South Boulder Peak* ! (en français on
appelle ça un [tamia][tamia] semble-t-il).

![Chipmunk](images/chipmunk-1.jpg)

![Chipmunk](images/chipmunk-2.jpg)

En redescendant il y avait aussi de belles choses à voir, comme ces fleurs, et
ces rochers étrangement empilés (un dolmen américain ? :D ).

![Dolmen](images/dolmen.jpg)

![Fleurs](images/fleurs.jpg)


[ncar]: https://fr.wikipedia.org/wiki/National_Center_for_Atmospheric_Research

[chipmunk]: https://en.wikipedia.org/wiki/Chipmunk

[tamia]: https://fr.wikipedia.org/wiki/Tamia
