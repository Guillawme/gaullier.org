---
title: Nouveau déménagement
author: Guillaume Gaullier
date: '2020-04-08'
slug: nouveau-demenagement
categories: []
tags:
  - relocation
  - sweden
lastmod: ~
authors:
  - guillaume
---


Cela fait un moment que je n'ai rien écrit sur ce blog. J'essaye toujours
d'écrire un billet environ une fois par mois, tant que j'ai des choses
intéressantes à raconter, mais les deux derniers mois ont été bien chargés, si
bien que je n'ai pas pris le temps de faire ça. Après [trois ans et neufs mois à
Boulder][boulder], nous voilà en Suède, à [Uppsala][uppsala], car j'ai eu le
boulot pour lequel j'avais passé un entretien d'embauche [en août
dernier][interview].

<!--more-->

Le déménagement a été difficile pour moi, principalement car je n'étais pas
complètement prêt mentalement et émotionnellement. La raison principale était
que je ne suis pas parvenu à complètement terminer le [projet][parp2] sur lequel
je travaillais (l'article n'a pas passé le *peer review* et je ne suis plus là
pour faire les révisions nécessaires à une nouvelle tentative de publication),
et par conséquent le changement de labo et le déménagement m'ont paru prématurés
et forcés. Mais je ne pouvais pas refuser ni retarder cette opportunité dans mon
nouveau labo. Heureusement, Rebecca a eu bien plus que moi la tête sur les
épaules, et a été assez patiente pour me supporter (dans les deux sens du terme)
pendant cette période. Boulder, les Rocheuses, et tous nos amis là-bas me
manquent toujours énormément, mais maintenant que je suis de nouveau dans une
routine au labo, j'ai au moins l'impression d'avoir à nouveau un peu de contrôle
sur ma vie. Je vais donc recommencer à écrire et poster des photos. Pour
commencer, voilà quelques photos de Stockholm prises le premier week-end de
février (seulement trois jours après notre atterrissage), quand nous sommes
allés y rencontrer les propriétaires de l'appartement où nous vivons maintenant
pour signer le contrat de location.

![Stockholm](images/stockholm-1.jpeg)

![Stockholm](images/stockholm-2.jpeg)

![Stockholm](images/stockholm-3.jpeg)

Nous avons visité le [musée Nobel][nobelmuseum], qui contient plusieurs choses
en lien avec la biologie structurale (le domaine dans lequel je travaille) :

![Musée Nobel](images/nobel-museum-1.jpeg)

![Musée Nobel](images/nobel-museum-2.jpeg)

![Musée Nobel](images/nobel-museum-3.jpeg)

![Musée Nobel](images/nobel-museum-4.jpeg)

Je suis retourné à Stockholm quatre fois depuis, pour utiliser un microscope
électronique à l'[institut Karolinska][ki], mais nous n'allons pas y aller
pendant les week-ends tant que la pandémie de covid-19 n'est pas terminée (les
dernières fois, j'y suis allé en voiture avec un collègue pour éviter les
transports en commun).


[boulder]: {{< relref "2016-04-03-premiers-jours-a-boulder" >}}

[uppsala]: https://fr.wikipedia.org/wiki/Uppsala

[interview]: {{< relref "2019-09-01-week-end-a-uppsala" >}}

[parp2]: https://doi.org/10.1101/846618

[nobelmuseum]: https://nobelprizemuseum.se/en/

[ki]: https://fr.wikipedia.org/wiki/Institut_Karolinska
