---
title: Vacances d'été 2018
author: Guillaume Gaullier
date: '2018-09-08'
slug: vacances-ete-2018
categories: []
tags:
  - Oregon
  - Washington
  - travels
lastmod: ~
authors:
  - guillaume
---


Voilà quelques photos de nos vacances en Washington et Oregon pendant la
deuxième semaine d'août.

<!--more-->

Nous avons beaucoup roulé :

<div class="embedded-content">
<iframe width="100%" height="300px" frameBorder="0" src="https://framacarte.org/en/map/ete-2018_30598?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>
</div>

Après avoir atterri à SeaTac, nous sommes allés rendre visite à la grand-mère de
Rebecca qui habite à Bellingham, tout près de la frontière avec le Canada.
Voilà la vue depuis son jardin :

![Bellingham](images/bellingham.jpg)

Nous sommes restés une nuit chez elle, puis nous sommes partis pour Centralia 
où nous avons passé trois jours en camping avec le petit frère de Rebecca et
leur père, pour [une sorte de festival][campout]. Voilà une vue du camping :

![Centralia](images/centralia-1.jpg)

Il n'y avait pas de concert, seulement des sessions informelles et des ateliers
de musique et chant plus ou moins improvisés. Nous avons enseigné à quelques
personnes deux morceaux bretons et les danses qui vont avec. Nous avons aussi
suivi un atelier animé par [Lisa Ornstein][lisa]. Cet été, des incendies de
forêt ont ravagé la côte ouest du continent de la Californie à la Colombie
Britannique. Même en étant très loin des flammes, l'air était très enfumé :

![Centralia](images/centralia-2.jpg)

Après cela, nous avons roulé jusqu'à [Astoria][astoria], une petite ville
portuaire à l'embouchure du [fleuve Columbia][columbia]. À cet endroit de la
côte du Pacifique, c'est apparemment normal qu'il fasse un temps plus agréable
[en hiver][pacifique]. En août, il faisait plutôt gris et froid...

Voilà le pont qui traverse le fleuve Columbia, reliant les états du Washington
et de l'Oregon :

![Astoria](images/astoria-1.jpg)

![Astoria](images/astoria-2.jpg)

![Astoria](images/astoria-3.jpg)

Ce bateau est un ancien phare flottant, et fait maintenant partie du musée qui
est sur la photo suivante :

![Astoria](images/astoria-4.jpg)

![Astoria](images/astoria-5.jpg)

Nous avons visité le musée en vitesse car il fermait peu de temps après notre
arrivée :

![Musée](images/musee-1.jpg)

![Musée](images/musee-2.jpg)

![Musée](images/musee-3.jpg)

Après avoir passé l'après-midi à Astoria, nous sommes allés installer notre
tente au camping de [*Fort Stevens State Park*][fort-stevens]. Nous sommes allés
faire un tour à la plage toute proche avant la nuit :

![Fort Stevens](images/fort-stevens-1.jpg)

![Fort Stevens](images/fort-stevens-2.jpg)

![Fort Stevens](images/fort-stevens-3.jpg)

Le lendemain matin, avant de reprendre la route, nous sommes allés jusqu'au bout
de la pointe qui avance dans l'embouchure du fleuve :

![Fort Stevens](images/fort-stevens-4.jpg)

![Fort Stevens](images/fort-stevens-5.jpg)

Dans la vue suivante, le Pacifique se trouve à gauche de cette jetée et le
fleuve juste à droite (hors du cadre) :

![Fort Stevens](images/fort-stevens-6.jpg)

On regarde maintenant vers le sud, avec le Pacifique sur la droite :

![Fort Stevens](images/fort-stevens-7.jpg)

En route pour Dayton, notre prochaine étape pour aller rendre visite au
grand-père (de l'autre côté de la famille) de Rebecca, nous nous sommes arrêtés
à Gearhart pour le petit déjeûner. Ce petit village a une vraie pâtisserie !

![Gearhart](images/gearhart.jpg)

Nous voilà à Dayton, une petit ville très calme :

![Dayton](images/dayton-1.jpg)

Les noisetiers se portent toujours bien depuis
[notre dernier passage][dayton2017] :

![Dayton](images/dayton-2.jpg)

![Dayton](images/dayton-3.jpg)

Nous avons passé les deux derniers jours du voyage à Lebanon, chez les parents
de Rebecca. Nous avons visité [un ancien moulin][mill] qui a été en activité
pendant plus de 100 ans et est maintenant un musée.

![Thompson's Mills](images/mill-1.jpg)

![Thompson's Mills](images/mill-2.jpg)

Il n'y a pas tout à fait 50 étoiles sur ce drapeau, il doit être ancien :

![Thompson's Mills](images/mill-3.jpg)

![Thompson's Mills](images/mill-4.jpg)

![Thompson's Mills](images/mill-5.jpg)

![Thompson's Mills](images/mill-6.jpg)

![Thompson's Mills](images/mill-7.jpg)

![Thompson's Mills](images/mill-8.jpg)

![Thompson's Mills](images/mill-9.jpg)

Nous avons aussi visité le carrousel de la petite ville d'Albany, qui a ouvert
très récemment.

![Carrousel](images/carousel-1.jpg)

![Carrousel](images/carousel-2.jpg)

![Carrousel](images/carousel-3.jpg)

Il est entièrement géré par des bénévoles, y compris pour la fabrication de
nouvelles pièces pour le manège. L'atelier est ouvert aux visiteurs, et contient
un bestiaire impressionnant d'animaux en bois peint destinés à compléter le
manège. Chaque pièce est financée par des dons (les donateurs peuvent choisir un
animal et un nom), puis sculptée et peinte par des bénévoles.

![Carrousel](images/carousel-4.jpg)

![Carrousel](images/carousel-5.jpg)

![Carrousel](images/carousel-6.jpg)

![Carrousel](images/carousel-7.jpg)

![Carrousel](images/carousel-8.jpg)

![Carrousel](images/carousel-9.jpg)

![Carrousel](images/carousel-10.jpg)

Dans les photos suivantes, on voit que les dessins qui servent de modèles aux
sculpteurs sont déjà splendides.

![Carrousel](images/carousel-11.jpg)

![Carrousel](images/carousel-12.jpg)

![Carrousel](images/carousel-13.jpg)

![Carrousel](images/carousel-14.jpg)

![Carrousel](images/carousel-15.jpg)

![Carrousel](images/carousel-16.jpg)

![Carrousel](images/carousel-17.jpg)

![Carrousel](images/carousel-18.jpg)

![Carrousel](images/carousel-19.jpg)

![Carrousel](images/carousel-20.jpg)

Les détails sont impressionnants, comme par exemple les plumes du griffon :

![Carrousel](images/carousel-21.jpg)

![Carrousel](images/carousel-22.jpg)

À notre retour à Boulder, nous avons vu à nouveau de la fumée en provenance des
incendies en Colombie Britannique. C'est très très loin, mais il suffisait que
le vent souffle dans notre direction pour recevoir des quantités inquiétantes de
fumée. Voilà la vue depuis l'entrée du bâtiment où je travaille, en début et fin
de journée le lendemain de notre retour :

![Smoky Boulder](images/smoky-boulder-1.jpg)

![Smoky Boulder](images/smoky-boulder-2.jpg)


[campout]: http://www.centraliacampout.com/

[lisa]: http://www.lisaornstein.com/bio/

[astoria]: https://fr.wikipedia.org/wiki/Astoria_(Oregon)

[columbia]: https://fr.wikipedia.org/wiki/Columbia_(fleuve)

[pacifique]: {{< relref "2017-01-30-fetes-en-oregon" >}}

[fort-stevens]: https://fr.wikipedia.org/wiki/Fort_Stevens_(Oregon)

[dayton2017]: {{< relref "2017-07-29-voyage-en-bateau" >}}

[mill]: https://en.wikipedia.org/wiki/Thompson%27s_Mills_State_Heritage_Site
