---
title: Öregrund
author: Guillaume Gaullier
date: '2020-04-26'
slug: oregrund
categories: []
tags:
  - sweden
lastmod: ~
authors:
  - guillaume
---


Voilà quelques photos d'une promenade que nous avons faite à [Öregrund][oregrund]
le 14 mars. C'était un des premiers week-ends ensoleillé depuis notre arrivée en
Suède, donc nous avons pris un bus pour rejoindre ce petit village [sur la côte
de la mer Baltique][oregrund-map], à une heure et demi d'Uppsala.

Voilà une vue du port :

![Öregrund](images/oregrund-1.jpeg)

J'ai bien aimé ce signe "attention au chat" sur le portail d'une maison du
village :

![Öregrund](images/oregrund-2.jpeg)

![Öregrund](images/oregrund-3.jpeg)

![Öregrund](images/oregrund-4.jpeg)

Il ne faisait pas trop froid à l'abri du vent et au soleil, mais comme on le
devine dans la première photo, l'eau de mer était gelée sur le rivage :

![Öregrund](images/oregrund-5.jpeg)

![Öregrund](images/oregrund-6.jpeg)

Quand il fera plus chaud, nous y retournerons pour visiter l'île de
[Gräsö][graso] juste en face d'Öregrund, qu'on peut apercevoir dans cette
dernière photo.


[oregrund]: https://en.wikipedia.org/wiki/%C3%96regrund

[oregrund-map]: https://goo.gl/maps/LVP1ysrF4kyKi5tu5

[graso]: https://en.wikipedia.org/wiki/Gr%C3%A4s%C3%B6
