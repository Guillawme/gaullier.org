---
title: Billets de blog
linkTitle: Blog
description: Surtout des photos. J'écris sur mon travail et la science sur le blog en anglais.
menu: main
weight: 1
slug: blog
---

![Icône RSS](/images/rss.svg) Flux: [RSS]({{< relref path="_index.fr.md" outputFormat="rss" >}}), [JSON]({{< relref path="_index.fr.md" outputFormat="json" >}}).
([Qu'est-ce qu'un flux ? [en]](https://aboutfeeds.com/))
