---
title: New preprint
author: Guillaume Gaullier
date: '2024-02-17'
slug: new-preprint
categories:
  - Preprints
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
bibliography: ../../static/bibliography/references.bib
csl: ../../static/bibliography/the-embo-journal.csl
link-citations: yes
---


I have been too busy until now to write a note here, so this is old news
already, but back in September I helped some people visually inspect cryoEM maps
and make a figure, and they were kind enough to list me as author on a preprint
they updated on [bioRxiv][biorxiv] back in October. Here is the full reference:

Sanchez-Garcia R, **Gaullier G**, Cuadra-Troncoso JM & Vargas J (2023) Cryo-EM
map anisotropy can be attenuated by map post-processing and a new method for its
estimation. *bioRxiv*: **2022.12.08.517920**
{{< doi "10.1101/2022.12.08.517920" >}}

I also added it to the [publications page][publications].

[biorxiv]: https://www.biorxiv.org

[publications]: {{< relref path="/page/publications" lang="en" >}}
