---
title: "Dinosaur Ridge"
date: 2017-08-06
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: dinosaur-ridge
---


Le week-end dernier nous sommes allés voir [*Dinosaur Ridge*][dino-ridge], un
site paléontologique à moins d'une heure de route au sud de chez nous.
Il y a principalement des traces de pas de dinosaures, et aussi quelques os
fossilisés. Voilà quelques photos.

<!--more-->

![Dinosaur Ridge](images/dino-1.jpg)

![Dinosaur Ridge](images/dino-2.jpg)

![Dinosaur Ridge](images/dino-3.jpg)

![Dinosaur Ridge](images/dino-4.jpg)

![Dinosaur Ridge](images/dino-5.jpg)

![Dinosaur Ridge](images/dino-6.jpg)

![Dinosaur Ridge](images/dino-7.jpg)

![Dinosaur Ridge](images/dino-8.jpg)

![Dinosaur Ridge](images/dino-9.jpg)


[dino-ridge]: https://en.wikipedia.org/wiki/Dinosaur_Ridge
