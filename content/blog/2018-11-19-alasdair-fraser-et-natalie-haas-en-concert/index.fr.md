---
title: Alasdair Fraser et Natalie Haas en concert
author: Guillaume Gaullier
date: '2018-11-19'
slug: alasdair-fraser-natalie-haas-concert
categories: []
tags:
  - music
lastmod: ~
authors:
  - guillaume
---


Avant-hier nous avons entendu Alasdair White et Natalie Haas en concert à
Lakewood (près de Denver). J'avais déjà entendu parler de ce duo, mais je
n'avais jamais vraiment écouté leur musique de façon approfondie. C'était un
excellent concert !

<!--more-->

Ils sont non seulement d'excellents musiciens, mais leur répertoire est aussi
varié et brillament arrangé. Voilà deux exemples pris à peu près au hasard :

{{< youtube id="mmTz8Afy5Lg" class="embedded-content" >}}

{{< youtube id="dXXkz5BvxVo" class="embedded-content" >}}

La grande surprise du concert, c'est qu'à la toute fin lorsqu'ils sont revenus
pour le rappel ils ont demandé aux spectateurs de ne pas se rassoir, ont
commencé à enseigner l'[an dro][andro] et ont carrément transformé le rappel en
un mini [fest-noz][festnoz] ! Des amis qui avaient assisté à d'autres de leurs
concerts nous ont dit qu'ils font danser leur audience d'une façon ou d'une
autre à chaque fois.


[andro]: https://fr.wikipedia.org/wiki/An-dro

[festnoz]: https://fr.wikipedia.org/wiki/Fest-noz
