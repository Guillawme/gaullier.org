---
title: New publication
author: Guillaume Gaullier
date: '2018-12-10'
slug: new-publication
categories:
  - Publications
tags:
  - work
lastmod: ~
authors:
  - guillaume
---


A review article I contributed to was published today in [*NSMB*][nsmb]. Here is
the full reference (also added to the [publications page][publications]):

Zhou K, **Gaullier G** & Luger K (2018) Nucleosome structure and dynamics are
coming of age. *Nature Structural & Molecular Biology*.
{{< doi "10.1038/s41594-018-0166-x" >}}


[nsmb]: https://www.nature.com/nsmb/

[publications]: {{< relref path="/page/publications" lang="en" >}}
