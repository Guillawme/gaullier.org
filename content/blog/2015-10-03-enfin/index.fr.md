---
title: "Enfin"
date: 2015-10-03
lastmod:
authors: [guillaume]
categories: []
tags: [work]
slug: enfin
---


Ma thèse est *enfin* terminée. C'est un grand soulagement, après plus de trois
ans sur un même projet (qui a débuté avec mes stages de master, et a continué
par la thèse financée pour trois ans), de pouvoir enfin penser à d'autres
choses. Même si parmi ces autres choses c'est la recherche d'un emploi qui va
maintenant être le projet prioritaire. J'aurai néanmoins plus de temps
à accorder à des projets annexes, et c'est pour occuper une partie de ce temps
que je souhaite m'essayer à la tenue d'un blog.

<!--more-->

La thèse dans son ensemble est riche en expériences, et tout particulièrement
les derniers mois. Beaucoup de réflexions me tournent maintenant dans la tête,
et puisque je compte les écrire (cet exercice me semble intéressant), autant le
faire publiquement si cela peut être utile à d'autres. Je ne me fixe pas de
thématique particulière, j'écrirai simplement ce qui me passe par la tête et
peut présenter un quelconque intérêt. Certains articles seront probablement
rédigés en anglais si leur sujet mérite une plus large visibilité (voyez cela
comme une déformation professionnelle : les scientifiques communiquent en
anglais). Les pages statiques (page "à propos", etc.) seront rédigées
exclusivement en anglais, car je compte me servir de ce petit site pour établir
un peu plus de visibilité en ligne pour des gens potentiellement intéressés par
mes travaux (et par mon CV...), c'est-à-dire par des chercheurs (ai-je déjà dit
que les chercheurs communiquent en anglais ?). Bien entendu les idées exprimées
ici sont uniquement les miennes (sauf *explicitement* indiqué) et ne sauraient
être associées à un quelconque employeur passé, présent ou futur auquel je
serais lié.

Puisque c'est la première fois que je me livre à cet exercice, je ne sais pas
encore à quelle fréquence je serai capable d'écrire. D'autre part, j'apprécie
parfois des périodes sans écrans plus ou moins longues. Pour ces deux raisons,
n'attendez pas des mises à jour très fréquentes.
