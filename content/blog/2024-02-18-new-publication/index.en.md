---
title: New publication
author: Guillaume Gaullier
date: '2024-02-18'
slug: new-publication
categories:
  - Publications
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
bibliography: ../../static/bibliography/references.bib
csl: ../../static/bibliography/the-embo-journal.csl
link-citations: yes
---


Earlier this month, an article that was long in the making was finally published:

Bacic L, **Gaullier G**, Mohapatra J, Mao G, Brackmann K, Panfilov M, Liszczak G,
Sabantsev A & Deindl S (2024) Asymmetric nucleosome PARylation at DNA breaks
mediates directional nucleosome sliding by ALC1. *Nat Commun* **15**: 1000
{{< doi "10.1038/s41467-024-45237-8" >}}

This study confirmed a hypothesis that had formed after our [previous work on
ALC1][bacic-gaullier-2021], and these things don't happen very often so it is
particularly nice to finally see this article published.

I added it to the [publications page][publications]. I also posted [a summary of
this article][tldr-thread] on Mastodon.

[bacic-gaullier-2021]: {{< relref "2021-09-28-new-publication" >}}

[publications]: {{< relref path="/page/publications" lang="en" >}}

[tldr-thread]: https://fediscience.org/@Guillawme/111954509979706938
