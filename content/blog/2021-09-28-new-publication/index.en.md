---
title: New publication
author: Guillaume Gaullier
date: '2021-09-28'
slug: new-publication
categories:
  - Publications
tags:
  - work
  - cryo-em
lastmod: '2024-02-18'
authors:
  - guillaume
---


Our [preprint from last June][preprint] was published in its peer-reviewed form
earlier this month! Here is the full reference:

Bacic L, **Gaullier G**, Sabantsev A, Lehmann LC, Brackmann K, Dimakou D,
Halic M, Hewitt G, Boulton SJ & Deindl S (2021) Structure and dynamics of the
chromatin remodeler ALC1 bound to a PARylated nucleosome. *eLife* **10**:
e71420. {{< doi "10.7554/eLife.71420" >}}

I added it to the [publications page][publications]. I also posted [a
summary][tldr-thread-twitter] on Twitter.

**Update on 18 February 2024**: I reposted the entire summary thread [on
Mastodon][tldr-thread-mastodon].


[preprint]: {{< relref "2021-06-20-new-preprint" >}}

[publications]: {{< relref path="/page/publications" lang="en" >}}

[tldr-thread-twitter]: https://twitter.com/Guillawme/status/1442828837251911682

[tldr-thread-mastodon]: https://fediscience.org/@Guillawme/111534984107819771
