---
title: "Découvertes musicales"
date: 2018-04-24
lastmod:
authors: [guillaume]
categories: []
tags: [music, Colorado]
slug: decouvertes-musicales
---


Je n'ai rien écrit ici depuis bien longtemps : pas de rando récente à raconter,
ni vraiment grand-chose d'autre... Mais nous allons quand même de temps en temps
écouter des concerts, alors je vais simplement lister quelques découvertes
musicales plus ou moins récentes.

<!--more-->

Le premier groupe qui m'a bien plu depuis que j'habite dans cette région est
[Masontown][masontown], un groupe de bluegrass local découvert au [*Wildflower
Pavilion*, dans la ville voisine de Lyons][wildflower]. Ils jouent des morceaux
traditionnels, et aussi leurs propres compositions. Voilà un exemple :

{{< youtube id="hf6unk9WQ0M" class="embedded-content" >}}

Nous avons découvert cet autre excellent groupe, [The Western
Flyers][western-flyers], dans une salle de concert et de danse appellée [*The
Barn*][barn] ("la grange") à Longmont. Voilà à quoi ressemble leur
musique :

{{< youtube id="m3DrX5ttiU8" class="embedded-content" >}}

Le concert que j'ai le plus apprécié jusque-là était donné au départment de
musique de l'université à l'occasion du [*Recreate Your Roots
festival*][cu-fest]. Nous y avons entendu [*Jayme Stone's Folklife
Project*][folklife] : des morceaux traditionnels et chansons des États-Unis et
des Caraïbes, arrangés et interprétés dans un style contemporain par quatre
excellents musiciens et chanteurs. C'était absolument formidable de les entendre
et voir jouer sur scène ! Beaucoup d'énergie, mais aussi beaucoup de subtilité
dans les arrangements et l'interprétation.

À ce même *Recreate Your Roots festival*, la semaine suivante, deux autres
formations étaient invitées : [Dom Flemons][flemons] (un des musiciens
fondateurs du groupe [*Carolina Chocolate Drops*][chocolate]), et [Anna &
Elizabeth][ae]. Le premier est un multi-instrumentiste et chanteur accompli. Le
duo était une jolie surprise : elles ont joué des airs instrumentaux
traditionnels, mais ont aussi chanté des chansons illustrées par des dessins
déroulés au fil de la chanson sur des rouleaux de toile. C'est une chouette
façon de raconter des histoires.

Pour ce concert, nous étions censé garder le fils d'un couple d'amis pendant
qu'ils allaient écouter le concert (nous avions réagi trop tard, toutes les
places étaient déjà vendues). Pas de chance pour eux, le petit gars a fait
pousser trois dents la veille du concert et était de si mauvaise humeur qu'au
lieu de nous le confier, ils ont préféré nous donner leurs places pour le
concert. Le groupe que nous avons entendu s'appelle [*I'm with Her*][her] et
joue ses propres compositions et des morceaux bluegrass. Ce n'est pas mon
répertoire et style de musique préféré, mais les trois musiciennes sont encore
une fois tellement excellentes que j'ai passé un très bon moment.

Finalement, le dernier concert que nous avons écouté était donné par une
guitariste appelée [Molly Tuttle][molly], dans une formation avec une
contrebassiste, un violoniste et un banjoïste. Et je crois que c'est le deuxième
concert que j'ai le plus apprécié cette année après Jayme Stone's Folklife. En
particulier, cette musicienne utilise une technique appellée
[*clawhammer*][clawhammer] qui est apparemment plus couramment utilisée au
banjo. Voilà un exemple :

{{< youtube id="xbaAmr9LXKA" class="embedded-content" >}}

L'année n'est pas terminée, nous allons sûrement découvrir encore d'autres
groupes et musiciens épatants. :-)


[masontown]: http://www.masontownmusic.com

[wildflower]: https://www.bluegrass.com/wildflower

[western-flyers]: http://thewesternflyers.com

[barn]: http://www.barnevents.info

[cu-fest]: https://www.colorado.edu/music/2018/01/05/recreate-your-roots-festival-presents-music-people-people

[folklife]: http://jaymestone.com/projects/folklife

[flemons]: https://theamericansongster.com

[chocolate]: https://en.wikipedia.org/wiki/Carolina_Chocolate_Drops

[ae]: https://www.annaandelizabeth.com

[her]: https://imwithherband.com

[molly]: https://www.mollytuttlemusic.com

[clawhammer]: https://en.wikipedia.org/wiki/Clawhammer
