---
title: Retraite 2018
author: Guillaume Gaullier
date: '2018-09-18'
slug: retraite-2018
categories: []
tags:
  - hiking
  - Colorado
  - work
lastmod: ~
authors:
  - guillaume
---


L'année dernière j'ai raté la retraite du département car elle a été annoncée
seulement deux semaines à l'avance, et nous avions déjà planifié notre week-end.
Cette année, elle a été annoncée suffisamment à l'avance pour que je puisse y
participer. C'était vendredi et samedi dernier à [*Snow Mountain Ranch*][smr],
au milieu des Rocheuses.

<!--more-->

Avec les deux collègues avec qui j'ai covoituré, nous nous sommes arrêtés sur la
route pour déjeûner dans un minuscule village appelé *Empire* :

![Empire, CO](images/empire.jpg)

Cette retraite était bien mieux organisée qu'[il y a deux ans][retreat-2016] (je
n'avais pas eu le temps de prendre beaucoup de photos). Cette année, il y avait
deux heures de temps libre pour faire une randonnée dans les environs.

Voilà la vue depuis ma chambre (vers l'ouest, puis le sud, puis l'est) :

![Vue](images/vue-ouest.jpg)

![Vue](images/vue-sud.jpg)

![Vue](images/vue-est-1.jpg)

![Vue](images/vue-est-2.jpg)

![Vue](images/vue-est-3.jpg)

Et voilà des photos prises pendant la rando :

![Rando](images/rando-1.jpg)

![Rando](images/rando-2.jpg)

![Rando](images/rando-3.jpg)

![Rando](images/rando-4.jpg)

![Rando](images/rando-5.jpg)

![Rando](images/rando-6.jpg)

![Rando](images/rando-7.jpg)

![Rando](images/rando-8.jpg)

![Rando](images/rando-9.jpg)


[smr]: https://goo.gl/maps/rBvYAjTna2P2

[retreat-2016]: {{< relref "2016-10-23-la-retraite" >}}
