---
title: "March for Science"
date: 2017-04-22
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: march-for-science
---


Aujourd'hui avaient lieu partout dans le monde des "marches citoyennes pour la
science". Voilà quelques photos de l'événement à Denver.

<!--more-->

Il y avait cette fois beaucoup moins de monde que pour la *Women's March* (au
sujet de laquelle [j'ai déjà écrit][womensmarch]), mais toujours la même
ambiance accueillante et presque festive. J'ai apprécié les nombreux jeux de
mots scientifiques sur les panneaux. Voilà quelques photos de panneaux que j'ai
aperçus pendant la marche :

![Alternative facts are imaginary](images/march-for-science-1.jpg)

![Nerds need jobs too](images/march-for-science-2.jpg)

![Be like a proton, stay positive](images/march-for-science-3.jpg)

![March for Science](images/march-for-science-4.jpg)

![Don't let science go extinct](images/march-for-science-5.jpg)

![Less invasions, more equations](images/march-for-science-6.jpg)

![Scientific progress goes "boink"](images/march-for-science-7.jpg)

![Show me the data](images/march-for-science-8.jpg)

![Trump's p-values are all insignificant](images/march-for-science-9.jpg)

![Be a part of the solution](images/march-for-science-10.jpg)

![No science? No beer!](images/march-for-science-11.jpg)


[womensmarch]: {{< relref "2017-01-22-womens-march-on-denver" >}}
