---
title: 'Gmail et sa fonction "Archiver"'
date: 2015-11-02
lastmod:
authors: [guillaume]
categories: []
tags: [internet]
slug: gmail-et-sa-fonction-archiver
---


Gmail a archivé ma vie depuis 2007. Plus précisément, le message le plus ancien
que je retrouve au fin fond du dossier "Tous les messages" de mon compte Gmail
date du 28 janvier 2007. Je ne sais plus à quelle date précise remonte
l'ouverture de ce compte, mais je me souviens très bien que c'était pendant mon
année de terminale (2006-2007), et que c'était dans la période où Gmail n'était
disponible que sur invitation. À l'époque la capacité de stockage proposée
(autour de 1 Go) était incroyable, et Gmail nous assurait que nous n'avions plus
à jeter un seul email. Le fameux webmail intelligent nous a donc bien éduqués
à utiliser le bouton "Archiver" plutôt que "Supprimer", dans mon cas avec un
succès certain puisque je retrouve aujourd'hui des messages dont la date est
proche de la date d'ouverture du compte.

<!--more-->

Dans une récente série d'articles,
[partant du constat qu'elle utilise Gmail depuis environ une dizaine
d'années][gmail-10-ans] (soit un temps considérable à l'échelle d'internet),
Poulpette [parle des raisons qui la poussent à vouloir s'en éloigner et commente
quelques alternatives][quitter-gmail], avec comme souci principal (et tout
à fait légitime) le stockage et l'utilisation de ses messages par la firme.
Pour ma part, je ne compte pas abandonner mon adresse Gmail (du moins pas dans
un futur proche). Dans cet article je livrerai quelques réflexions que je me
suis récemment faites au sujet de mon utilisation des emails en général, et de
Gmail en particulier. Certaines de ces réflexions valent pour toute forme de
stockage de données en ligne.

## L'analyse des messages et la publicité ciblée

La première critique évidente à l'encontre de Gmail, c'est bien entendu le fait
que le contenu des messages soit analysé pour permettre entre autres choses
l'affichage de publicités ciblées dans le webmail. À cette fin, Gmail a tout
intérêt à ce que l'utilisateur ne supprime aucun message. En effet, plus il
y a de messages à analyser et plus la prédiction des centres d'intérêt de
l'utilisateur est fiable, donc plus les publicités affichées seront
potentiellement cliquées.

Du point de vue de l'expérience utilisateur, ça n'a jamais été un énorme
problème pour moi : les publicités sont très discrètes, elles sont d'autre part
aisément filtrées par des extensions du navigateur comme uBlock, AdBlock et
consorts, et dans le cas où les messages sont rapatriés dans un client de
messagerie tout contact avec la publicité est évité. En revanche d'un point de
vue plus général, c'est évidemment inquiétant de savoir que des communications
en apparence privées sont en fait analysées dans leurs moindres détails. Il est
néanmoins possible d'éviter d'exposer le *contenu* des messages, en utilisant
par exemple le chiffrement par le [protocole OpenPGP][openpgp].
Malheureusement cette technologie n'est efficace que si les deux correspondants
l'utilisent, et l'utiliser correctement nécessite une compréhension au moins
sommaire de son fonctionnement. Or la majorité des utilisateurs actuels n'est
pas suffisamment sensibilisée au problème de la confidentialité des données, et
n'a par conséquent aucune motivation pour apprendre à utiliser ce système.
J'ai tout de même décidé de l'utiliser, même si pour le moment je ne m'en sers
que pour signer des fichiers (faute d'avoir des correspondants qui l'utilisent).
Ma clé publique OpenPGP est disponible sur la [page de contact][contact] du
site. Avec un usage simple, cette technologie ne donne cependant qu'une illusion
de vie privée : certes le contenu des messages n'est désormais lisible que par
l'expéditeur et le destinataire, mais Gmail ou tout autre intermédiaire peut
toujours analyser les *métadonnées* qui se révèlent aussi extrêmement
informatives (qui communique avec qui ? à quelle fréquence ?).

Récemment, de nouveaux fournisseurs d'emails se sont donnés pour objectif de
rendre les échanges confidentiels. Parmi ceux-ci, le service ProtonMail est
devenu relativement célèbre. De nombreuses revues sont disponibles en ligne
(voyez par exemple [celle-ci][revue-protonmail]), je ne commenterai donc pas son
fonctionnement. J'ai ouvert un compte sur ce service pour le tester,
l'expérience utilisateur est agréable mais je trouve que, quitte à utiliser
OpenPGP, autant en faire un choix éclairé et l'utiliser pleinement, plutôt que
par l'intermédiaire d'un service "clés en main" comme ProtonMail.
L'avantage étant que je peux utiliser ma clé OpenPGP avec d'autres adresses
email si j'en ai besoin, et je peux m'en servir pour autre chose que l'échange
d'emails (comme signer des fichiers, etc.). Mais ces services ont le mérite de
populariser la problématique de la confidentialité des données, et peuvent donc
aider la prise de conscience chez les utilisateurs lambda. Le problème des
métadonnées est selon moi insoluble : en effet, si ProtonMail et consorts
garantissent une certaine sécurité aux échanges entre leurs adresses, il ne
peuvent pas éviter la collecte de métadonnées lorsqu'une de leurs adresses
communique avec une adresse d'un autre fournisseur (par exemple Gmail).

## Le comportement de l'utilisateur vis-à-vis des emails

Gmail et sa fonction "Archiver" ont aussi changé la façon dont j'utilise les
emails. Comme je le disais dans l'introduction, j'ai rapidement pris l'habitude
d'archiver tous les emails et je n'ai utilisé que rarement la suppression.
Bien entendu j'ai eu d'autres adresses email avec un quota de stockage beaucoup
plus réduit que celui offert par Gmail, notamment avec les adresses email
offertes par les universités dans lesquelles j'ai été inscrit au cours de mes
études (l'Université de Bretagne-Sud, l'Université Pierre et Marie Curie alias
Paris 6, et l'Université Paris-Sud alias Paris 11). Malgré leur faible quota de
stockage, ces adresses n'ont pas changé l'habitude prise avec Gmail : en effet,
elles permettaient de rapatrier les messages ou de les transférer
automatiquement vers un autre compte, ainsi j'ai toujours utilisé ces adresses
à travers Gmail (et j'ai donc archivé les conversations tenues avec ces
adresses), ce qui nécessitait seulement de purger les messages stockés sur le
serveur de l'université de temps en temps, lorsque le quota était atteint.

Ce qui m'a fait prendre conscience de cette habitude fortement ancrée d'utiliser
la fonction "Archiver", c'est l'adresse email professionnelle qui m'a été
fournie pendant ma thèse, avec son quota de 300 Mo... et l'impossibilité totale
de transférer les emails vers un autre compte ou de les rapatrier par les
protocoles POP ou IMAP. Je me suis vite aperçu que je remplissais le quota très
rapidement et régulièrement, non pas à cause de messages trop fréquents (le
débit de messages se limitait à quelques-uns envoyés et reçus chaque jour, en
moyenne) mais surtout à cause de mon habitude, prise en utilisant Gmail, de ne
rien supprimer. Cette adresse professionnelle avec son faible quota m'a forcé
à me confronter au problème de la gestion des vieux messages, et il m'a suffit
de créer quelques dossiers (là les *tags* de Gmail m'ont vraiment manqué...) et
règles de tri automatique pour maitriser le remplissage du quota. Pour parvenir
à maitriser le quota, j'ai aussi dû reprendre l'habitude de supprimer des
messages. Faire du ménage dans une boîte email de 300 Mo prend un peu de temps,
mais ce n'est pas insurmontable. De plus, il est facile de voir rapidement si un
email professionnel doit être conservé plus longtemps ou pas (la plupart de ces
emails concernent des *deadlines* donc une fois la date passée on peut supprimer
le message). Une fois le premier ménage réalisé, il suffit donc de garder de
bonnes habitudes et la liste de messages garde des proportions raisonnables
à tout instant.

Mais avec une boîte email de 4,5 Go (la taille actuelle de ma boîte Gmail),
c'est une autre galère... en plus, contrairement aux emails professionnels, on
peut plus facilement s'attacher à des emails personnels et être tenté de garder
certaines conversations. J'ai néanmoins décidé de faire le ménage dans ces
milliers d'emails qui s'amassent depuis 2007. Je le fais progressivement lorsque
j'ai vraiment du temps à tuer, et tout en écoutant un bon album pour faire en
sorte que ce temps ne soit pas complètement perdu. En parallèle de cette
opération de ménage, je recommence à supprimer des messages au fur et à mesure
de leur arrivée, lorsqu'ils deviennent inutiles. Une bonne raison pour
entreprendre ce grand ménage, plus que le fait de réduire la quantités de
messages disponibles pour le système d'analyse de Gmail (de toute façon depuis
2007 il doit avoir une idée très précise de ce qui m'intéresse), c'est aussi que
le "tout en ligne" (ou *cloud* comme disent les anglophones) est atrocement
consommateur d'énergie. Imaginez : chacun des environ 900 millions
d'utilisateurs de Gmail a potentiellement 15 Go (l'espace offert actuellement)
de données qui dorment quelque part, et sont inutilisées la plupart du temps
(personne ne relit ses emails archivés très souvent), mais nécessitent néanmoins
que des milliers (millions ?) de serveurs restent en fonctionnement 24h sur 24.
Ajoutez à cela la consommation électrique des systèmes de refroidissement
nécessaires au bon fonctionnement de tous ces serveurs. En clair, actuellement
une *énorme* quantité d'énergie est employée à maintenir en ligne *des masses de
données* quasiment **jamais utilisées** par leurs propriétaires. Tout cela
à l'heure où l'énergie est plus précieuse que jamais, dans un contexte où l'on
prend conscience des limites des ressources non renouvelables à l'échelle
humaine (les combustibles fossiles en tête). Ceci représente pour moi une grande
aberration, et j'ai envie de limiter autant que possible ma contribution à ce
phénomène. Bien sûr, mettre en ligne un article de blog va complètement
à l'encontre de cette idée... à la différence près que cet article est public et
sera potentiellement lu beaucoup de fois (je l'espère) tandis que les emails
archivés ne sont quasiment pas utilisés. D'autre part, le fichier contenant cet
article pèse environ 11 ko, soit environ 409 000 fois moins que les 4,5 Go que
pèse ma boîte Gmail à l'heure où j'écris. Si je parviens donc à faire
suffisamment de ménage dans mes emails pour réduire l'espace occupé à moins d'un
Go, et si on suppose que je n'arriverai jamais à écrire 409 000 articles de la
taille de celui-ci sur ce blog (ce qui me semble une supposition tout à fait
raisonnable, car à raison d'un article par jour il faudrait environ 1120 ans
pour atteindre les 409 000 articles publiés), l'impact énergétique de mes
activités en ligne sera donc plus économique qu'il ne l'est à l'heure
où j'écris.


[gmail-10-ans]: http://www.poulpettedelamare.com/2015/09/18/Menage-automne-et-controle-des-donnees/

[quitter-gmail]: http://www.poulpettedelamare.com/2015/09/26/seloigner-de-gmail-protonmail-posteo-et-serveur-de-confiance/

[openpgp]: https://fr.wikipedia.org/wiki/Pretty_Good_Privacy

[contact]: {{< relref path="/page/contact" lang="fr" >}}

[revue-protonmail]: http://www.arobase.org/messageries/protonmail.htm
