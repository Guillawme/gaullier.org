---
title: "Women's March on Denver, 2018 edition"
date: 2018-01-22
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: womens-march-on-denver-2018
---


Cela fait déjà un an que Trump a pris ses fonctions, et [comme l'année
dernière][womens-march-2017] juste après son inauguration, beaucoup de gens se
sont mis en marche pour montrer qu'ils ne soutiennent toujours pas sa politique.
Voilà quelques photos de la marche qui a eu lieu à Denver samedi dernier (le 20
janvier).

<!--more-->

![Women's march on Denver](images/womens-march-1.jpg)

![Women's march on Denver](images/womens-march-2.jpg)

![Women's march on Denver](images/womens-march-3.jpg)


[womens-march-2017]: {{< relref "2017-01-22-womens-march-on-denver" >}}
