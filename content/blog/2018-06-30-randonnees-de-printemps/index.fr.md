---
title: "Randonnées de printemps"
date: 2018-06-30
lastmod:
authors: [guillaume]
categories: []
tags: [hiking, Colorado]
slug: randonnees-de-printemps
---

Voilà quelques photos de randonnées de ce printemps.

<!--more-->

Fin mai, nous sommes allés plusieurs fois aux alentours de *Geen Mountain*. De
là, on a une belle vue sur le [*continental divide*][continental-divide] :

![Green Mountain](images/green-mountain-1.jpg)

C'est aussi le bon moment de l'année et la bonne altitude pour voir plein de
magnifiques fleurs sauvages :

![Green Mountain](images/green-mountain-2.jpg)

![Green Mountain](images/green-mountain-3.jpg)

![Green Mountain](images/green-mountain-4.jpg)

Et aussi des animaux sauvages au retour :

![Green Mountain](images/green-mountain-5.jpg)

Début juin nous avons fait une rando entre les *Flat Irons*, avec des vues
impressionnantes de ces énormes rochers :

![Flat Irons](images/flat-irons-1.jpg)

![Flat Irons](images/flat-irons-2.jpg)

![Flat Irons](images/flat-irons-3.jpg)

![Flat Irons](images/flat-irons-4.jpg)

![Flat Irons](images/flat-irons-5.jpg)

Le rocher central vu de derrière :

![Flat Irons](images/flat-irons-6.jpg)

Mi-juin nous étions à *Rocky Mountain National Park*, pour une promenade le long
du chemin qui va jusqu'à des chutes appelées *Ouzell Falls*. Comme le dit ce
panneau, l'eau était très froide...

![Ouzell Falls](images/ouzell-falls-1.jpg)

L'eau froide rafraichissait aussi l'air presque tout le long du chemin, ce qui
était bien par une journée très chaude (déjà en début de matinée).

![Ouzell Falls](images/ouzell-falls-2.jpg)

![Ouzell Falls](images/ouzell-falls-3.jpg)

![Ouzell Falls](images/ouzell-falls-4.jpg)

C'est très courant de voir des animaux dans ce parc, mais je les prends quand
même toujours en photo :

![Ouzell Falls](images/ouzell-falls-5.jpg)

![Ouzell Falls](images/ouzell-falls-6.jpg)

À un moment, le chemin donne une vue excellente sur [*Longs Peak*][longs-peak]
(la montagne au sommet plat à gauche), qui vu d'ici semble accessible malgré ses
4 345 mètres :

![Ouzell Falls](images/ouzell-falls-7.jpg)

![Ouzell Falls](images/ouzell-falls-8.jpg)

Les chutes, avec des promeneurs pour l'échelle :

![Ouzell Falls](images/ouzell-falls-9.jpg)

Le chipmunk vu à l'aller nous a aussi fait coucou au retour au même endroit :

![Ouzell Falls](images/ouzell-falls-10.jpg)

Mi-juin à *Eldorado Canyon* :

![Eldorado Canyon](images/eldorado-canyon-1.jpg)

![Eldorado Canyon](images/eldorado-canyon-2.jpg)

Et la dernière randonnée que nous avons fait jusqu'à présent était presque
jusqu'au sommet de *South Boulder Peak* (presque car un orage commençait juste
quand nous avons aperçu le sommet) par *Shadow Canyon* sur un versant différent
de [la dernière fois][rando]. Je n'ai qu'une seule photo, même pas du sommet
mais d'un rocher aperçu sur le chemin :

![South Boulder Peak](images/south-boulder-peak-1.jpg)


[continental-divide]: https://fr.wikipedia.org/wiki/Ligne_continentale_de_partage_des_eaux_d%27Am%C3%A9rique_du_Nord

[longs-peak]: https://fr.wikipedia.org/wiki/Pic_Longs

[rando]: {{< relref "2016-09-24-bear-peak-south-boulder-peak" >}}
