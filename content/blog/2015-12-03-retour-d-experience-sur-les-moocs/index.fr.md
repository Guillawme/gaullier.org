---
title: "Retour d'expérience sur les MOOCs"
date: 2015-12-03
lastmod:
authors: [guillaume]
categories: []
tags: [learning, internet]
slug: retour-d-experience-sur-les-moocs
---


Voilà un bref retour d'expérience sur quelques MOOCs (*massive open online
courses*) que j'ai suivis ces derniers mois. Contrairement à divers types de
contenus pédagogiques en ligne (tutoriels, documentations, etc.) qui sont pour
la plupart uniquement textuels, les MOOCs incorporent souvent des vidéos, et
mettent surtout l'accent sur les interactions entre élèves.

<!--more-->

## Sur le site France Université Numérique

Le tout premier MOOC que j'ai suivi s'est tenu en juin dernier, et s'intitulait
"Bioinformatique : algorithmes et génomes". Il était dispensé par une équipe de
l'INRIA (Institut National de la Recherche en Informatique et Automatique) sur
le site [France Université Numérique (FUN)][fun]. Alors que j'étais pourtant en
pleine rédaction de ma thèse à ce moment-là, j'ai décidé de suivre ce MOOC par
intérêt mais sans vraiment penser le suivre assidûment. Il s'est en fait révélé
très facile à suivre car les notions de biologie abordées (principalement
l'expression des gènes) étaient pour moi bien connues. Du côté de
l'algorithmique, ce cours a pris le parti d'expliquer quelques méthodes de
comparaison de séquences et de recherche dans des séquences, mais avec une
approche uniquement théorique à base de pseudo-code. Si je suis content d'avoir
maintenant une idée d'ensemble de ces méthodes, il faut bien reconnaître que ce
cours n'a eu aucune retombée pratique pour mon travail de recherche. Je le
recommande néanmoins à tout biologiste qui aimerait avoir une introduction douce
à quelques notions théoriques de bioinformatique, pour mieux comprendre les
outils d'analyse de séquences les plus souvent utilisés.

J'ai ensuite suivi, sur ce même site, un cours intitulé "Introduction à la
statistique avec R", dispensé par des professeurs des universités Paris-Sud et
Paris Diderot. Celui-ci a été très bénéfique car il m'a permis de réactiver mes
faibles connaissances en statistique. Je dois avouer que le seul cours dédié
à cette discipline que j'ai suivi en licence ne m'avait pas séduit à l'époque...
et comme pour beaucoup de choses, on s'aperçoit par la suite que c'était en fait
important. Ce cours donne une bonne approche de notions statistiques
fondamentales (toujours utiles pour un chercheur, même si on ne s'en sert pas
soi-même, ne serait-ce que pour bien comprendre ce qu'on peut lire dans les
articles), et de l'utilisation de R en tant qu'outil pour cette discipline.
En revanche ce cours n'a que très peu abordé R en tant que langage
de programmation. Le cours est proposé régulièrement, et je le conseille à toute
personne qui aimerait aborder la statistique en douceur.

En parallèle du cours sur la statistique, j'ai tenté de suivre un cours de
programmation avec Python (dispensé par une équipe de l'INRIA) mais je m'en suis
rapidement désintéressé faute d'applications concrètes dans mon domaine
scientifique. Avant ce cours j'avais essayé d'apprendre Python de façon
autodidacte en tentant de résoudre les défis de bioinformatique proposés par le
site [Rosalind][rosalind], mais j'ai rapidement manqué de temps pour cette
activité à cause de la rédaction de ma thèse.

## Sur le site Coursera

[Coursera][coursera] est un autre site majeur dans l'offre de MOOCs, cette fois
anglophone (mais les vidéos sont souvent sous-titrées dans de nombreuses langues
dont le français). Sur ce site, la série de cours intitulée ["*Data Science
Specialization*"][dss] m'a rapidement intéressé. Pour le moment je n'ai suivi
que les deux premiers cours : "*The Data Scientist's Toolbox*" et "*R
Programming*". Le premier était fait pour guider les gens dans l'installation et
l'utilisation basique des outils, notamment git et GitHub. Pour quelqu'un qui
connait déjà certains de ces outils, répondre à l'ensemble des questions ne
nécessite pas forcément de regarder les vidéos, et peut prendre seulement une
demi-journée (le cours étant prévu pour durer un mois...). J'ai suivi ce cours
en parallèle du deuxième, qui était en revanche beaucoup plus intensif, avec
chaque semaine des exercices de programmation à faire (tous évalués
automatiquement, sauf un évalué par les autres élèves). Je me suis bien amusé
à suivre ce cours, et je suis convaincu que ces quelques compétences pourront me
servir un jour ou l'autre. J'ai aussi découvert avec ce cours que la
programmation pouvait m'amuser, contrairement à l'expérience frustrante qu'ont
été les quelques dizaines d'heures que j'ai passées à jouer avec Python. Il faut
dire que R est spécialement conçu pour l'analyse de données, et en tant que
chercheur c'est ce qui m'intéresse le plus.

J'envisage de suivre quelques-uns des autres cours de cette spécialisation, mais
peut-être pas tous (les trois derniers m'intéressent moins). Cela dépendra de
mon temps libre, car dès que j'aurai trouvé un nouveau labo où travailler je
n'aurai certainement plus le temps de suivre des cours aussi intensifs.

## Pour conclure

D'après une lecture rapide des catalogues des deux sites, il semblerait que FUN
offre beaucoup de cours relativement théoriques ou de culture générale, tandis
que Coursera axe nettement son offre sur des cours permettant de se forger des
compétences pratiques et "monnayables".

Je n'ai pas encore essayé de suivre un MOOC sur le site français
[OpenClassrooms][oc] (anciennement "Le Site du Zéro"). Ayant actuellement le
statut de demandeur d'emploi, j'ai même apparemment la possibilité d'y ouvrir un
compte *premium* gratuitement. Cela dit, leur offre est très axée sur le
développement web et le développement d'applications, et ces domaines ne
m'intéressent vraiment pas...

Au final, je garde une bonne expérience de ces quelques cours.
C'est encourageant de savoir que toute période d'inactivité professionnelle peut
facilement être mise à profit pour apprendre plein de nouvelles choses grâce
à une offre en ligne très abondante et surtout sous un format facile. En effet,
pour peu que l'élève soit suffisamment intéressé par le sujet, le format général
des cours sur ces deux sites permet de s'auto-motiver facilement grâce aux
exercices à rendre régulièrement. Il est aussi bon de savoir que chaque cours
peut potentiellement être suivi plus légèrement, sans forcément faire les
exercices, si l'on souhaite seulement augmenter sa culture dans un domaine sans
engager trop de temps dans cette activité.


[fun]: https://www.france-universite-numerique-mooc.fr

[rosalind]: http://rosalind.info

[coursera]: https://www.coursera.org/

[dss]: https://www.coursera.org/specializations/jhudatascience

[oc]: https://openclassrooms.com
