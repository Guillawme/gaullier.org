---
title: Week-end à Uppsala
author: Guillaume Gaullier
date: '2019-09-01'
slug: week-end-a-uppsala
categories: []
tags:
  - travels
  - sweden
lastmod: ~
authors:
  - guillaume
---


Après nos vacances [en Irlande][irlande] et en France, nous avons passé trois
jours à [Uppsala][uppsala] à la fin du mois d'août. Nous sommes allés là-bas
parce qu'un chercheur que j'ai rencontré à deux conférences l'année dernière m'a
invité à venir présenter mes travaux (il s'agissait en réalité d'un entretien
d'embauche informel, j'en reparlerai ici plus tard).

<!--more-->

C'est une jolie ville, et Rebecca et moi avons tous les deux bien apprécié ce
week-end. Nous avons visité le vieux centre, les jardins botaniques de la ville
et le [musée][musee] [Linné][linne]. Pendant la journée que j'ai passée à
l'université, Rebecca a aussi eu le temps de visiter le
[Gustavianum][gustavianum] et la [cathédrale][cathedrale].

Voilà quelques photos :

![Uppsala](images/uppsala-1.jpg)

![Uppsala](images/uppsala-2.jpg)

![Uppsala](images/uppsala-3.jpg)

![Uppsala](images/uppsala-4.jpg)

![Uppsala](images/uppsala-5.jpg)

![Uppsala](images/uppsala-6.jpg)

![Uppsala](images/uppsala-7.jpg)

Nous sommes aussi allés à [*Gamla Uppsala*][gamla], l'emplacement original de la
ville avant que sa cathédrale ne soit reconstruite à l'emplacement actuel, et
maintenant un village beaucoup plus petit.

![Gamla Uppsala](images/gamla-uppsala-1.jpg)

![Gamla Uppsala](images/gamla-uppsala-2.jpg)


[irlande]: {{< relref "2019-08-31-voyage-en-irlande" >}}

[uppsala]: https://fr.wikipedia.org/wiki/Uppsala

[musee]: http://www.linnaeus.se/en

[linne]: https://fr.wikipedia.org/wiki/Carl_von_Linn%C3%A9

[gustavianum]: https://fr.wikipedia.org/wiki/Gustavianum

[cathedrale]: https://fr.wikipedia.org/wiki/Cath%C3%A9drale_d%27Uppsala

[gamla]: https://fr.wikipedia.org/wiki/Gamla_Uppsala
