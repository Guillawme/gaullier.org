---
title: 'New tool: countparticles'
author: Guillaume Gaullier
date: '2020-11-06'
slug: new-tool-countparticles
categories:
  - Software
tags:
  - work
  - Python
  - learning
  - cryo-em
lastmod: ~
authors:
  - guillaume
---


In my [previous post][angdist-post], I wrote that I would not bother writing a
Python command-line program to simply count particles in each class in a
`run_data.star` file from RELION, because it is straightforward to do [with
AWK][count-particles-awk] (and it probably runs faster on large files). I
changed my mind and made [a new tool][repo] (also [installable with
`pip`][countparticles-pypi] and citeable with
{{< doi "10.5281/zenodo.4139778" >}}).


I still love the AWK solution for many reasons: it is indeed straightforward, it
runs fast even on large files, there was no boilerplate code to write to handle
file input. And most importantly, it consists of only one file: drop it anywhere
in your `$PATH`, make it executable, and it will work on any system that has AWK,
which means everywhere, since [AWK is "mandatory" in the sense of IEEE Std
1003.1-2008][ieee]. The only problem is that star files from RELION change
between versions, in a way that makes the relevant data for this counting not
always stored in the same column. And AWK can only refer to a column by index,
not by name. Changing the [column number in the AWK script][fix] is trivial, but
when the script produces nonsensical output or no output at all while I am
trying to make sense of data, this kind of limitation gets frustrating quickly.

The obvious solution was to write a Python program, because the [`starfile`
library][starfile] produces [pandas][pandas] `DataFrame`s, and these in turn can
refer to a column by its name as defined in the star file. This works regardless
of the column's numerical index, so it doesn't break when a new version of
RELION produces star files in which the relevant column has a new index. The
downside is having to manage [a Python installation][python-mess]... Luckily
`conda` makes this manageable, but it now seems like a way over-engineered
solution to the simple problem of counting lines by groups in a file... so I
also added an option to display a bar graph representing the counts.


[angdist-post]: {{< relref "2020-10-18-new-tool-angdist" >}}

[count-particles-awk]: https://github.com/Guillawme/cryoEM-scripts/blob/master/count_particles.awk

[repo]: https://github.com/Guillawme/countparticles

[countparticles-pypi]: https://pypi.org/project/countparticles

[ieee]: https://en.wikipedia.org/wiki/List_of_Unix_commands

[fix]: https://github.com/Guillawme/cryoEM-scripts/blob/master/count_particles.awk#L20

[starfile]: https://pypi.org/project/starfile

[pandas]: https://en.wikipedia.org/wiki/Pandas_(software)

[python-mess]: https://xkcd.com/1987/
