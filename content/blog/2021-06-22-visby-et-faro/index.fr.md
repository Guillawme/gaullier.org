---
title: Visby et Fårö
author: Guillaume Gaullier
date: '2021-06-22'
slug: visby-et-faro
categories: []
tags:
  - travels
  - sweden
lastmod: ~
authors:
  - guillaume
---


La semaine dernière, nous étions en vacances à [Visby][visby], sur l'île de
[Gotland][gotland]. Nous avons principalement visité la vieille ville et ses
environs.

<!--more-->

Voilà quelques photos de la vieille ville. Quelques vues des remparts :

![Visby vue de l'extérieur des remparts](images/visby-01.jpeg)

![Remparts de Visby](images/visby-03.jpeg)

![Remparts de Visby](images/visby-10.jpeg)

La cathédrale :

![Cathédrale de Visby](images/visby-02.jpeg)

Quelques-unes des nombreuses ruines dans la vieille ville :

![Ruines à Visby](images/visby-04.jpeg)

![Ruines à Visby](images/visby-05.jpeg)

![Ruines à Visby](images/visby-06.jpeg)

![Ruines à Visby](images/visby-09.jpeg)

![Ruines à Visby](images/visby-11.jpeg)

[Galgberget][potence], la potence en dehors de la ville, qui a été utilisée du
Moyen Âge jusqu'en... 1845 :

![Potence en dehors de Visby](images/visby-07.jpeg)

Un ancien fourneau en dehors de la ville :

![Fourneau en dehors de Visby](images/visby-08.jpeg)

Le musée de Gotland a une excellent exposition sur l'histoire de l'île, avec en
particulier ces stèles peintes magnifiques de l'époque Viking :

![Stèle peinte](images/gotland-museum-01.jpeg)

![Stèle peinte](images/gotland-museum-02.jpeg)

Près de Gnisvärd, nous avons vus ces [bateaux de pierres][stoneships], des sites
mégalitiques de l'époque Viking apparemment courants non seulement dans toute
l'île de Gotland mais aussi ailleurs en Scandinavie.

![Bateau de pierres près de Gnisvärd](images/stoneships-01.jpeg)

![Bateau de pierres près de Gnisvärd](images/stoneships-02.jpeg)

Pour notre dernier jour sur l'île, nous sommes allés jusqu'à [Fårö][faro], une
île plus petite tout à l'extrémité nord-est de Gotland. On traverse le détroit
qui sépare les deux îles avec ce bac :

![Bac de Fårösund à Fårö](images/faro-07.jpeg)

Nous avons parcouru une partie de Fårö en vélo, surtout la pointe sud de l'île
où ont été prises ces quelques photos :

![Fårö](images/faro-02.jpeg)

![Fårö](images/faro-04.jpeg)

![Fårö](images/faro-05.jpeg)

![Fårö](images/faro-06.jpeg)

Enfin, la veille de notre retour nous avons eu ce magnifique coucher de soleil
vu depuis Visby :

![Coucher de soleil à Visby](images/visby-12.jpeg)


[visby]: https://fr.wikipedia.org/wiki/Visby

[gotland]: https://fr.wikipedia.org/wiki/Gotland

[potence]: https://en.wikipedia.org/wiki/Galgberget,_Visby

[stoneships]: https://fr.wikipedia.org/wiki/Bateau_de_pierre

[faro]: https://fr.wikipedia.org/wiki/F%C3%A5r%C3%B6
