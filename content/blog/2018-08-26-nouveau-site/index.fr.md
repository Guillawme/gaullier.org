---
title: Nouveau site
author: Guillaume Gaullier
date: '2018-08-26'
slug: nouveau-site
categories: []
tags:
  - internet
lastmod: '2023-02-05'
authors:
  - guillaume
---


Je viens de terminer la migration de mon petit site vers un nouveau générateur
et un nouveau thème. Voici les changements visibles pour les visiteurs :

<!--more-->

- le thème ;
- l'adresse, qui est maintenant <https://gaullier.org> ou
  <https://www.gaullier.org> ;
- la catégorisation des articles de blogs (en bas d'un article, il suffit de 
  cliquer sur une catégorie ou une étiquette pour lister tous les articles sur
  le même sujet) ;
- le flux RSS distribue maintenant les articles en entier (et non plus seulement
  le premier paragraphe), et chaque *tag* et catégorie a son propre flux ;
- les pages ("à propos", etc.) sont maintenant disponibles en français ;
- les deux versions du site, anglaise et française, sont maintenant bien mieux
  séparées (sur l'ancien site, tout était mélangé).

Mes motivations pour cette mise à jour sont diverses. Un avantage de cette
migration est d'utiliser désormais le générateur de site [Hugo][hugo], qui a de
nombreux avantages sur celui que j'utilisais jusqu'alors (appelé
[Jekyll][jekyll]) : changer de thème est beaucoup plus facile, traduire le 
contenu du site est beaucoup plus facile, la pagination est automatique (avec
Jekyll, il avait fallu bidouiller pour que ça fonctionne), la classification
par *tags* et catégories est automatique, etc. Utiliser Hugo me permet aussi
d'utiliser [blogdown][blogdown], un autre générateur qui délègue la plupart des
tâches à Hugo mais permet quelques bricoles en plus. Je m'en servirai surtout
pour publier sur la version anglaise du blog.

Une autre motivation était d'utiliser le nom `gaullier.org`, que j'utilise déjà
pour mon adresse email depuis presque deux ans. J'aurais pu simplement faire
pointer le nom sur l'ancien site, mais c'était aussi l'occasion de tout
déménager chez [Framasoft][framasoft], une association française. La motivation
principale pour ça était que l'ancien hébergeur de ce site
[s'est fait racheter][rachat] par un géant de l'internet qui traite les données
de ses utilisateurs comme de la matière première à valoriser : je préfère donc
faire un don périodique à Framasoft et héberger ce site chez eux.

**Mise à jour du 5 février 2023 :** en [avril 2020][moved-to-gitlab], j'ai à
nouveau déménagé le site, cette fois vers [GitLab][gitlab]. Je l'avais alors
indiqué sur la page [À propos][a-propos], mais avais oublié de le signaler de
façon plus visible sur le blog (oups). La raison de ce nouveau déplacement était
que [Framasoft allait arrêter ou limiter certains de ses services][deframa], et
à l'époque de cette annonce [Framagit][framagit] était concerné. J'avais donc
anticipé cela et déplacé le site. Finalement, Framagit est resté ouvert
(Framasoft a révisé ce plan de fermeture pour soutenir des usages changeants
pendant la pandémie). J'ai tout de même laisé le site sur GitLab, car leur
instance offre plus d'espace de stockage (j'ai tout de même cherché d'autres
instances, mais toutes celles que j'ai trouvées en France offraient toutes moins
d'espace de stockage).

L'ancien site est toujours en ligne, mais je ferai bientôt en sorte que toutes
ses pages redirigent vers les pages correspondantes sur ce nouveau site.


[hugo]: https://gohugo.io/

[jekyll]: https://jekyllrb.com/

[blogdown]: https://bookdown.org/yihui/blogdown/

[framasoft]: https://framasoft.org/

[rachat]: https://blog.github.com/2018-06-04-github-microsoft/

[moved-to-gitlab]: https://gitlab.com/Guillawme/gaullier.org/-/commit/fe21f700ce1245d7cabac478b8819599ed0bf2a7

[gitlab]: https://about.gitlab.com/

[a-propos]: {{< relref path="/page/about" lang="fr" >}}

[deframa]: https://framablog.org/2019/09/24/deframasoftisons-internet/

[framagit]: https://framagit.org
