---
title: "Voyage en bateau"
date: 2017-07-29
lastmod:
authors: [guillaume]
categories: []
tags: [travels, Washington, Oregon]
slug: voyage-en-bateau
---


La première semaine de juillet, nous sommes allés à Seattle voir le petit frère
de Rebecca. Il vit sur un bateau, et nous avait proposé un petit voyage de
quelques jours pendant l'été. Voilà quelques photos prises pendant le voyage.

<!--more-->

Voilà pour commencer une carte qui montre notre trajet et nos escales, pour
situer un peu tout ça :

<div class="embedded-content">
<iframe frameBorder="0" src="https://framacarte.org/fr/map/seattle-ete-2017_11733?scaleControl=true&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=false&onLoadPanel=undefined&captionBar=false&datalayers=18772&fullscreenControl=true&editinosmControl=false#8/48.201/-122.075"> </iframe>
</div>

Voilà quelques photos prises à Seattle, en sortant du canal :

![Seattle](images/seattle-1.jpg)

![Seattle](images/seattle-2.jpg)

![Seattle](images/seattle-3.jpg)

![Seattle](images/seattle-4.jpg)

Nous voilà maintenant dans le [*Puget Sound*][puget-sound]. On voit la côte
partout autour, un peu comme dans le golfe du Morbihan, mais en beaucoup plus
grand.

![Puget Sound](images/puget-sound-1.jpg)

![Puget Sound](images/puget-sound-2.jpg)

![Puget Sound](images/puget-sound-3.jpg)

L'oncle et la tante de Rebecca nous ont accompagnés pendant une partie du
voyage, on aperçoit leur bateau sur cette photo :

![Puget Sound](images/puget-sound-4.jpg)

Après quelques heures de navigation très calme depuis Seattle, nous arrivons à
Port Townsend, notre première escale :

![Port Townsend](images/port-townsend-1.jpg)

![Port Townsend](images/port-townsend-2.jpg)

![Port Townsend](images/port-townsend-3.jpg)

C'est une petite ville magnifique, j'ai pris beaucoup de photos. Je ne peux pas
tout mettre en ligne, mais voilà quelques photos qui montrent bien les alentours
de la ville :

![Port Townsend](images/port-townsend-4.jpg)

![Port Townsend](images/port-townsend-5.jpg)

![Port Townsend](images/port-townsend-6.jpg)

![Port Townsend](images/port-townsend-7.jpg)

J'ai adoré ces paysages de littoral avec des montagnes très hautes en arrière
plan :

![Port Townsend](images/port-townsend-8.jpg)

![Port Townsend](images/port-townsend-9.jpg)

![Port Townsend](images/port-townsend-10.jpg)

Le lendemain de notre arrivée à Port Townsend, nous avons marché jusqu'à [Fort
Worden][fort-worden], un ancien fort maintenant reconverti en *state park*. Sur
cette première photo, on aperçoit la jetée du fort, et [Mount
Baker][mount-baker] en arrière plan :

![Fort Worden](images/fort-worden-1.jpg)

![Fort Worden](images/fort-worden-2.jpg)

Nous sommes allés à cet endroit pour assister à un concert du [festival Fiddle
Tunes][fiddle-tunes], qui a lieu chaque année à cet endroit pendant la première
semaine de juillet.

![Fort Worden](images/fort-worden-3.jpg)

C'était le 4 juillet, la fête nationale, et une grande fête était organisée dans
le parc. Il y avait un groupe de jazz (des soldats), un rally de voitures
anciennes :

![Fort Worden](images/fort-worden-4.jpg)

Il y avait aussi quelques *food trucks*, dont celui-ci qui a particulièrement
attiré mon attention :

![Fort Worden](images/fort-worden-5.jpg)

Je suis allé discuter avec ces gens, et c'étaient de vrais bretons qui vivent
aux États-Unis depuis longtemps ! J'ai appris qu'ils ne viennent pas exactement
de Quimper (mais de quelque part dans le Morbihan, je ne me souviens plus
exactement où), mais qu'ils ont nommé leur crêperie d'après la [péninsule de
Quimper][quimper-peninsula], pour donner un nom familier aux habitants de la
région tout en donnant une référence bretonne impossible à rater pour n'importe
quel touriste français. J'ai donc appris que la péninsule de Quimper, où est
située Port Townsend, a été nommée par [Manuel Quimper][manuel-quimper] ([page
wikipedia en français][manuel-quimper-fr], beaucoup moins fournie), un
explorateur espagnol né d'une mère espagnole et d'un père... français ! Je n'ai
pas fait assez de recherches pour déterminer si son père était breton, je vous
laisse donc le loisir de rechercher cette information.

Nous sommes rentrés à Port Townsend par la plage :

![Fort Worden](images/fort-worden-6.jpg)

Le jour suivant, nous avons continué le voyage vers le nord et avons fait une
courte escale en face de [Lopez Island][lopez-island] :

![Lopez Island](images/lopez-island-1.jpg)

![Lopez Island](images/lopez-island-2.jpg)

![Lopez Island](images/lopez-island-3.jpg)

Nous sommes arrivés en fin d'après-midi à Anacortes, notre seconde escale.
Je n'ai qu'une seule photo prise dans le port (et floue...) :

![Anacortes](images/anacortes.jpg)

Nous avons dîné avec la grand-mère paternelle de Rebecca, qui habite à
Bellingham et a fait le trajet jusqu'à Anacortes pour nous voir. Le lendemain,
nous sommes repartis pour Seattle. Voilà quelques photos prises en chemin.

Un [pygargue à tête blanche][bald-eagle] (en anglais *bald eagle*), très flou...
on dirait presque une aquarelle :

![Retour](images/retour-1.jpg)

L'entrée d'un canal qui n'a pas l'air d'avoir de nom (j'ai bien cherché sur la
carte) :

![Retour](images/retour-2.jpg)

La petite ville de La Conner :

![Retour](images/retour-3.jpg)

La sortie du canal :

![Retour](images/retour-4.jpg)

Je n'ai pas d'autres photos jusqu'à l'arrivée à Seattle.

Après le voyage en bateau, nous sommes allés en Oregon pour deux jours, pour
rendre visite au grand-père maternel de Rebecca à [Dayton][dayton]. Il faisait
très chaud, un temps parfait pour une promenade à l'ombre des noisetiers :

![Dayton](images/dayton-1.jpg)

![Dayton](images/dayton-2.jpg)

Nous sommes finalement repartis le jour suivant depuis l'aéroport de Portland,
toujours avec un temps magnifique idéal pour observer la géographie locale.

Sur cette photo, les quatre volcans sont [Mount Hood][hood] au premier plan,
[Mount St Helens][st-helens] à gauche, [Mount Adams][adams] à droite, et [Mount
Rainier][rainier] au loin en arrière plan :

![Avion](images/avion-1.jpg)

Et voilà une vue de l'est de l'Oregon, presque désertique comparé à la
[Willamette Valley][willamette-valley] :

![Avion](images/avion-2.jpg)

C'était un superbe voyage !


[puget-sound]: https://fr.wikipedia.org/wiki/Puget_Sound

[fort-worden]: https://en.wikipedia.org/wiki/Fort_Worden

[mount-baker]: https://fr.wikipedia.org/wiki/Mont_Baker

[fiddle-tunes]: http://centrum.org/festival-of-american-fiddle-tunes-performances

[quimper-peninsula]: https://en.wikipedia.org/wiki/Quimper_Peninsula

[manuel-quimper]: https://en.wikipedia.org/wiki/Manuel_Quimper

[manuel-quimper-fr]: https://fr.wikipedia.org/wiki/Manuel_Quimper

[lopez-island]: https://en.wikipedia.org/wiki/Lopez_Island

[bald-eagle]: https://fr.wikipedia.org/wiki/Pygargue_%C3%A0_t%C3%AAte_blanche

[dayton]: https://goo.gl/maps/GhkjUL6AqjJ2

[willamette-valley]: https://en.wikipedia.org/wiki/Willamette_Valley

[hood]: https://en.wikipedia.org/wiki/Mount_Hood

[st-helens]: https://en.wikipedia.org/wiki/Mount_St._Helens

[adams]: https://en.wikipedia.org/wiki/Mount_Adams_(Washington)

[rainier]: https://en.wikipedia.org/wiki/Mount_Rainier
