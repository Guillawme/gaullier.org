---
title: Mälaren
author: Guillaume Gaullier
date: '2021-04-14'
slug: malaren
categories: []
tags:
  - sweden
  - hiking
lastmod: ~
authors:
  - guillaume
---


Nous avons passé le week-end de Pâques dans une petite maison à
[Stallarholmen][stallarholmen], au bord du lac [Mälaren][lac]. Voilà quelques
photos des environs.

<!--more-->

Le premier jour, nous avons marché jusqu'au [château de Mälsåker][malsaker].

![Église dans la campagne entre Stallarholmen et Mälsåker](images/j1-malsaker-01.jpeg)

Le château :

![Mälsåker](images/j1-malsaker-02.jpeg)

![Mälsåker](images/j1-malsaker-03.jpeg)

Le deuxième jour, nous avons visité la ville de [Strängnäs][strangnas] :

![Strängnäs](images/j2-strangnas-01.jpeg)

![Strängnäs](images/j2-strangnas-02.jpeg)

![Strängnäs](images/j2-strangnas-03.jpeg)

Le dernier jour, nous avons visité la ville de [Mariefred][mariefred], où nous
avons vu le château de [Gripsholm][gripsholm] :

![Runes](images/j3-mariefred-01.jpeg)

![Runes](images/j3-mariefred-02.jpeg)

![Gripsholm](images/j3-mariefred-03.jpeg)

![Gripsholm](images/j3-mariefred-04.jpeg)

![Gripsholm](images/j3-mariefred-05.jpeg)

![Gripsholm](images/j3-mariefred-06.jpeg)

![Gripsholm](images/j3-mariefred-07.jpeg)

![Gare de Mariefred](images/j3-mariefred-08.jpeg)

![Église de Mariefred](images/j3-mariefred-09.jpeg)

Pour finir, voilà une vue d'un coucher de soleil sur le lac, depuis la maison où
nous avons logé :

![Coucher de soleil sur le lac Mälaren](images/lac.jpeg)


[stallarholmen]: https://goo.gl/maps/B5xpz1fmUUn34zpe7

[lac]: https://en.wikipedia.org/wiki/M%C3%A4laren

[malsaker]: https://en.wikipedia.org/wiki/M%C3%A4ls%C3%A5ker_Castle

[strangnas]: https://goo.gl/maps/EraRsBgVj38prsft6

[mariefred]: https://goo.gl/maps/8YQv7Td8HuT9TooS6

[gripsholm]: https://en.wikipedia.org/wiki/Gripsholm_Castle
