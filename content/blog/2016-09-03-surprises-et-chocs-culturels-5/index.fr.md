---
title: "Surprises et chocs culturels - 5"
date: 2016-09-03
lastmod:
authors: [guillaume]
categories: []
tags: [relocation]
slug: surprises-et-chocs-culturels-5
---


Aux États-Unis, le premier lundi de septembre est férié : c'est
[*Labor Day*][labor-day], autrement dit la fête du travail. 

<!--more-->

Elle n'a pas lieu le premier mai comme en Europe pour des raisons politiques
(détaillées dans l'article Wikipedia). J'ai donc un week-end de trois jours, je
me sens comme en vacances. :-)

Je trouve intelligent de planifier les jours fériés le premier lundi du mois, ou
le dernier lundi du mois (comme le [*Memorial Day*][memorial-day]), ou tout
autre jour de la semaine fixé. Cela garantit que le jour en question est
effectivement férié chaque année, alors qu'en Europe par exemple, le premier mai
tombe bien trop souvent pendant un week-end...


[labor-day]: https://en.wikipedia.org/wiki/Labor_Day

[memorial-day]: https://en.wikipedia.org/wiki/Memorial_Day
