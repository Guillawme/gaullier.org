---
title: Couleurs d'automne
author: Guillaume Gaullier
date: '2019-10-12'
slug: couleurs-d-automne
categories: []
tags:
  - Colorado
  - hiking
lastmod: ~
authors:
  - guillaume
---


Ces deux derniers week-ends, nous sommes allés randonner pour voir les
[peupliers faux-tremble][peuplier] avec leurs couleurs d'automne. J'ai déjà [mis
en ligne][peupliers-1] plusieurs [fois][peupliers-2] des  [photos
similaires][peupliers-3], mais je ne m'en lasserai jamais tant ces couleurs sont
splendides.

<!--more-->

Il y a deux semaines nous sommes allés au
[*Golden Gate Canyon State Park*][golden-park], pas très loin au sud de Boulder.
Les paysages sont magnifiques :

![Golden Gate State Park](images/golden-gate-state-park-1.jpg)

![Golden Gate State Park](images/golden-gate-state-park-2.jpg)

![Golden Gate State Park](images/golden-gate-state-park-3.jpg)

![Golden Gate State Park](images/golden-gate-state-park-4.jpg)

Le week-end dernier, nous sommes allés au *Mud Lake Open Space*, à côté de
Nederland, où il y avait encore plus de peupliers partout :

![Mud Lake Open Space](images/mud-lake-open-space-1.jpg)

![Mud Lake Open Space](images/mud-lake-open-space-2.jpg)

![Mud Lake Open Space](images/mud-lake-open-space-3.jpg)

![Mud Lake Open Space](images/mud-lake-open-space-4.jpg)

![Mud Lake Open Space](images/mud-lake-open-space-5.jpg)


[peuplier]: https://fr.wikipedia.org/wiki/Peuplier_faux-tremble

[peupliers-1]: {{< relref "2016-10-01-c-est-l-automne" >}}

[peupliers-2]: {{< relref "2018-09-18-retraite-2018" >}}

[peupliers-3]: {{< relref "2018-09-25-spanish-peaks-festival" >}}

[golden-park]: https://en.wikipedia.org/wiki/Golden_Gate_Canyon_State_Park
