---
title: "Première semaine : au labo et en dehors"
date: 2016-04-10
lastmod:
authors: [guillaume]
categories: []
tags: [work, Colorado, music]
slug: premiere-semaine-au-labo-et-en-dehors
---


Beaucoup de choses se sont passées en une semaine depuis le dernier article, et
je n'ai pas eu le temps d'écrire plus tôt... Voilà un condensé de ma première
semaine au labo, et aussi en dehors (je ne passe pas encore toute ma vie au
labo).

<!--more-->

## Au labo

Pour commencer voilà des photos du bâtiment tout neuf (construit en 2012) dans
lequel se trouve mon labo :

![Jennie Smoly Caruthers Biotechnology Building](images/jscbb-1.jpg)

![Jennie Smoly Caruthers Biotechnology Building](images/jscbb-2.jpg)

Cette première semaine a été un marathon de paperasses et d'échanges avec une
bureaucratie très dense... Lundi, mon premier jour au labo, j'ai eu une
présentation de mon employeur donnée par leur branche "locale" (tout l'Ouest des
États-Unis) située en Californie. C'était donc par vidéo-conférence.
Évidemment c'est ce matin-là qu'il y a eu des coupures de réseau dans le
bâtiment... C'était donc très embarrassant car la personne en Californie a lu
quasiment tout son diaporama dans le vide, probablement sans même s'en
apercevoir, pendant que de notre côté nous étions en panique pour tenter de
rétablir la liaison. Heureusement j'ai pu récupérer une copie des diapos et donc
prendre connaissance des infos qui m'étaient destinées (ça parlait
principalement des différentes assurances santé proposées par l'employeur, d'un
ennui à mourir). Il fallait aussi renseigner plein d'informations dans des
formulaires en ligne, sur leur site assez difficile à utiliser.

Mardi était plus productif : j'ai assisté à une matinée d'orientation
à l'*International Students and Scholars Service*. On nous a expliqué plein de
choses importantes pour maintenir notre statut *Exchange Visitor* et donc notre
autorisation à rester sur le territoire, et aussi comment être autorisé
à rentrer sur le territoire après une sortie temporaire. C'est important sinon
on se retrouve coincé à la douane à l'aéroport... Enfin, cette orientation était
indispensable pour recevoir l'autorisation de demander un *Social Security
Number* (SSN). En plus de cette autorisation, il fallait attendre plus d'une
heure dans un bureau (là je ne me sentais pas trop dépaysé). En France ce numéro
ne sert qu'à l'assurance maladie (enfin je crois, on ne me l'a jamais demandé
pour autre chose), tandis qu'ici ils s'en servent pour tout : l'employeur le
demande, les impôts le demandent, certaines banques le demandent pour pouvoir
ouvrir un compte. C'était le cas de [*Elevations Credit Union*][elevations], où
je voulais ouvrir mon compte (je ne sais pas si l'équivalent des
[*credit unions*][credit-unions] existe en France : ce sont des sortes de
coopératives détenues par leurs utilisateurs, contrairement aux banques
classiques). Ma demande de SSN a été acceptée et j'ai pu ouvrir un compte, étape
importante vers l'autonomie. J'ai aussi définitivement sécurisé l'appartement
dans lequel je vais bientôt emménager (il fallait payer le premier mois).

Le reste de la semaine, j'ai principalement suivi des *safety trainings*
obligatoires pour travailler dans le labo (celui sur les animaux est une pure
perte de temps puisque mes travaux sont uniquement *in vitro*, mais bon c'est
obligatoire...). Il s'agit de lire une cinquantaine de diapos pas très
passionnantes, puis de répondre à un quizz pour vérifier qu'on a bien lu
les diapos.

Mercredi 13 le labo m'emmène en congrès sur la côte Est jusqu'à samedi, donc le
vrai travail à la paillasse ne commencera que la semaine suivante. J'ai donc
lundi et mardi pour terminer les *safety trainings* et commencer un peu de
recherches bibliographiques, puis le congrès pour vraiment me mettre dans le
sujet. Le congrès se tient dans un endroit qui a l'air magnifique, j'en parlerai
dans un prochain article.

## Et en dehors du labo

Je voulais aller à des événements de la *Conference on World Affairs*, il
y avait notamment en soirée des concerts et conférences intéressantes.
Finalement je n'ai pas eu le courage... Mais il se passe tellement de choses ici
que j'aurai bien d'autres occasions de sorties.

Mercredi soir Marta m'a emmené à un
[*meetup* du groupe *Analyze Boulder*][analyze-boulder] : un groupe de personnes
qui utilisent des données publiques pour toutes sortes d'analyses. La première
présentation expliquait comment concevoir des bâtiments plus performants (en
termes de consommation électrique, etc.) en se basant non pas sur des modèles
théoriques mais sur de nombreuses mesures faites sur des bâtiments déjà
existants. La deuxième présentation montrait comment le service de partage de
voitures de Denver fonctionnait : dans quels quartiers les trajets sont les plus
fréquents, quels jours de la semaine, à quelles heures, etc. La dernière
présentation était pour moi la plus intéressante : il s'agissait d'une [analyse
du texte des lois votées par le parlement des États-Unis depuis le gouvernement
de Clinton jusqu'à aujourd'hui][congress-analysis]. Entre autres choses,
l'auteur de l'étude a recherché systématiquement certains mots dans les textes,
et tracé les courbes de fréquence de ces mots en fonction du temps. Un de ses
graphiques est très très parlant : il semble montrer une certaine stabilité sous
Clinton, un chaos total sous Bush, et un lent retour à la stabilité sous Obama.
Voilà le graphique en question :

![Topics by year](images/TopicsbyYear_3.png)

Jeudi soir Marta & Jeff m'ont emmené dîner dans un restaurant pas loin de chez
eux. Ce restaurant brasse ses propres bières, ils en proposent de nombreuses
différentes, parfois seulement disponibles pour une saison. J'en ai goûté une
brune très sombre, avec un léger goût de basilic qui allait très bien avec
ma pizza. :-)

Vendredi soir Marta & Jeff m'ont emmené écouter un concert de bluegrass dans une
petite ville appelée Lyons, située au Nord de Boulder. Lyons est un coin
vraiment sympa, dans la campagne. Le concert se tenait dans une salle située
dans un grand espace au pieds de falaises et près d'une petite rivière.
Apparemment un festival de musique a lieu dans cet endroit chaque été en
juillet. Deux groupes ont joué : le
[*Springfever Bluegrass Band*][springfever-band] et
[*Frank Solivan & Dirty Kitchen*][dirty-kitchen]. Le deuxième groupe était
excellent : tous les musiciens étaient de vrais virtuoses. Mais leur musique m'a
moins touché que celle du premier groupe, c'était très rapide et démonstratif,
avec moins de nuances. Le premier groupe m'a énormément plu, ils ont chanté de
jolies chansons avec plein d'harmonies. C'était globalement une super soirée !

Samedi matin Marta m'a montré plein de magasins de seconde main où je vais
pouvoir trouver tout ce qui manque pour l'appartement (c'est un meublé, je dois
juste trouver des couverts et autres petits équipements). J'ai presque acheté un
vélo dans un magasin d'articles de sport d'occasion, malheureusement il était
déjà réservé quand je l'ai vu. Les vendeurs du magasin m'ont dit qu'au printemps
ils reçoivent beaucoup de vélos et que leur stock tourne rapidement (à la fin de
l'année universitaire beaucoup d'étudiants repartent chez eux, potentiellement
à l'autre bout du pays, donc ils vendent beaucoup de leurs affaires).
J'y retournerai la semaine prochaine, ou je chercherai un vélo sur Craig's List
(l'équivalent local du Bon Coin).

Samedi après-midi je suis allé me balader dans les chemins de randonnée les plus
proches de là où habitent Marta & Jeff, à environ 15 min à pieds. On arrive
rapidement dans la montagne, au calme complet, c'est super relaxant. J'ai suivi
un parcours d'environ 2h30 (aucune idée de la distance en km) qui montait assez
haut. Les paysages sont magnifiques. Voilà quelques photos (de qualité très
moyenne, il faudra que je trouve un véritable appareil photo car mon téléphone
n'est pas terrible) :

![Rando en montagne](images/rando-1.jpg)

![Rando en montagne](images/rando-2.jpg)

![Rando en montagne](images/rando-3.jpg)

![Rando en montagne](images/rando-4.jpg)

Et finalement aujourd'hui dimanche... j'ai passé toute la matinée à trier des
photos et à écrire cet article. Il fait un peu gris aujourd'hui donc je n'ai pas
perdu mon temps en restant à l'intérieur. Je vais essayer de reprendre un peu la
musique, ça fait trop longtemps que je n'ai pas joué.


[elevations]: https://www.elevationscu.com

[credit-unions]: https://en.wikipedia.org/wiki/Credit_union

[analyze-boulder]: http://www.meetup.com/fr-FR/Analyze-Boulder/events/228611534

[congress-analysis]: https://github.com/scsherm/Congress_work

[springfever-band]: http://www.thebluegrassgirl.com/#/springfever-bluegrass-band

[dirty-kitchen]: http://dirtykitchenband.com
