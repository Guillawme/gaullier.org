---
title: "Janelia Research Campus"
date: 2017-12-14
lastmod:
authors: [guillaume]
categories: []
tags: [travels, work]
slug: janelia-research-campus
---


Lundi et mardi derniers (11 et 12 décembre), j'étais à [*Janelia Research
Campus*][janelia] pour le travail. Je n'ai pas eu le temps de prendre des
photos, cela dit ce centre de recherche se trouve à Ashburn, Virginia :
autrement dit, [au milieu de nulle part...][ashburn] Mais je comptais quand même
écrire quelques mots pour raconter ce court voyage.

<!--more-->

C'est au milieu de nulle part, dans une campagne assez vide et inintéressante,
mais le centre de recherche lui-même est un endroit fantastique où travailler :
super équipé pour plusieurs techniques de biochimie et biologie structurale, et
avec les locaux sont très récents.

Janelia est un centre de recherche entièrement financé par le [*Howard Hughes
Medical Institute*][hhmi] (HHMI, qui se trouve être aussi mon employeur),
spécialisé en neurosciences. Une particularité intéressante de ce centre, c'est
que les gens qui y travaillent n'ont ni à chercher leurs propres financements,
ni à enseigner, contrairement à ce qui se pratique couramment dans les
universités. Cela fait partie de la stratégie de recherche de HHMI, qu'ils
résument par *"funding people, not projects"* (nous finançons des personnes
plutôt que des projets) : ils investissent dans des chercheurs sans évaluer une
proposition de projet (mais en évaluant quand même les accomplissements
précédents des chercheurs), ce qui favorise la créativité en laissant une
liberté assez large aux chercheurs pour entreprendre des recherches risquées (au
sens où les résultats sont difficiles à anticiper).

Mes recherches n'ont rien à voir avec les neurosciences, si j'y suis allé c'est
parce que Janelia dispose d'un laboratoire de microscopie électronique équipé de
deux microscopes et détecteurs de dernière génération (pour les curieux, le
microscope est un [FEI Titan Krios][krios], et le détecteur un [Gatan K2 Summit
direct electron detector][k2]). La microscopie électronique a fait des progrès
technologiques énormes ces dernières années, notamment avec des meilleurs
détecteurs et des meilleures techniques d'analyse d'image. Ces progrès ont été
remarqués avec [l'attribution cette année du prix Nobel de chimie][nobel-cryoem]
à trois chercheurs ayant contribué aux premiers développements de cette
technique appliquée aux molécules biologiques. Tout cela permet désormais à des
chercheurs comme moi qui étudient de tout petits objets d'employer cette
méthode. Comme ma chef est financée par HHMI, notre labo a accès à ces
équipements gratuitement ; il faut seulement payer pour envoyer les
échantillons, mais le temps d'utilisation nous est offert.

Ma présence sur place était utile lundi pour choisir les zones de l'échantillon
où collecter des images, ensuite la collecte s'est déroulée automatiquement
jusqu'à ce matin (bien après mon retour). Je devrais bientôt recevoir par la
poste le disque dur contenant les images (il y a bien trop d'images, et chacune
est bien trop volumineuse, pour pouvoir les transférer par internet en un temp
raisonnable), et à partir de ce moment l'analyse des images me prendra au moins
deux semaines, probablement plus.


[janelia]: https://www.janelia.org

[ashburn]: https://goo.gl/maps/VfqB7hmWMdp

[hhmi]: http://www.hhmi.org

[krios]: https://www.fei.com/products/tem/krios

[k2]: http://www.gatan.com/products/tem-imaging-spectroscopy/k2-direct-detection-cameras

[nobel-cryoem]: https://www.nobelprize.org/nobel_prizes/chemistry/laureates/2017
