---
title: "Incendie de forêt à Nederland"
date: 2016-07-10
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: incendie-de-foret-a-nederland
---


Voilà un rapide article pour donner des nouvelles, suite à l'incendie de forêt
qui fait toujours rage dans le *Boulder County* à l'heure où j'écris ces lignes.

<!--more-->

Hier soir, samedi 9 juillet, une odeur de fumée a commencé à se répandre
à Boulder, jusqu'à un niveau suffisamment inquiétant pour que je consulte les
actualités. Le site du *Denver Post* (un journal local) a maintenant plusieurs
articles sur le sujet, qui permettent de retracer à peu près toute l'histoire.
En bref, deux campeurs ont apparemment laissé derrière eux un feu sans s'assurer
qu'il était bien complètement éteint.

- [July 9, 2016: Boulder County wildfire prompts hundreds of evacuations near Nederland][denpost-1]
- [July 10, 2016: Cold Springs fire in Boulder County destroys 3 homes, grows to more than 600 acres as evacuations increase][denpost-2]
- [July 10, 2016: Cold Springs fire evacuees say packing up their lives to flee has been "surreal" ][denpost-3]
- [July 10, 2016: Photos of the Cold Springs Fire near Nederland, Colorado][denpost-4]
- [July 10, 2016: Two arrested for starting Cold Springs fire in Boulder County; more homes threatened][denpost-5]

Aujourd'hui l'odeur de fumée s'était dissipée, avant de revenir en soirée.
Dans la journée, voilà ce que nous pouvions apercevoir **depuis notre immeuble**
(assez inquiétant !) :

![Incendie de forêt](images/incendie-foret.jpg)

Cette colonne de fumée a fini par disparaitre, ce qui je l'espère signifie que
l'incendie est maitrisé.

Les incendies de forêt sont apparemment assez fréquents ici à cause du climat
semi-aride (donc très chaud et sec l'été). La surveillance et la communication
sur les incendies est donc très efficace, comme par exemple avec
[cette carte actualisée en quasi temps réel][carte]. Twitter fournit aussi
beaucoup d'informations en temps réel, par exemple en suivant le compte
[@COWildfireInfo][cowildfireinfo], ou les mots-clés
[#ColdSpringsFire][coldspringsfire] et [#NederlandFire][nederlandfire] /
[#NedFire][nedfire] (les deux incendies les plus proches, dans le *Boulder
County* ; d'autres incendies dans d'autres contés de l'état sont documentés de
la même façon, par exemple en suivant [#HaydenPassFire][haydenpassfire] et
[#HogbackFire][hogbackfire]).

Pour le moment notre quartier n'a pas besoin d'être évacué. J'espère que ça ne
sera pas nécessaire...


[denpost-1]: http://www.denverpost.com/2016/07/09/boulder-wildfire-prompts-evacuations-in-nederland

[denpost-2]: http://www.denverpost.com/2016/07/10/more-boulder-county-residents-in-path-of-cold-spring-fire

[denpost-3]: http://www.denverpost.com/2016/07/10/cold-springs-fire-evacuees

[denpost-4]: http://www.denverpost.com/2016/07/10/cold-springs-fire-near-nederland-colorado

[denpost-5]: http://www.denverpost.com/2016/07/10/two-arrested-for-starting-cold-springs-fire-more-homes-threatened-as-fire-grows

[coldspringsfire]: https://twitter.com/hashtag/ColdSpringsFire

[haydenpassfire]: https://twitter.com/hashtag/HaydenPassFire

[hogbackfire]: https://twitter.com/hashtag/HogbackFire

[nedfire]: https://twitter.com/hashtag/NedFire

[cowildfireinfo]: https://twitter.com/COWildfireInfo

[carte]: http://boulder.maps.arcgis.com/apps/PublicInformation/index.html?appid=cd569c1a616f4b258681716bbe5418f0

[nederlandfire]: https://twitter.com/hashtag/NederlandFire
