---
title: "Colorado Shakespeare Festival"
date: 2018-07-22T16:21:18-06:00
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, theater]
slug: colorado-shakespeare-festival
---


Chaque année pendant l'été, l'université accueille le [*Colorado Shakespeare
Festival*][csf]. Chaque été également, un couple de retraités new yorkais vient
passer ses vacances à Boulder et cette année ils sont dans l'appartement juste à
côté du nôtre. Ils sont des habitués du festival, et nous ont emmené voir trois
des pièces pour leur toute dernière répétition avec les costumes.

<!--more-->

C'est donc très semblable à la vraie représentation, même si le metteur en scène
explique au début qu'il peut y avoir des pauses pour corriger certaines choses.
Dans les faits, deux des pièces ont été jouées parfaitement, et la troisième a
simplement commencé une heure en retard à cause de problèmes avec le système de
sonorisation.

La première pièce que nous avons vue était [Richard III][richard-3] de
Shakespeare. Je n'ai compris qu'environ 20 % du texte car il me manquait
beaucoup de vocabulaire, mais j'ai énormément apprécié le jeu des acteurs et la
mise en scène qui ont réussi à transmettre des éléments comiques (alors que
cette pièce est une tragédie).

Nous avons ensuite vu [Cyrano de Bergerac][cyrano], excellement bien traduit en
anglais (je n'arrive pas à retrouver par qui) pour rester en alexandrins et
rimes, et tout aussi bien interprété et mis en scène. Cette fois je n'ai eu
aucun problème pour comprendre l'anglais, et je connaissais déjà la pièce.

Cette semaine, nous avons vu une pièce américaine de 1936 intitulée [*You can't
Take It With You*][cant-take] (de Moss Hart & George S. Kaufman). Là encore, pas
de problème de compréhension car ce n'était même pas en vers. La pièce raconte
le choc culturel quand les deux familles d'un couple fiancé se rencontrent.

Nous avons raté la répétition de la première pièce du festival, [*Love's
Labour's Lost*][lll], mais puisqu'elle est encore jouée jusqu'en août nous irons
sûrement voir une des représentations.


[csf]: https://cupresents.org/series/shakespeare-festival

[richard-3]: https://fr.wikipedia.org/wiki/Richard_III_(Shakespeare)

[cyrano]: https://fr.wikipedia.org/wiki/Cyrano_de_Bergerac_(Rostand)

[cant-take]: https://en.wikipedia.org/wiki/You_Can%27t_Take_It_with_You_(play)

[lll]: https://fr.wikipedia.org/wiki/Peines_d%27amour_perdues
