---
title: New publication
author: Guillaume Gaullier
date: '2020-01-20'
slug: new-publication
categories:
  - Publications
tags:
  - work
lastmod: ~
authors:
  - guillaume
---


A short review article I contributed to got published today. Here is the full
reference (also added to the [publications page][publications]):

Makowski MM, **Gaullier G** & Luger K (2020) Picking a nucleosome lock:
Sequence- and structure-specific recognition of the nucleosome. *J Biosci*
**45**: 13. {{< doi "10.1007/s12038-019-9970-7" >}}


[publications]: {{< relref path="/page/publications" lang="en" >}}
