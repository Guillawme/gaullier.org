---
title: "Les meilleurs croissants de Boulder"
date: 2017-12-06
lastmod:
authors: [guillaume]
categories: []
tags: [food]
slug: meilleurs-croissants-de-boulder
---


J'ai déjà écrit deux fois à propos de la difficulté de trouver des bonnes
boulangeries dans ce pays ([ici][pain] et [là][moxie]). Eh bien nous avons
découvert récemment le *French Café*, nouvellement établi à Boulder (en Octobre
dernier), et qui vend de merveilleux croissants et pains au chocolat !

<!--more-->

Ces croissants et pains au chocolat sont vraiment authentiques, comme en France.
Voyez plutôt :

![Croissants](images/croissants.jpg)

Jusqu'à l'ouverture du *French Café*, il fallait conduire jusqu'à Fort Collins
(une heure de routes quand la circulation est bonne) pour trouver des croissants
de qualité équivalente à [La Crêperie][creperie] (excellent établissement, mais
simplement trop loin pour pouvoir y aller facilement).

C'est donc une excellente nouvelle, d'autant plus que le *French Café* est tout
près de chez nous. :-)

![French Café](images/french-cafe.jpg)


[pain]: {{< relref "2016-05-07-surprises-et-chocs-culturels-4" >}}

[moxie]: {{< relref "2016-12-16-moxie-bread-co" >}}

[creperie]: http://fortcollinscreperiebakery.com
