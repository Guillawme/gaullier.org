---
title: "DEMAIN : un concentré de bon sens"
date: 2016-01-01
lastmod:
authors: [guillaume]
categories: []
tags: [cinema]
slug: demain-un-concentre-de-bon-sens
---


Bonne année 2016 à tous. :-)  
Pour bien commencer l'année, je vous recommande le film [*DEMAIN*][demain], si
vous ne l'avez pas encore vu. Ce documentaire est en effet un véritable
concentré de bon sens !

<!--more-->

Le film aborde **le problème global de notre civilisation** que certains
appellent, à cause d'une vision trop étroite, "la crise", "le changement
climatique" ou encore "la pénurie prochaine de combustibles fossiles".
Mais contrairement à la majorité des documentaires qui s'intéressent à ces
sujets, celui-ci a deux énormes avantages : il adopte un point de vue réellement
global, et il est merveilleusement optimiste sur notre futur. Là où la plupart
des autres documentaires se contentent d'approfondir un problème particulier qui
se trouve en fait être une des nombreuses conséquences du problème global, et
sans réellement proposer de solution concrète.

*DEMAIN* vous présentera des solutions déjà en marche partout dans le monde pour
améliorer notre vie selon cinq axes fondamentaux : l'alimentation, l'énergie,
l'économie, la politique et l'éducation.

En somme, si vous en avez marre des *happy endings* hollywoodiens, alors je vous
recommande vivement d'aller voir *DEMAIN*. Il ne raconte pas une fin heureuse,
en fait il ne raconte même pas du tout une fin. Il montre comment pourra se
poursuivre sereinement une histoire qui est rarement racontée au cinéma : **la
nôtre**.


[demain]: http://www.demain-lefilm.com
