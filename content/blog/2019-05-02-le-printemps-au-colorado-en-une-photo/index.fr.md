---
title: Le printemps au Colorado en une photo
author: Guillaume Gaullier
date: '2019-05-02'
slug: le-printemps-au-colorado-en-une-photo
categories: []
tags:
  - Colorado
lastmod: ~
authors:
  - guillaume
---


Ici au printemps, il est courant d'avoir des jours chauds et ensoleillés et des
chutes de neige dans la même semaine, et quand cela arrive on voit des choses
insolites :

![Tulipes enneigées](images/tulipes-enneigees.jpg)

