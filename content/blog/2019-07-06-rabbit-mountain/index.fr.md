---
title: Rabbit Mountain
author: Guillaume Gaullier
date: '2019-07-06'
slug: rabbit-mountain
categories: []
tags:
  - Colorado
  - hiking
lastmod: ~
authors:
  - guillaume
---


Ce printemps, nous sommes allés plusieurs fois faire une même randonnée plutôt
facile, mais avec une vue excellente vers les montagnes. C'était à un endroit
appelé [*Rabbit Mountain*][rabbit-mountain], qui est en fait plutôt une colline
comparée aux Rocheuses juste à côté. Voilà quelques photos.

<!--more-->

Voilà une des collines faisant partie du parc :

![Vue depuis Rabbit Mountain](images/rabbit-mountain-1.jpg)

La randonnée fait le tour d'un groupe de collines similaires. Les photos qui
suivent montrent plusieurs vues depuis ces collines.

![Vue depuis Rabbit Mountain](images/rabbit-mountain-2.jpg)

Le canal qu'on aperçoit dans la photo suivante fait partie d'[un réseau
d'aqueducs qui importent de l'eau depuis le versant ouest des
Rocheuses][aqueducs] (le versant ouest reçoit plus de précipitations, mais la
population de l'état se trouve en majorité à l'est des Rocheuses).

![Vue depuis Rabbit Mountain](images/rabbit-mountain-3.jpg)


![Vue depuis Rabbit Mountain](images/rabbit-mountain-4.jpg)

![Vue depuis Rabbit Mountain](images/rabbit-mountain-5.jpg)

Une belle vue de [*Longs Peak*][longs-peak] :

![Vue depuis Rabbit Mountain](images/rabbit-mountain-6.jpg)


[rabbit-mountain]: https://goo.gl/maps/3EKhKPGSyfK2tUUt8

[aqueducs]: https://en.wikipedia.org/wiki/Colorado-Big_Thompson_Project

[longs-peak]: https://en.wikipedia.org/wiki/Longs_Peak
