---
title: Mon premier match de baseball
author: Guillaume Gaullier
date: '2019-07-13'
slug: mon-premier-match-de-baseball
categories: []
tags:
  - Colorado
lastmod: ~
authors:
  - guillaume
---


Le 3 juillet, Marta et Jeff nous ont emmenés voir un match de baseball à Denver.
C'était la première fois que je voyais ça en vrai, mais je n'ai toujours pas
bien compris les règles malgré les explications de Marta.

<!--more-->

J'ai compris qu'il y a une équipe qui attaque, (celle dont les batteurs jouent)
et une équipe qui défend (celle avec un joueur qui lance la balle au batteur).
Dans l'équipe attaquante, chaque batteur tente d'envoyer la balle assez loin
pour avoir le temps de faire le tour du terrain ou au moins de progresser
jusqu'à la prochaine base. L'équipe qui défend tente de les en empêcher en
rattrapant les balles et en apportant la balle à une base à bloquer. Ce que je
ne savais pas, c'est que le joueur qui lance la balle au batteur a le droit de
faire volontairement des lancers mauvais pour éliminer le batteur. Il y a plein
d'autres règles que je n'ai pas comprises.

Dans un match typique, il y a quelques phases rapides et intéressantes à
regarder, mais elles sont entrecoupées de longues phases où il ne se passe pas
grand-chose (principalement des lancers de balle ratés).

Le stade, appelé [Coors Field][coors-field], est immense (plus de 50 000 places) :

![Coors Field](images/baseball-1.jpg)

![Coors Field](images/baseball-2.jpg)

Juste avant une phase de jeu rapide... ou un autre lancer raté :

![Match](images/baseball-3.jpg)

Comme ce match était la veille de la fête nationale, il y a eu un feu d'artifice
à la fin. Et puisqu'il était tiré depuis le bâtiment même, tous les spectateurs
d'un côté du stade ont été déplacés jusque sur le terrain pour pouvoir voir le
spectacle et surtout ne pas être dans la zone de tir :

![Spectateurs](images/baseball-4.jpg)

![Feu d'artifice](images/baseball-5.jpg)


[coors-field]: https://fr.wikipedia.org/wiki/Coors_Field
