---
title: "Denver"
date: 2016-11-06
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: denver
---


Denver, ce n'est pas seulement [un dinosaure][denver-dino] mais c'est aussi le
nom de [la capitale du Colorado][denver-co]. Depuis quelques semaines nous avons
une voiture, ce qui nous permet d'y aller beaucoup plus facilement (il y a aussi
un bus, mais les horaires ne sont pas pratiques et le trajet est bien plus
long). Voilà quelques photos prises il y a déjà deux semaines lors d'un passage
en ville : enfin des photos sans montagnes !

<!--more-->

![Denver](images/denver-1.jpg)

![Denver](images/denver-2.jpg)

![Denver](images/denver-3.jpg)

![Denver](images/denver-4.jpg)

![Denver](images/denver-5.jpg)

![Denver](images/denver-6.jpg)

![Denver](images/denver-7.jpg)


[denver-dino]: https://www.youtube.com/watch?v=dFY-VWey5xw

[denver-co]: https://fr.wikipedia.org/wiki/Denver
