---
title: "Peanut Butter Cookies"
date: 2015-11-21
lastmod:
authors: [guillaume]
categories: []
tags: [food]
slug: peanut-butter-cookies
---


Au cours du grand ménage que je réalise en ce moment dans mes emails (dont je
parlais dans [un article précédent][gmail]), j'ai retrouvé une recette de
cookies au beurre de cacahuètes. Elle sera bien plus utile ici que perdue au fin
fond de ma boîte mail. :-)

<!--more-->

Cette recette vient de la mère de Rebecca. Nous l'avons testée quand je suis
allé rendre visite pour la première fois à ses parents en Oregon en
février 2014. Pour un francophone, le mot *cookie* évoque surtout un biscuit
contenant des pépites de chocolat, mais le mot désigne en fait tout type de
biscuit sucré (les anglophones font une distinction avec les biscuits salés,
qu'ils appellent *crackers*). La recette est donnée avec les
[unités du système anglo-saxon][unités-us]. N'étant pas de culture
anglo-saxonne, et étant de formation scientifique, ce système d'unités est pour
moi très bizarre comparé au [système international][unités-si] et sa logique par
puissances de 10. Voici la recette telle que je l'ai reçue, j'expliciterai
quelques subtilités anglo-saxonnes par la suite :

> Peanut Butter Cookies
> 
> Ingredients:
> 
> - 1/2 cup salted butter
> - 1/2 cup peanut butter (chunky or smooth)
> - 1/2 cup brown sugar
> - 1/4 cup white sugar
> - 1 egg
> - 1 + 1/4 cups white flour
> - 1/2 teaspoon baking powder
> - 3/4 teaspoon baking soda
> 
> Recipe:
> 
> Cream salted butter. Beat in peanut butter, brown sugar, white sugar and
> the egg.
> Sift or stir together white flour, baking powder and baking soda.
> Stir the dry mixture into the moist ingredients. Roll the dough into 1-inch
> balls, and set the balls on baking sheets. Press each ball once (or twice at
> right angles) with a fork dipped in sugar. Bake the cookies for 10 to 20
> minutes at 350 degrees F.

Si vous n'êtes pas habitué aux unités de mesure du système anglo-saxon, les
quelques précisions suivantes vous seront certainement utiles.

Le terme *cup* pourrait s'interpréter simplement comme un volume, permettant
ainsi d'exprimer les proportions entre les différents ingrédients. En réalité
non : **il s'agit d'un volume normalisé**.
[Aux État-Unis une *US customary cup* équivaut à 236,59 mL.][cup] Si vous êtes
correctement équipé, alors votre *cup* dispose de lignes de jauge pour marquer
les fractions de *cup* (la moitié et le quart). Si vous êtes très bien équipé,
vous disposez alors d'une série de plusieurs *cups* de tailles différentes,
correspondant chacune à une fraction (au moins 1/4, 1/2 et 1) : il est en effet
plus précis de remplir une *cup* de la bonne taille à ras bord que d'essayer
d'aligner le niveau de liquide ou de poudre sur une ligne de jauge dans la
*cup* unité.

Il en va de même pour toute une déclinaison de [*measuring spoons*][spoons] :
*tablespoon*, *teaspoon*, *1/2 teaspoon* et *1/4 teaspoon*. Il y a trois
*teaspoons* dans une *tablespoon*. Pour faire le lien avec la *cup*, il y a 16
*tablespoons* dans une *cup*, soit 48 *teaspoons*. Autrement dit, toutes ces
cuillères représentent elles aussi des volumes normalisés.

Ce système d'unités est pratique quand on est équipé des ustensiles de cuisine
appropriés : en effet, une mesure de volume se réduit alors à choisir le bon
ustensile et à le remplir à ras bord. C'est bien plus simple que de devoir
verser son ingrédient jusqu'au bon trait dans un verre doseur gradué en mL,
s'apercevoir qu'on a dépassé le trait et mettre de la farine partout en essayant
de remettre l'excédent dans le sac... En revanche, les conversions deviennent
atrocement compliquées : vous en avez un aperçu avec les 16 *tablespoons* qui
font une *cup*. En comparaison, le système international permet de convertir
bien plus facilement, simplement en multipliant ou divisant par des puissances
de 10 (dans 1 L il y a 1000 mL, ou 100 cL, ou 10 dL).

Les [degrés Fahrenheit][fahrenheit] sont plus connus, mais n'en sont pas moins
une unité complètement bizarre. L'échelle a en effet été construite à l'aide de
deux températures de référence issues de mesures de systèmes complètement
différents. Au contraire, l'échelle Celsius qui est plus familière pour nous
a été établie à l'aide de deux températures de référence basées sur les
changements d'état de l'eau (0 est la température au dessus de laquelle la glace
fond, et 100 est la température d'ébullition). L'information qui nous
intéressera ici, c'est que les 350 °F de la recette correspondent à environ
176,67 °C (la plupart des fours proposent 200 °C, ce qui convient très bien).

Finalement, un pouce ([*inch*][inch]) correspond à 25,4 mm. Mais la taille des
cookies n'est pas un paramètre critique dans la recette, c'est même plus
intéressant s'il y a plusieurs tailles. ^^

Après les unités, il vous manque encore peut-être un élément pour bien
interpréter la recette : que sont la *baking powder* et le *baking soda* ?
Ces deux ingrédients sont ce que nous appelons en français de la levure
chimique. Plus précisément, le [*baking soda*][baking-soda] n'est rien d'autre
que du bicarbonate de sodium, qui permet d'aérer la pâte en dégageant du dioxyde
de carbone lorsqu'il réagit avec des acides, ou avec lui-même sous l'effet de la
chaleur. La [*baking powder*][baking-powder] est une préparation plus élaborée,
contenant un carbonate ou bicarbonate (pas toujours de sodium, d'après ce que
j'ai pu lire ici et là), un acide faible et de l'amidon de maïs ou de pomme de
terre. L'acide présent dans la préparation permet d'assurer que la réaction
commencera dès le mélange avec la pâte (alors que le bicarbonate de sodium seul
dans une pâte peu acide ne peut dégager du dioxyde de carbone qu'à partir de la
cuisson). L'amidon, par son caractère hygroscopique, permet de protéger la
préparation de l'humidité ambiante de l'air, qui provoquerait une réaction
prématurée en humidifiant les deux autres poudres. Pour en venir à l'aspect
pratique qui nous intéresse pour la recette, je suppose qu'elle fonctionnerait
très bien en remplaçant ces deux ingrédients par un volume équivalent de
levure chimique.

Enfin, le beurre de cacahuètes n'est pas tellement utilisé en France. Comme nous
n'avons pas l'habitude d'en consommer, il existe un risque de choisir un produit
de type "pâte à tartiner" mais c'est une erreur ! Ces produits contiennent une
énorme proportion de sucre, de l'huile de palme, et de nombreux autres
ingrédients pas tous naturels qui n'ont rien à faire dans nos cookies (j'irai
même jusqu'à dire "rien à faire dans notre corps", mais ce n'est que mon avis ;
libre à chacun de s'empoisonner avec les produits ultra-transformés de la grande
distribution...). On peut généralement trouver du beurre de cacahuètes brut dans
les magasins bio. Une autre possibilité est de moudre soi-même des cacahuètes
qu'on trouve facilement dans les rayons dédiés aux produits apéritifs. Mais je
ne recommande pas vraiment cette option car c'est assez difficile, à moins
d'avoir un moulin mécanique ou électrique vraiment prévu pour cet usage (avec un
mortier et un pilon, ne vous donnez même pas la peine d'essayer...).
J'insiste sur l'huile de palme : il y en a dans beaucoup de sortes de beurres de
cacahuètes mais elle ne sert qu'à éviter qu'il se sépare en deux phases (une
phase d'huile au dessus d'une phase plus épaisse contenant la chair des
cacahuètes), uniquement pour l'esthétique du produit. Elle n'est absolument pas
nécessaire à la recette, et sa production est intimement liée à des
déforestations et autres pratiques que vous ne voulez vraiment pas soutenir par
des achats trop peu réfléchis.

En février 2014 nous avions essayé cette recette, et avions obtenu ce résultat
aussi joli que délicieux :

![Peanut Butter Cookies](images/peanut-butter-cookies-1.jpg)

Et une amie à qui j'avais donné la recette a obtenu ce résultat, visiblement un
peu plus cuit :

![Peanut Butter Cookies](images/peanut-butter-cookies-2.jpg)

J'ai personnellement une préférence pour la première cuisson. Ces cookies ne
cassaient pas mais *pliaient*, signe qu'ils étaient vraiment parfaits :D (et pas
trop gras...).


[gmail]: {{< relref "2015-11-02-gmail-et-sa-fonction-archiver" >}}

[unités-us]: https://fr.wikipedia.org/wiki/Unit%C3%A9s_de_mesure_anglo-saxonnes

[unités-si]: https://fr.wikipedia.org/wiki/Syst%C3%A8me_international_d'unit%C3%A9s

[cup]: https://en.wikipedia.org/wiki/Cup_%28unit%29#United_States_customary_cup

[spoons]: https://en.wikipedia.org/wiki/Measuring_spoon

[fahrenheit]: https://fr.wikipedia.org/wiki/Degr%C3%A9_Fahrenheit

[baking-soda]: https://en.wikipedia.org/wiki/Sodium_bicarbonate#Cooking

[baking-powder]: https://en.wikipedia.org/wiki/Baking_powder

[inch]: https://en.wikipedia.org/wiki/Inch
