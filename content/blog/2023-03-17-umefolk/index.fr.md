---
title: Umefolk
author: Guillaume Gaullier
date: '2023-03-17'
slug: umefolk
categories: []
tags:
  - sweden
  - music
lastmod: ~
authors:
  - guillaume
---


Quand nous sommes arrivés à Uppsala fin janvier 2020, nous avons presque
immédiatement rejoint le [V-Dala Spelmanslag][v-dala] (un ensemble de musique).
Les membres de cet ensemble parlaient alors d'aller à [Umefolk][umefolk], un
festival de musique qui a lieu chaque année vers fin février à [Umeå][umea].
Nous n'y étions pas allé car nous avions déjà accepté une invitation le même
week-end, et nous nous étions dit que nous irions l'année suivante. Mais à cause
de la pandémie, le festival n'a pas eu lieu en 2021 ni en 2022. Il a repris
cette année, et cette fois nous n'avons pas laissé passer l'opportunité d'y
aller.

<!--more-->

Voilà la vue depuis notre chambre d'hôtel :

![Umeå](images/umea-01.jpeg)

Le concert "*allspel*" du festival, très impressionnant (ouvert à tous les
musiciens ; nous n'avons pas participé car nous ne connaissions pas les morceaux
et n'avons pas eu le temps de les apprendre) :

![Umefolk allspel](images/umea-02.jpeg)

Nous avons assissté à quelques concerts.

Le duo [Lisas][lisas]:

![Lisas](images/umea-03.jpeg)

[Kardemimmit][kardemimmit], un groupe finlandais :

![Kardemimmit](images/umea-04.jpeg)

Nous avons écouté quelques autres concerts desquels je n'ai pas de photos. Nous
avons revu entre autres *Våtmark*, un groupe que nous avions [déjà entendu en
2020][music-2020].

Bien sûr, une bonne partie du festival a consisté à jouer de la musique avec des
gens rencontrés au hasard. Et nous avons aussi joué pour la danse avec V-Dala
Spelmanslag.

Nous avons aussi eu le temps de visiter un peu de centre d'Umeå, et de faire une
belle promenade le long de la rivière :

![Umeå](images/umea-05.jpeg)

![Umeå](images/umea-06.jpg)

![Umeå](images/umea-07.jpg)

![Umeå](images/umea-08.jpg)

![Umeå](images/umea-09.jpeg)


[v-dala]: https://www.v-dalaspelmanslag.se/

[umefolk]: https://umefolk.com

[umea]: https://fr.wikipedia.org/wiki/Ume%C3%A5

[lisas]: https://lisalangbacka.com/portfolio/langbacka-rydberg-lisas/

[kardemimmit]: http://www.kardemimmit.fi/

[music-2020]: {{< relref "2020-10-03-decouvertes-musicales" >}}
