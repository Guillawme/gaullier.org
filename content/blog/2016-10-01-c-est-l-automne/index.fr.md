---
title: "C'est l'automne"
date: 2016-10-01
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado]
slug: c-est-l-automne
---


Les jours raccourcissent rapidement, et il fait de plus en plus frais le matin
et le soir. On sent nettement l'automne s'installer ici. Le week-end dernier
nous sommes allés dans les montagnes, du côté de [Nederland][nederland], pour
voir les couleurs magnifiques avant que toutes les feuilles des peupliers ne
tombent. Voilà quelques photos.

<!--more-->

![Couleurs d'automnes](images/bouleaux-1.jpg)

![Couleurs d'automnes](images/bouleaux-2.jpg)

![Couleurs d'automnes](images/bouleaux-3.jpg)

![Couleurs d'automnes](images/bouleaux-4.jpg)

![Couleurs d'automnes](images/bouleaux-5.jpg)

![Couleurs d'automnes](images/bouleaux-6.jpg)

![Couleurs d'automnes](images/bouleaux-7.jpg)

![Couleurs d'automnes](images/bouleaux-8.jpg)

[nederland]: https://goo.gl/maps/Up8Rt8Djey42
