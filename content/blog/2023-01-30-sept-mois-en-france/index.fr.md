---
title: Sept mois en France
author: Guillaume Gaullier
date: '2023-01-30'
slug: sept-mois-en-france
categories: []
tags:
  - relocation
  - sweden
  - france
lastmod: ~
authors:
  - guillaume
---


L'année 2022 a été mouvementée. Nous avons quitté Uppsala en juin sur un coup de
tête de ma part, car j'avais un grand besoin de changer de travail, et une
annonce pour un poste en France correspondant assez à mon profil était parue
juste au moment où j'étais prêt à démissionner. Malheureusement, cette décision
n'était pas assez réfléchie.

<!--more-->

Changer de travail était indispensable, mais quitter Uppsala alors que nous nous
y sentions chez nous [après deux ans et demi][suede] n'était pas une bonne idée.
À peine arrivé à Toulouse, j'ai eu l'impression d'être revenu à Paris et de
littéralement étouffer dans une trop grande ville (j'ai adoré les cinq années
que j'ai passées à Paris, mais je ne veux plus vivre dans une ville aussi
grande). Un énorme choc culturel inverse a peut-être aussi contribué à mon
malaise, puisque je n'avais pas vécu en France depuis [avril 2016][us], et que
je suis allé dans une région que je ne connaissais pas du tout, et
culturellement très différente de celles où j'ai grandi. Enfin, la
[canicule][canicule] n'a fait que rendre tout cela plus difficile.

Heureusement j'ai eu une chance incroyable de réussir à retrouver un travail à
Uppsala, et nous y sommes revenus le 1er janvier. Rebecca poursuit la reprise
d'études qu'elle avait démarrée ici (même sans retard, puisque nous ne sommes
partis qu'un semestre, pendant lequel le cours suivant qu'elle devait prendre
n'était pas enseigné). Tout cela nous a coûté beaucoup de stress, d'argent
dépensé en déménagements et probablement d'opportunités manquées pendant ces
sept mois. Mais nous avons tout de même tiré parti de cette situation pour faire
un peu de tourisme et voir nos familles (les parents de Rebecca nous ont rendu
visite pendant l'été). Voilà quelques photos.

En juin, j'ai surtout occupé mon temps à m'adapter à un nouvel endroit, à faire
plein de démarches bureaucratiques, et à survivre à la canicule. Je n'ai pas
fait beaucoup de tourisme avant que Rebecca me rejoigne fin juin.

Nous avons fait quelques promenades dans le centre de Toulouse, mais je n'ai pas
pris beaucoup de photos. Voilà un des nombreux jolis monuments : 

![Toulouse](images/toulouse.jpg)

En juillet, nous sommes allés quelques jours dans le pays Basque avec les
parents de Rebeca.

Saint-Jean-de-Luz :

![Saint-Jean-de-Luz](images/saint-jean-de-luz.jpeg)

Hendaye :

![Hendaye](images/hendaye.jpeg)

San Sebastian :

![San Sebastian](images/san-sebastian.jpeg)

Après ça, Rebecca et ses parents sont allés dans les Pyrénées :

![Pyrénées](images/pyrenees.jpg)

Tandis que de mon côté, j'ai retrouvé mon cousin Vincent dans les Landes, à
Seignosse (nous ne nous étions pas vus depuis 2016, quand il nous avait rendu
visite à Boulder alors qu'il était aux États-Unis pour une conférence) :

![Seignosse](images/seignosse.jpeg)

Nous avons fait du surf pendant quelques jours, et cela m'a rappelé à quel point
ce sport est difficile physiquement. C'était aussi très angoissant de voir la
fumée des [feux de forêts][feux] depuis l'eau.

Après ça, nous sommes revenus à Toulouse et avons fait quelques petits voyages à
la journée.

À Foix :

![Foix](images/foix.jpeg)

À Cahors :

![Cahors](images/cahors.jpeg)

Ensuite, début août nous avons reçu Willie, un ami du Colorado, qui était en
Europe et a profité de l'occasion pour nous rendre visite. Nous sommes allés en
Méditerrannée avec lui, à Sigean puis à Port-la-Nouvelle, où nous avons pu nous
rafraichir un peu à la mer. Il y avait même assez de vent pour faire un peu de
kitesurf, c'était un bon bol d'air alors que je n'en avais pas fait depuis des
mois.

![Plage de la Vieille Nouvelle](images/port-la-nouvelle.jpeg)

Sur le trajet de retour vers Toulouse, nous nous sommes arrêtés à Narbonne, puis
à Carcassonne :

![Carcassonne](images/carcassonne.jpeg)

Après le départ de Willie, nous avons retrouvé mes grands-parents aux
Moutiers-en-Retz, où il a enfin fait une température supportable.

![Les Moutiers-en-Retz](images/les-moutiers-1.jpeg)

Le jour après cet orage, tout était très calme :

![Les Moutiers-en-Retz](images/les-moutiers-2.jpeg)

Je suis ensuite allé quelques jours en Bretagne avec ma famille, nous avons fait
le tour de l'île d'Hoëdic à pieds :

![Hoëdic](images/hoedic.jpeg)

En septembre, nous sommes allés à Albi :

![Albi](images/albi.jpeg)

Nous sommes aussi allés à Montech rendre visite à mon cousin Emmanuel et sa
famille. Nous avons vu la [pente d'eau][pente-d-eau].

![Montech](images/montech.jpeg)

Nous sommes aussi retournés à Hendaye pour un week-end, pour voir nos amis
Corinne et Jasper et faire du kayak dans les vagues.

En octobre, nous sommes allés à Paris pour un week-end. J'ai revu Maud et Teddy
(des amis de l'université) et rencontré leurs deux enfants. Rebecca a donné un
concert avec les *Cuckoo Sisters*, le groupe de musique américaine dans lequel
elle jouait quand nous habitions à Paris (ça faisait 6 ans que le groupe n'avait
pas été au complet, car plusieurs membres n'habitent plus à Paris).

Fin octobre, nous avons fait un peu de tourisme avec ma mère qui revenait d'un
road trip au Portugal. Nous sommes allés à Saint-Cirq-Lapopie pour le weekend de
la Toussaint. Il faisait incroyablement doux, c'était parfait pour de la rando :

![Dans les environs de Saint-Cirq-Lapopie](images/saint-cirq-lapopie-1.jpeg)

![Saint-Cirq-Lapopie](images/saint-cirq-lapopie-2.jpeg)

En novembre, je savais déjà que nous repartirions bientôt à Uppsala, alors j'ai
fait une grande virée en Bretagne pour voir des amis que je n'avais pas vus
depuis notre départ de Paris en 2016. Je suis d'abord allé à Lorient voir Mikaël
et Océane et leurs deux enfants. Une petite balade à la plage m'a donné envie
d'aller sur l'eau, mais je n'avais pas mon équipement de kitesurf avec moi alors
je me suis contenté de regarder.

![Près de Lorient](images/lorient.jpeg)

Je suis ensuite allé à Brest voir Béatrice et son fils. Nous avons fait une
belle promenade sur la côte, autour de la pointe Saint-Mathieu.

![Pointe Saint-Mathieu](images/pointe-saint-mathieu-1.jpeg)

![Pointe Saint-Mathieu](images/pointe-saint-mathieu-2.jpeg)

L'étape suivante était Morlaix, où plein de gens que nous connaissions de Paris
ont déménagé plus ou moins récemment. J'ai revu nos amis Robin et Sylvain, Sarah
et Pierre. J'ai vu la baie de Morlaix des deux côtés ; voilà la photo qui avait
la meilleure lumière :

![La baie de Morlaix](images/morlaix-1.jpeg)

Je me suis aussi beaucoup promené dans la ville.

![Morlaix](images/morlaix-2.jpeg)

Enfin, je suis allé en Normandie voir mes grands-parents à nouveau. La côte
était aussi magnifique là-bas :

![Une plage du Calvados](images/normandie-1.jpeg)

![Une plage du Calvados](images/normandie-2.jpeg)

![Un arc en ciel au dessus de la mer, vu depuis une plage du Calvados](images/normandie-3.jpeg)

En décembre, nous avons quitté Toulouse et passé quelques jours dans le Morbihan
autour de Noël.

![Soleil couchant derrière la Vilaine](images/la-vilaine.jpeg)

Puis nous avons passé le réveillon du nouvel an à Bruxelles. Notre amie Elsa qui
habite là-bas mais n'y était pas pendant les fêtes avait offert de nous prêter
son appartement pour quelques nuits. Nous avons passé un peu de temps avec la
famille d'accueil de Rebecca à Liège et à Waterloo.

Finalement, après tout ça, être de retour à Uppsala nous a vraiment donné
l'impression d'être revenus chez nous (même si nous sommes encore dans un
appartement temporaire, et que nous n'aurons un vrai "chez nous" que mi-février).

![La vue depuis notre appartement temporaire à Uppsala](images/uppsala.jpeg)


[suede]: {{< relref "2020-04-08-nouveau-demenagement" >}}

[us]: {{< relref "2016-04-03-premiers-jours-a-boulder" >}}

[canicule]: https://fr.wikipedia.org/wiki/Canicule_de_2022_en_Europe

[feux]: https://fr.wikipedia.org/wiki/Feux_de_for%C3%AAt_de_2022_en_Gironde

[pente-d-eau]: https://fr.wikipedia.org/wiki/Pente_d%27eau_de_Montech
