---
title: "Voyage en Australie"
date: 2017-10-21
lastmod:
authors: [guillaume]
categories: []
tags: [travels, australia]
slug: voyage-en-australie
---


Le 30 septembre dernier, nous étions invités à un mariage en Australie,
à [Perth][perth]. Nous avons profité de cette occasion pour passer une dizaine
de jours dans la région pour faire un peu de tourisme. Voilà des photos prises
là-bas. :-)

<!--more-->

Je m'amuse encore une fois à dessiner une carte pour donner une vue d'ensemble
du voyage :

<div class="embedded-content">
<iframe frameBorder="0" src="https://framacarte.org/fr/map/australie-2017_13857?scaleControl=true&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=false&onLoadPanel=undefined&captionBar=false&datalayers=22528&fullscreenControl=true&editinosmControl=false#7/-33.128/115.862"> </iframe>
</div>

Nous sommes partis de Bedfordale, là où habitent les amis qui se mariaient (et
qui nous hébergeaint), et avons fait une première escale sur la côte, dans la
ville de Bunbury, qui ressemble à ça :

![Bunbury](images/bunbury-1.jpg)

![Bunbury](images/bunbury-2.jpg)

![Bunbury](images/bunbury-3.jpg)

![Bunbury](images/bunbury-4.jpg)

Puis le même jour, nous sommes allés jusqu'à Margaret River, où nous avons passé
deux nuits. Cette petite ville est située au milieu d'une région viticole, et
nous avons fait une visite guidée de 4 vignobles dans les environs, où ces
photos ont été prises :

![Margaret River](images/margaret-river-1.jpg)

![Margaret River](images/margaret-river-2.jpg)

En repartant de Margaret River nous nous sommes arrêtés admirer la côte au
*Surfers Point* :

![Surfers Point](images/surfers-point-1.jpg)

![Surfers Point](images/surfers-point-2.jpg)

![Surfers Point](images/surfers-point-3.jpg)

![Surfers Point](images/surfers-point-4.jpg)

Plutôt que de prendre la route principale, nous avons suivi *Caves Road*, et
avons fait une escale pour visiter une des grottes :

![Grotte](images/cave-1.jpg)

De l'intérieur de la grotte, on peut voir des racines d'arbres qui ont percé le
toit de la grotte et continuent de pousser pour aller un jour collecter l'eau
qu'il y a au fond :

![Grotte](images/cave-2.jpg)

Nous avons ensuite roulé jusqu'au [Cap Leeuwin][cape-leeuwin] :

![Cap Leeuwin](images/cape-leeuwin-1.jpg)

![Cap Leeuwin](images/cape-leeuwin-2.jpg)

![Cap Leeuwin](images/cape-leeuwin-3.jpg)

![Cap Leeuwin](images/cape-leeuwin-4.jpg)

Après le Cap, nous avons fait une très longue étape jusqu'à Walpole, où nous
sommes restés une nuit. Le lendemain, nous avons fait une courte rando en forêt
peu après Walpole :

![Forêt](images/forest-1.jpg)

![Forêt](images/forest-2.jpg)

![Forêt](images/forest-3.jpg)

![Forêt](images/forest-4.jpg)

Puis nous sommes allés jusqu'à une autre forêt, plus loin sur la route, qui
offrait une grande passerelle appelée *Tree Top Walk*, pour se promener près de
la cîme des arbres jusqu'à 40 m de hauteur (j'ai eu un peu le vertige... Rebecca
n'a pas eu de problème) :

![Tree Top Walk](images/tree-top-1.jpg)

![Tree Top Walk](images/tree-top-2.jpg)

![Tree Top Walk](images/tree-top-3.jpg)

![Tree Top Walk](images/tree-top-4.jpg)

![Tree Top Walk](images/tree-top-5.jpg)

![Tree Top Walk](images/tree-top-6.jpg)

Après ça, la voiture que nous utilisions est tombée en panne : c'était un
problème mineur, mais qui nous a quand même empêchés de continuer (nous aurions
fait plus de dégâts en continuant...). Bien sûr c'est arrivé au milieu de nulle
part, où les téléphones portables n'avaient aucun réseau. Heureusement nous
étions tout près d'une maison quand nous avons dû nous arrêter, et ces gens nous
ont aidés à appeler une dépanneuse. Nous nous sommes faits remorquer jusqu'à la
ville de Denmark, plus loin sur notre itinéraire, où nous sommes restés bloqués
deux jours. Car bien sûr, en plus de tomber en panne au milieu de nulle part,
c'est arrivé un dimanche veille de jour férié (l'anniversaire de la reine ; qui
d'ailleurs n'était pas le vrai jour de son anniversaire, car nous avons appris
que les différents états et territoires d'Australie fêtent cet anniversaire
à des dates différentes). La météo était misérable pendant ces deux jours, donc
nous avons surtout visité la boulangerie de Denmark... et aussi la poste, car
nous avons eu du temps pour écrire des cartes postales. À la poste de Denmark,
un client juste devant nous dans la file venait se plaindre que son courrier
arrivait au Danemark ! (le pays d'Europe, pas la ville de Denmark en Australie).
Une fois mardi arrivé, et le garagiste ouvert, il a suffi de quelques heures
pour réparer la voiture. :-) Nous sommes alors rentrés à Bedfordale directement,
avec seulement une courte escale à Kojonup. Nous avions encore trois jours
devant nous, donc aucun risque de rater le mariage, mais nous voulions avoir le
temps de visiter [*Rottnest Island*][rottnest]. Nous avons fait une bonne partie
de l'île en vélo, et les photos suivantes ont été prises là-bas :

![Rottnest Island](images/rottnest-1.jpg)

Cette île abrite un petit marsupial appelé le [quokka][quokka]. Ces animaux
n'ont pas de prédateurs sur l'île, et ils ne sont donc pas du tout effrayés par
les humains.

![Rottnest Island](images/rottnest-2.jpg)

![Rottnest Island](images/rottnest-3.jpg)

![Rottnest Island](images/rottnest-4.jpg)

![Rottnest Island](images/rottnest-5.jpg)

![Rottnest Island](images/rottnest-6.jpg)

![Rottnest Island](images/rottnest-7.jpg)

![Rottnest Island](images/rottnest-8.jpg)

![Rottnest Island](images/rottnest-9.jpg)

À la fin de notre séjour, nous avons passé une journée à Perth, principalement
pour voir *King's Park* où les quelques photos suivantes (ainsi que certaines
photos de fleurs à la fin de l'article) ont été prises :

![Perth](images/perth-1.jpg)

![Perth](images/perth-2.jpg)

![Perth](images/perth-3.jpg)

![Perth](images/perth-4.jpg)

![Perth](images/perth-5.jpg)

Cette dernière photo a été prise dans un autre parc, là où a eu lieu la
réception après le mariage :

![Perth](images/perth-6.jpg)

Pour terminer en beauté, voilà des photos de fleurs natives de l'Australie (pour
la plupart, je ne me souviens plus des noms...) :

![Fleurs](images/flowers-1.jpg)

Les fleurs de la photo suivante s'appellent des "pattes de kangourou"
([*kangaroo paw*][kangaroo-paw]) :

![Fleurs](images/flowers-2.jpg)

![Fleurs](images/flowers-3.jpg)

![Fleurs](images/flowers-4.jpg)

![Fleurs](images/flowers-5.jpg)

![Fleurs](images/flowers-6.jpg)

![Fleurs](images/flowers-7.jpg)

![Fleurs](images/flowers-8.jpg)

![Fleurs](images/flowers-9.jpg)

![Fleurs](images/flowers-10.jpg)

![Fleurs](images/flowers-11.jpg)

![Fleurs](images/flowers-12.jpg)


[perth]: https://fr.wikipedia.org/wiki/Perth_(Australie-Occidentale)

[cape-leeuwin]: https://fr.wikipedia.org/wiki/Cap_Leeuwin

[rottnest]: https://fr.wikipedia.org/wiki/Rottnest_Island

[quokka]: https://fr.wikipedia.org/wiki/Quokka

[kangaroo-paw]: https://en.wikipedia.org/wiki/Kangaroo_paw
