---
title: 'New tool: localres'
author: Guillaume Gaullier
date: '2019-12-13'
slug: new-tool-localres
categories:
  - Software
tags:
  - work
  - learning
  - Python
  - visualization
  - cryo-em
lastmod: ~
authors:
  - guillaume
---


I just finished putting together a command-line tool to plot a histogram of
local resolution values from a local resolution 3D map and a 3D mask
(`relion_locres.mrc` and `mask.mrc` files from RELION, respectively). This is a
useful visualization to complement a color-coded local resolution scale mapped
onto the 3D reconstruction (or visualized across slices of said reconstruction)
as usually shown in publications describing a cryo-EM structure. An example is
displayed in Figure S3C of my [recent preprint][preprint].

I got the idea from [a Twitter conversation][twitter-thread], reused the
suggested code and wrapped it in a command-line tool that is documented and
[easy to install][localres-pypi]. The (very simple) code is available
[here][repo]. I also made it citeable with {{< doi "10.5281/zenodo.3575230" >}}.

This was a good toy project to learn some basics of Python, especially to learn
how to use the [click library][click] to parse command-line arguments, how to
package a Python program and how to distribute it. I was surprised that anyone
can upload anything to the [Python Package Index][pypi] without any form of
review (in the R world, a review team checks every package submitted to the
[CRAN repository][cran]).

[preprint]: {{< relref "2019-11-19-new-preprint" >}}

[twitter-thread]: https://twitter.com/biochem_fan/status/1161347681110962177

[localres-pypi]: https://pypi.org/project/localres

[repo]: https://github.com/Guillawme/localres

[click]: https://palletsprojects.com/p/click

[pypi]: https://pypi.org

[cran]: https://cran.r-project.org/web/packages/policies.html
