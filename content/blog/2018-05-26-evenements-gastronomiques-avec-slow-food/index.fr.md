---
title: "Évènements gastronomiques avec Slow Food"
date: 2018-05-26T15:26:14-06:00
lastmod:
authors: [guillaume]
categories: []
tags: [food]
slug: evenements-gastronomiques-avec-slow-food
---


Depuis déjà plusieurs mois, Rebecca a remis en activité la [branche
locale][slowfoodboco] de [*Slow Food*][slowfood], qui était dormante depuis
plusieurs années. Voilà des photos des deux derniers évènements gastronomiques
organisés par *Slow Food* auquels nous avons assisté.

<!--more-->

Le premier était une dégustation de fromages de brebis et chèvre (il manque le
premier échantillon sur la photo, parce que je n'ai pensé à prendre une photo
qu'après avoir commencé...). Cet évènement était hébergé par [une ferme
locale][curefarm].

![Fromages](images/fromages.jpg)

Le deuxième évènement était un dîner mexicain, pour une levée de fonds pour
*Slow Food México*. Cinq plats et trois boissons ! Je n'ai pas de photo de la
première boisson, c'était simplement une [margarita classique][margarita]
(quoique très salée...). La deuxième boisson était aussi une sorte de margarita,
au charbon. La troisième était de la tequila agée en fût.

![Menu](images/slow-food-menu.jpg)

![Premier plat](images/plat-1.jpg)

![Deuxième plat](images/plat-2.jpg)

![Troisième plat](images/plat-3.jpg)

![Margarita au charbon](images/margarita-charbon.jpg)

![Quatrième plat](images/plat-4.jpg)

![Dessert](images/dessert.jpg)

![Digestif](images/digestif.jpg)


[slowfoodboco]: http://www.slowfoodboulder.com

[slowfood]: https://fr.wikipedia.org/wiki/Slow_Food

[curefarm]: http://www.cureorganicfarm.com

[margarita]: https://fr.wikipedia.org/wiki/Margarita_(cocktail)
