---
title: "Randonnée matinale"
date: 2017-03-19
lastmod:
authors: [guillaume]
categories: []
tags: [Colorado, hiking]
slug: randonnee-matinale
---


Aujourd'hui nous nous sommes levés à 5h du matin pour arriver en haut des
collines voisines avant le lever du soleil. Voilà ce que nous avons vu.

<!--more-->

Nous avons réussi à voir le lever du soleil ! C'était l'objectif principal. :-)

![Aube](images/aube-1.jpg)

![Aube](images/aube-2.jpg)

![Aube](images/aube-3.jpg)

![Aube](images/aube-4.jpg)

Nous avons aussi eu un aperçu de l'incendie qui s'est déclenché cette nuit dans
le *Sunshine Canyon*... Il est toujours en cours à l'heure où j'écris, mais
bientôt maîtrisé. Je ne sais pas encore si son origine a été déterminée.
Beaucoup de gens ont posté des photos sur Twitter au cours de l'évolution de
l'incendie, avec le mot-clé [#SunshineFire][sunshinefire]. Voilà deux photos
prises à une heure d'intervalle. La fumée s'étendait beaucoup plus loin vers le
nord-est que ce que montre la photo.

![Sunshine Fire](images/incendie-1.jpg)

![Sunshine Fire](images/incendie-2.jpg)

Voilà deux photos prises au sommet de la colline que nous avons grimpée :

![Sommet](images/sommet-1.jpg)

![Sommet](images/sommet-2.jpg)

Et en redescendant par l'autre côté de la colline, nous avons eu une bonne
surprise :

![Biches](images/biches-1.jpg)

![Biches](images/biches-2.jpg)


[sunshinefire]: https://twitter.com/hashtag/SunshineFire
