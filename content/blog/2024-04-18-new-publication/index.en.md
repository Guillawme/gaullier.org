---
title: New publication
author: Guillaume Gaullier
date: '2024-04-18'
slug: new-publication
categories:
  - Publications
tags:
  - work
  - cryo-em
lastmod: ~
authors:
  - guillaume
bibliography: ../../static/bibliography/references.bib
csl: ../../static/bibliography/the-embo-journal.csl
link-citations: yes
---


The [preprint I contributed to][preprint] at the end of last year is now
peer-reviewed:

Sanchez-Garcia R, **Gaullier G**, Cuadra-Troncoso JM & Vargas J (2024) Cryo-EM
Map Anisotropy Can Be Attenuated by Map Post-Processing and a New Method for Its
Estimation. *International Journal of Molecular Sciences* **25**: 3959
{{< doi "10.3390/ijms25073959" >}}

I added it to the [publications][publications] page.

[preprint]: {{< relref "2024-02-17-new-preprint" >}}

[publications]: {{< relref path="/page/publications" lang="en" >}}
