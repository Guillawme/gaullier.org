---
title: I no longer use X/Twitter
author: Guillaume Gaullier
date: '2024-11-16'
slug: i-no-longer-use-x-twitter
categories: []
tags:
  - work
lastmod: ~
authors:
  - guillaume
---


I am not proud it took me so long, but I finally closed my account on X/Twitter
earlier this week.

I had virtually not used it since late 2022. I started using it in 2009 for
random chitchat, but in recent years I had mostly adjusted my use of this social
network for professional purposes (discussing with other scientists). Checking
back recently, I noticed that several of the accounts that started following
mine in the last two years were bots promoting content that, well, let's say
does not exactly look professional...

Some people chose to delete their messages but keep the account, to prevent the
handle from being re-used. I considered doing the same, but figured that I am
not famous enough to worry about my handle being re-used. More importantly, I
decided that I no longer want any connection whatsoever with this social network
and its owner.

I currently have an account on Bluesky, because unfortunately most of the people
I follow out of professional interest moved to this platform. But I am not
planning to invest any effort into a platform that recently took funding from
investors. We now know too well what happens when the return on investment comes
due: ads, paid subscriptions, sell-out to a richer guy, or a combination thereof.
There is even [a new word][enshitiffication] for this.

The bottom line is: if you ever see *a @Guillawme on X, this is not me*, and
*don't bother following me on Bluesky*.

Choose a future-proof platform instead: choose the [fediverse][fedi]. It has
been decentralized and federated from day one, which makes it immune to corporate
interests and takeovers. A variety of entities run fediverse servers: individuals,
non-profits, government agencies, news outlets, etc. All can interoperate. This
is a much saner ecosystem than a single platform owned by a corporation whose
customers are advertisers. If you are in a position to do so, encourage your
organization to run their own fediverse instance: ownership of their data,
control of their digital infrastructure and of their branding are all reasons
that a sensible organization should recognize are important. Visibility will
temporarily decrease, but we're in this for the long haul. In a few years, some
billionaire might well decide to buy and ruin Bluesky. How many such cycles of
platform decay will you tolerate? I personally already have too much platform
churn fatigue. When Bluesky "does a Twitter", I am confident that the fediverse
will still be around and will have become a stable piece of internet
infrastructure here to stay.

You will find me on the fediverse as [@Guillawme@fediscience.org][me] (I chose
an instance run by a non-profit and funded by donations). It's super nice over
there. :-)


[enshitiffication]: https://en.wikipedia.org/wiki/Enshittification

[fedi]: https://en.wikipedia.org/wiki/Fediverse

[me]: https://fediscience.org/@Guillawme
